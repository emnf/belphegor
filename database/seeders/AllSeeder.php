<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class AllSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([
            [
                'id' => 1,
                'uuid' => Uuid::uuid4()->getHex(1),
                'name' => 'admin',
                'username' => 'admin',
                'email' => 'admin@mail.com',
                'password' => Hash::make('1234567890'),
                's_id_hakakses' => 1,
                'nik' => null,
                'kecamatan' => null,
                'kelurahan' => null,
                'dukuh' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);


        DB::table('roles')->insert([
            [
                'id' => 1,
                'name' => 'Administrator',
                'guard_name' => 'web',
                'urut' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);

        DB::table('permissions')->insert([
            [
                'id' => 1,
                'name' => 'SettingPemda',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 2,
                'name' => 'SettingLoginBackground',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 3,
                'name' => 'SettingUser',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 4,
                'name' => 'SettingRole',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 5,
                'name' => 'SettingPermission',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 6,
                'name' => 'SettingAssignPermissionToRole',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 7,
                'name' => 'SettingAssignRoleToUser',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 8,
                'name' => 'SettingPass',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 9,
                'name' => 'SettingText',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);


        DB::table('model_has_roles')->insert([
            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 1,
            ],
            // [
            //     'role_id' => 2,
            //     'model_type' => 'App\Models\User',
            //     'model_id' => 2,
            // ],
        ]);

        DB::table('role_has_permissions')->insert([
            [
                'permission_id' => 1,
                'role_id' => 1,
            ],
            [
                'permission_id' => 2,
                'role_id' => 1,
            ],
            [
                'permission_id' => 3,
                'role_id' => 1,
            ],
            [
                'permission_id' => 4,
                'role_id' => 1,
            ],
            [
                'permission_id' => 5,
                'role_id' => 1,
            ],
            [
                'permission_id' => 6,
                'role_id' => 1,
            ],
            [
                'permission_id' => 7,
                'role_id' => 1,
            ],
            [
                'permission_id' => 8,
                'role_id' => 1,
            ],
            [
                'permission_id' => 9,
                'role_id' => 1,
            ]
        ]);

        DB::table('s_status')->insert([
            [
                's_id_status' => 1,
                's_nama_status' => 'Aktif',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                's_id_status' => 2,
                's_nama_status' => 'Tidak Aktif',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);

        DB::table('s_background')->insert([
            [
                'id' => 1,
                's_thumbnail' => '/public/upload/21anew.png',
                's_id_status' => 1,
                's_iduser' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);

        DB::table('s_pemda')->insert([
            [
                's_idpemda' => 1,
                's_namaprov' => 'Propinsi Papua Barat',
                's_namakabkot' => 'Kabupaten Fakfak',
                's_namaibukotakabkot' => 'Fakfak',
                's_kodeprovinsi' => '91',
                's_kodekabkot' => '01',
                's_namainstansi' => 'Badan Pendapatan DAERAH',
                's_namasingkatinstansi' => 'BAPENDA',
                's_alamatinstansi' => 'JL. JEND SUDIRMAN NO 9',
                's_notelinstansi' => '22323',
                's_namabank' => 'Bank Papua',
                's_norekbank' => '-',
                's_logo' => 'upload/logo.png',
                's_namacabang' => 'Fakfak',
                's_kodecabang' => '-',
                's_kodepos' => '-',
                's_email' => '-',
                's_urlapps' => '-',
                's_namakecamatan' => 'Distrik',
                's_namakelurahan' => 'Kampung',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);

        $statement11 = "ALTER SEQUENCE s_background_id_seq RESTART WITH 2;";
        DB::unprepared($statement11);
    }
}
