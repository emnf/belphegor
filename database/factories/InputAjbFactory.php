<?php

namespace Database\Factories;

use Faker\Generator as Faker;


$factory->define(\App\Models\Pelaporan\InputAjb::class, function (Faker $faker) {
    return [
        't_no_ajb' => $faker->words(3, true)
    ];
});
