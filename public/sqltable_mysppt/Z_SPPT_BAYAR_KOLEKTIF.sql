/*
 Navicat Premium Data Transfer

 Source Server         : ORACLE - MPU
 Source Server Type    : Oracle
 Source Server Version : 120100 (Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, OLAP, Advanced Analytics and Real Application Testing options)
 Source Host           : 172.16.70.9:1521
 Source Schema         : KULONPROGO

 Target Server Type    : Oracle
 Target Server Version : 120100 (Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, OLAP, Advanced Analytics and Real Application Testing options)
 File Encoding         : 65001

 Date: 22/09/2022 17:32:01
*/


-- ----------------------------
-- Table structure for Z_SPPT_BAYAR_KOLEKTIF
-- ----------------------------
DROP TABLE "KULONPROGO"."Z_SPPT_BAYAR_KOLEKTIF";
CREATE TABLE "KULONPROGO"."Z_SPPT_BAYAR_KOLEKTIF" (
  "KD_PROPINSI" CHAR(2 BYTE) VISIBLE,
  "KD_DATI2" CHAR(2 BYTE) VISIBLE,
  "KD_KECAMATAN" CHAR(3 BYTE) VISIBLE,
  "KD_KELURAHAN" CHAR(3 BYTE) VISIBLE,
  "KD_BLOK" CHAR(3 BYTE) VISIBLE,
  "NO_URUT" CHAR(4 BYTE) VISIBLE,
  "KD_JNS_OP" CHAR(1 BYTE) VISIBLE,
  "THN_PAJAK_SPPT" CHAR(4 BYTE) VISIBLE,
  "KODE_BAYAR" VARCHAR2(22 BYTE) VISIBLE,
  "JUMLAH_POKOK" NUMBER(15,0) VISIBLE,
  "JUMLAH_DENDA" NUMBER(15,0) VISIBLE,
  "JUMLAH_TOTAL" NUMBER(15,0) VISIBLE,
  "TGL_BAYAR_SPPT" DATE VISIBLE,
  "TGL_JATUH_TEMPO_KODEBAYAR" DATE VISIBLE,
  "TGL_JATUH_TEMPO_SPPT" DATE VISIBLE,
  "IDBANK" NUMBER(15,0) VISIBLE,
  "CREATED_AT" TIMESTAMP(6) VISIBLE,
  "NO_URUT_BILLING" NUMBER(5,0) VISIBLE,
  "STATUS_BAYAR" NUMBER(1,0) VISIBLE DEFAULT 0,
  "KD_BANK_TUNGGAL" CHAR(2 BYTE) VISIBLE,
  "KD_DUKUH" CHAR(3 BYTE) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536
  NEXT 1048576
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Triggers structure for table Z_SPPT_BAYAR_KOLEKTIF
-- ----------------------------
CREATE TRIGGER "KULONPROGO"."DEL_BILLING" BEFORE DELETE ON "KULONPROGO"."Z_SPPT_BAYAR_KOLEKTIF" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW
BEGIN
  UPDATE SPPT SET STATUS_BILLING_KOLEKTIF = 0
  WHERE KD_PROPINSI                      = :OLD.KD_PROPINSI AND
          KD_DATI2                         = :OLD.KD_DATI2 AND
        KD_KECAMATAN                     = :OLD.KD_KECAMATAN AND
        KD_KELURAHAN                     = :OLD.KD_KELURAHAN AND
        KD_BLOK                             = :OLD.KD_BLOK AND
        NO_URUT                             = :OLD.NO_URUT AND
        KD_JNS_OP                         = :OLD.KD_JNS_OP AND
        THN_PAJAK_SPPT                     = :OLD.THN_PAJAK_SPPT;
EXCEPTION WHEN OTHERS THEN NULL;
END;
/
CREATE TRIGGER "KULONPROGO"."INS_BILLING" BEFORE INSERT ON "KULONPROGO"."Z_SPPT_BAYAR_KOLEKTIF" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW
BEGIN
  UPDATE SPPT SET STATUS_BILLING_KOLEKTIF = 1
  WHERE KD_PROPINSI                      = :NEW.KD_PROPINSI AND
          KD_DATI2                         = :NEW.KD_DATI2 AND
        KD_KECAMATAN                     = :NEW.KD_KECAMATAN AND
        KD_KELURAHAN                     = :NEW.KD_KELURAHAN AND
        KD_BLOK                             = :NEW.KD_BLOK AND
        NO_URUT                             = :NEW.NO_URUT AND
        KD_JNS_OP                         = :NEW.KD_JNS_OP AND
        THN_PAJAK_SPPT                     = :NEW.THN_PAJAK_SPPT;
EXCEPTION WHEN OTHERS THEN NULL;
END;
/
