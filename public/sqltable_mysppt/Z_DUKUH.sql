/*
 Navicat Premium Data Transfer

 Source Server         : ORACLE - MPU
 Source Server Type    : Oracle
 Source Server Version : 120100 (Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, OLAP, Advanced Analytics and Real Application Testing options)
 Source Host           : 172.16.70.9:1521
 Source Schema         : KULONPROGO

 Target Server Type    : Oracle
 Target Server Version : 120100 (Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, OLAP, Advanced Analytics and Real Application Testing options)
 File Encoding         : 65001

 Date: 22/09/2022 17:30:11
*/


-- ----------------------------
-- Table structure for Z_DUKUH
-- ----------------------------
DROP TABLE "KULONPROGO"."Z_DUKUH";
CREATE TABLE "KULONPROGO"."Z_DUKUH" (
  "KD_KECAMATAN" CHAR(3 BYTE) VISIBLE,
  "KD_KELURAHAN" CHAR(3 BYTE) VISIBLE,
  "KD_DUKUH" CHAR(3 BYTE) VISIBLE,
  "NM_DUKUH" VARCHAR2(255 BYTE) VISIBLE,
  "IS_ACTIVE" NUMBER(1,0) VISIBLE DEFAULT 1,
  "CREATED_AT" TIMESTAMP(6) VISIBLE,
  "UPDATED_AT" TIMESTAMP(6) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536
  NEXT 1048576
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
