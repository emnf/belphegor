$(document).ready(function() {
    $(".masked-nop").mask('99.99.999.999.999.9999.9');
    $(".masked-noptahun").mask('99.99.999.999.999.9999.9-9999');
    $(".masked-nik").mask('9999999999999999');
    $(".masked-npwpd").mask('P.9.9999999.99.999');
    $(".masked-number-2").mask('99');
    $(".masked-number-3").mask('999');
    $(".masked-number-4").mask('9999');
    $(".masked-number-5").mask('99999');
    $(".masked-billing").mask('999999999999999999');
    $(".masked-hp").mask('999999999999999');
    $('.masked-money').mask('#.##0', { reverse: true });
});
