<?php

namespace App\Http\Controllers\Activity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class HistoryActivityController extends Controller
{
    public function index()
    {
        return view('history-activity.index');
    }

    public function datagrid(Request $request)
    {
        $response = Activity::leftJoin(
            'users', 'users.id', '=', 'activity_log.causer_id' 
        );

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('activity_log.id', 'desc');
        }

        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $properties = json_decode($v['properties'], true);
            $attr = "";
            if (!empty($properties)) {
                foreach ($properties['attributes'] as $l => $p) {
                    if (strpos($l, 's_') !== false) {
                        $nameColumn = explode("_", $l);
                        $attr .= ucwords($nameColumn[1]) . " : " . $p . "<br>";
                    } elseif (strpos($l, 't_') !== false) {
                        $nameColumn = explode("_", $l);
                        $attr .= ucwords($nameColumn[1]) . " : " . $p . "<br>";
                    }else {
                        $attr .= ucwords($l) . " : " . $p . "<br>";
                    }
                }
            }
            $subject_type = explode("\\", $v['subject_type']);
            $causer_type = explode("\\", $v['causer_type']);
            $dataArr[] = [
                'log_name'  => $v['log_name'],
                'description' => $v['description'],
                'subject_type'  => end($subject_type),
                'subject_id' => $v['subject_id'],
                'causer_type'  => end($causer_type),
                'causer_id' => $v['name'],
                'properties'  => $attr,
                'created_at' => date('d-m-Y H:i:s', strtotime($v['created_at'])),
                'actionList' => []
            ];
        }

        $response = [
            'data'  => [
                'content'           => $dataArr,
                'number'            => $response->currentPage() - 1,
                'size'              => $response->perPage(),
                'first'             => $response->onFirstPage(),
                'last'              => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages'        => $response->lastPage(),
                'numberOfElements'  => $response->count(),
                'totalElements'     => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }
}
