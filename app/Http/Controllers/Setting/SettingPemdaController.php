<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PemdaRequest;
use App\Models\Setting\Pemda;
use Illuminate\Support\Facades\Storage;

class SettingPemdaController extends Controller
{
    public function index()
    {
        return view('setting-pemda.index', [
            'pemda' => Pemda::orderBy('updated_at', 'desc')->first()
        ]);
    }

    public function createOrUpdate(PemdaRequest $pemdaRequest)
    {
        $attr = $pemdaRequest->except(['_token']);
        //sdd($pemdaRequest->file('s_logo'));
        if ($pemdaRequest->hasFile('s_logo')) {
            $fileExtension = $pemdaRequest->file('s_logo')->getClientOriginalExtension();
            if ($fileExtension == 'pdf') {
                $file_content = 'application';
            } else {
                $file_content = 'image';
            }
            $files = base64_encode(file_get_contents($pemdaRequest->file('s_logo')));
            $base64 = 'data:' . $file_content . '/' . $fileExtension . ';base64,' . $files;

            $attr['s_logo'] = $base64;
        }

        if ($attr['s_idpemda'] == null) {
            $process = Pemda::create($attr);
        } else {
            $process = Pemda::where('s_idpemda', $attr['s_idpemda'])
                ->update($attr);
        }

        if ($process) {
            session()->flash('success', 'Data pemda berhasil di update.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-pemda');
    }
}
