<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\DukuhRequest;
use App\Models\Dropdown\Status;
use App\Models\Pbb\Dukuh;
use App\Models\Pbb\RefKecamatan;
use App\Models\Pbb\RefKelurahan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingDukuhController extends Controller
{
    public function index()
    {
        return view('setting-dukuh.index');
    }

    public function create()
    {
        if (Auth::user()->s_id_hakakses == 4) {
            $kecamatan = RefKecamatan::where([
                ['KD_KECAMATAN', Auth::user()->kecamatan]
            ])->first();
            $kelurahan = RefKelurahan::where([
                ['KD_KECAMATAN', Auth::user()->kecamatan],
                ['KD_KELURAHAN', Auth::user()->kelurahan]
            ])->first();
        } else {
            $kecamatan = RefKecamatan::orderBy('KD_KECAMATAN')->get();
            $kelurahan = [];
        }
        return view('setting-dukuh.create', [
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,
            'dukuh' => new Dukuh(),
            'status' => Status::get()
        ]);
    }

    public function store(DukuhRequest $dukuhRequest)
    {
        $attr = $dukuhRequest->except('_token');

        // dd($attr);
        try {
            $kdDukuh = str_pad($attr['kd_dukuh'], 3, "0", STR_PAD_LEFT);
            $checkExits = Dukuh::where([
                ['KD_KECAMATAN', $attr['KD_KECAMATAN']],
                ['KD_KELURAHAN', $attr['KD_KELURAHAN']],
                ['kd_dukuh', $kdDukuh],
            ])->first();

            if (!$checkExits) {

                Dukuh::insert([
                    'KD_KECAMATAN' => $attr['KD_KECAMATAN'],
                    'KD_KELURAHAN' => $attr['KD_KELURAHAN'],
                    'kd_dukuh' => $kdDukuh,
                    'nm_dukuh' => $attr['nm_dukuh'],
                    'is_active' => $attr['is_active'],
                    'created_at' => Carbon::now()
                ]);

                session()->flash('success', 'Data Dukuh berhasil ditambahkan.');
                return redirect('dukuh');
            } else {
                session()->flash('info', 'Kode Dukuh ' . $kdDukuh . ' sudah digunakan!');
                return redirect('dukuh/create');
            }
        } catch (\Throwable $th) {
            session()->flash('error', $th->getMessage());
            return redirect('dukuh');
        }
    }

    public function edit(String $kode)
    {
        // dd(substr($kode,6,3));
        if (Auth::user()->s_id_hakakses == 4) {
            $kecamatan = RefKecamatan::where([
                ['KD_KECAMATAN', Auth::user()->kecamatan]
            ])->first();
            $kelurahan = RefKelurahan::where([
                ['KD_KECAMATAN', Auth::user()->kecamatan],
                ['KD_KELURAHAN', Auth::user()->kelurahan]
            ])->first();
        } else {
            $kecamatan = RefKecamatan::orderBy('KD_KECAMATAN')->get();
            $kelurahan = [];
        }

        $dukuh = Dukuh::where([
            ['KD_KECAMATAN', substr($kode, 0, 3)],
            ['KD_KELURAHAN', substr($kode, 3, 3)],
            ['kd_dukuh', substr($kode, 6, 3)],
        ])->first();

        // dd($dukuh);
        return view('setting-dukuh.edit', [
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,
            'dukuh' => $dukuh,
            'status' => Status::get()
        ]);
    }

    public function update(DukuhRequest $dukuhRequest, string $kode)
    {
        $attr = $dukuhRequest->except('_token', '_method');

        // dd($attr);
        try {

            Dukuh::where([
                ['KD_KECAMATAN', substr($kode, 0, 3)],
                ['KD_KELURAHAN', substr($kode, 3, 3)],
                ['kd_dukuh', substr($kode, 6, 3)],
            ])->update($attr);

            session()->flash('success', 'Data Dukuh berhasil diupdate.');
        } catch (\Throwable $th) {
            session()->flash('error', $th->getMessage());
        }
        return redirect('dukuh');
    }

    public function datagrid(Request $request)
    {
        $response = new Dukuh();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('KD_KECAMATAN');
            $response = $response->orderBy('KD_KELURAHAN');
            $response = $response->orderBy('kd_dukuh');
        }

        if ($request['filter']['kecamatan'] != null) {
            $response = $response->where('KD_KECAMATAN', 'like', $request['filter']['kecamatan'] . '%');
        }

        if ($request['filter']['kelurahan'] != null) {
            $response = $response->where('KD_KELURAHAN', 'like', $request['filter']['kelurahan'] . '%');
        }

        if ($request['filter']['status'] != null) {
            if ($request['filter']['status'] == 1) {
                $response = $response->where('is_active', 1);
            } else {
                $response = $response->where('is_active', 2);
            }
        }

        if (Auth::user()->s_id_hakakses == 4) {
            $response = $response->where('KD_KECAMATAN', Auth::user()->kecamatan);
            $response = $response->where('KD_KELURAHAN', Auth::user()->kelurahan);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'kecamatan' => $v->KD_KECAMATAN,
                'kelurahan' => $v->KD_KELURAHAN,
                'dukuh' => $v->kd_dukuh . ' - ' . $v->nm_dukuh,
                'status' => ($v->is_active == 1) ? 'Aktif' : 'Tidak Aktif',
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'dukuh/edit/' . $v['KD_KECAMATAN'] . $v['KD_KELURAHAN'] . $v['kd_dukuh'],
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => "javascript:showDeleteDialog('" . (string) $v['KD_KECAMATAN'] . $v['KD_KELURAHAN'] . $v['kd_dukuh'] . "')",
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function destroy(Request $request)
    {
        $kode = $request->query('id');
        Dukuh::where([
            ['KD_KECAMATAN', substr($kode, 0, 3)],
            ['KD_KELURAHAN', substr($kode, 3, 3)],
            ['kd_dukuh', substr($kode, 6, 3)],
        ])->delete();
    }

    public function comboKelurahan(Request $request)
    {
        $kelurahan = RefKelurahan::where('KD_KECAMATAN', $request->id)->orderBy('KD_KELURAHAN', 'ASC')->get();
        return response()->json(
            $kelurahan,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }
}
