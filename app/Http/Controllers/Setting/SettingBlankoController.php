<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\BlankoRequest;
use App\Models\Setting\Blanko;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class SettingBlankoController extends Controller
{
    public function index()
    {
        return view('setting-blanko.index', [
            'blanko' => Blanko::orderBy('id', 'asc')->get()
        ]);
    }

    public function detail(Request $request) {
        $res = Blanko::where('id', '=', $request->query('id'))->first();
        return response()->json(
            $res,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function update(BlankoRequest $blankoRequest)
    {
        $attr = $blankoRequest->except(['_token']);
        try {
            //code...
            if ($blankoRequest->hasFile('thumbnail')) {
                $fileExtension = $blankoRequest->file('thumbnail')->getClientOriginalExtension();
                if($fileExtension == 'pdf'){
                    $file_content = 'application';
                }else{
                    $file_content = 'image';
                }
                $files = base64_encode(file_get_contents($blankoRequest->file('thumbnail')));
                $base64 = 'data:'.$file_content.'/' . $fileExtension . ';base64,' . $files;

                // $files = $blankoRequest->file('thumbnail');
                // $destinationPath = 'blanko';
                // $file = Storage::putFile(
                //     $destinationPath,
                //     $files
                // );

                $attr['thumbnail'] = $base64;
                Blanko::where('uuid', $attr['uuid'])->update($attr);
            }

            session()->flash('success', 'Data berhasil di update.');
        } catch (\Throwable $th) {
            session()->flash('error', $th->getMessage());
        }

        return redirect('setting-blanko');
    }
}
