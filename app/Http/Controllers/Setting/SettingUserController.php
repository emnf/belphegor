<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\UserRequest;
use App\Models\User;
use App\Models\Dropdown\Role;
use App\Models\Pbb\Dukuh;
use App\Models\Pbb\RefKecamatan;
use App\Models\Pbb\RefKelurahan;
use App\Models\Setting\Pemda;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class SettingUserController extends Controller
{

    public function index()
    {
        $submit = 'Create';
        $user = new User();
        return view('setting-user.index', compact('submit', 'user'));
    }

    public function tambah()
    {
        $submit = 'Create';
        $user = new User();
        $role = Role::orderBy('urut', 'asc')->get();
        $kecamatan = RefKecamatan::orderBy('KD_KECAMATAN', 'ASC')->get();
        $data_pemda = Pemda::orderBy('updated_at', 'desc')->first();
        return view('setting-user.tambah', compact('submit', 'user', 'role', 'kecamatan', 'data_pemda'));
    }

    public function store(UserRequest $request)
    {
        $attr = $request->except('_token');

        try {
            $attr['uuid'] = Uuid::uuid4()->getHex();
            $attr['password'] = Hash::make($attr['password']);
            User::create($attr);

            $user = User::where('username', request('username'))->first();
            $user->assignRole(request('s_id_hakakses'));

            session()->flash('success', 'Data Berhasil disimpan.');
        } catch (\Throwable $e) {
            dd($e);
            report($e);
            session()->flash('error', $e->getMessage());
        }
        return redirect()->route('setting-user');
    }

    public function edit(User $user, string $uuid)
    {
        return view('setting-user.edit', [
            'user' => $user::where('uuid', $uuid)->first(),
            'role' => Role::orderBy('id', 'asc')->get(),
            'kecamatan' => RefKecamatan::orderBy('KD_KECAMATAN', 'ASC')->get(),
            'data_pemda' => Pemda::orderBy('updated_at', 'desc')->first(),
            'submit' => 'Update'
        ]);
    }

    public function update(User $user, UserRequest $request, string $uuid)
    {
        // $attr = $request->validated();
        $attr = $request->except('_token', '_method');
        try {
            $attr['password'] = Hash::make($attr['password']);
            $user->where('uuid', $uuid)->update($attr);
            $user->syncRoles($attr['s_id_hakakses']);

            session()->flash('success', 'Data Berhasil diupdate.');
        } catch (\Throwable $e) {
            dd($e);
            report($e);
            session()->flash('error', $e->getMessage());
        }
        return redirect()->route('setting-user');
    }

    public function datagrid(Request $request)
    {
        $response = User::select(
            'users.*',
            'roles.name as hakakses'
        )
            ->leftJoin('roles', 'roles.id', '=', 'users.s_id_hakakses');
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('id');
        }

        if ($request['filter']['name'] != null) {
            $response = $response->where('name', 'ilike', "%" . $request['filter']['name'] . "%");
        }
        if ($request['filter']['username'] != null) {
            $response = $response->where('username', 'ilike', "%" . $request['filter']['username'] . "%");
        }
        if ($request['filter']['email'] != null) {
            $response = $response->where('email', 'ilike', "%" . $request['filter']['email'] . "%");
        }
        if ($request['filter']['hakakses'] != null) {
            $response = $response->where('roles.name', 'ilike', "%" . $request['filter']['hakakses'] . "%");
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'name' => $v['name'],
                'username' => $v['username'],
                'email' => $v['email'],
                'hakakses' => $v['hakakses'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => '/setting-user/edit/' . $v['uuid'],
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['id'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $role = User::where('id', '=', $request->query('id'))->get();
        return response()->json(
            $role,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function delete(Request $request)
    {
        User::where('id', '=', $request->query('id'))->delete();
    }

    public function comboKelurahan(Request $request)
    {
        $kelurahan = RefKelurahan::where('KD_KECAMATAN', $request->id)->orderBy('KD_KELURAHAN', 'ASC')->get();
        return response()->json(
            $kelurahan,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function comboDukuh(Request $request)
    {
        $kelurahan = Dukuh::where([
            ['KD_KECAMATAN', $request->id],
            ['KD_KELURAHAN', $request->key]
        ])->orderBy('KD_DUKUH', 'ASC')->get();
        return response()->json(
            $kelurahan,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }
}
