<?php

namespace App\Http\Controllers\Setting;

use App\Exports\Setting\SettingLoginBackgroundExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\LoginBackgroundRequest;
use App\Models\Dropdown\Status;
use App\Models\Setting\LoginBackground;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class SettingLoginBackground extends Controller
{
    public function index()
    {
        return view('setting-login-background.index', [
            'logins' => LoginBackground::orderBy('updated_at', 'desc')->get(),
            'status' => Status::get(),
        ]);
    }

    public function create()
    {
        return view('setting-login-background.create', [
            'logins' => new LoginBackground(),
            'status' => Status::get(),
            ]);
    }

    public function store(LoginBackgroundRequest $loginBackgroundRequest)
    {
        $attr = $loginBackgroundRequest->all();

        $attr['s_id_status'] = request('status');

        if ($loginBackgroundRequest->hasFile('thumbnail')) {
            // $thumbnail = request()->file('thumbnail');
            // // $thumbnailUrl = $thumbnail->store("images/login-background");
            // $thumbnailUrl = $thumbnail->storeAs("images/login-background", "{$imageName}.{$thumbnail->extension()}");

            $fileExtension = $loginBackgroundRequest->file('thumbnail')->getClientOriginalExtension();
            if($fileExtension == 'pdf'){
                $file_content = 'application';
            }else{
                $file_content = 'image';
            }
            $files = base64_encode(file_get_contents($loginBackgroundRequest->file('thumbnail')));
            $base64 = 'data:'.$file_content.'/' . $fileExtension . ';base64,' . $files;

            $thumbnailUrl = $base64;
        } else {
            $thumbnailUrl = null;
        }

        $attr['s_thumbnail'] = $thumbnailUrl;
        $create = LoginBackground::create($attr);

        if($create){
            session()->flash('success', 'Data baru telah ditamabahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-login-background');

    }

    public function datagrid(Request $request)
    {
        $response = new LoginBackground();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('id');
        }
        if ($request['filter']['thumbnail'] != null) {
            $response = $response->where('s_thumbnail', 'ilike', "%" . $request['filter']['thumbnail'] . "%");
        }
        if ($request['filter']['status'] != null) {
            $response = $response->where('s_id_status', 'like', "%" . $request['filter']['status'] . "%");
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'thumbnail'=> '<img src="'. $v['s_thumbnail'] .'" style="width: 50px; object-position: center; border-radius:20px ">',
                'status' => $v->status['s_nama_status'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-login-background/' . $v['id'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['id'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $detail = LoginBackground::where('id', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function edit(LoginBackground $loginBackground)
    {
        // dd($loginBackground);
        return view('setting-login-background.edit', [
            'logins' => $loginBackground,
            'status' => Status::get(),
        ]);
    }

    public function update(LoginBackgroundRequest $loginBackgroundRequest, LoginBackground $loginBackground)
    {
        $attr = $loginBackgroundRequest->all();

        $attr['s_id_status'] = request('status');

        if ($loginBackgroundRequest->hasFile('thumbnail')) {
            // Storage::delete($loginBackground->s_thumbnail);
            // $thumbnail = request()->file('thumbnail');
            // $thumbnailUrl = $thumbnail->store("images/login-background");

            $fileExtension = $loginBackgroundRequest->file('thumbnail')->getClientOriginalExtension();
            if($fileExtension == 'pdf'){
                $file_content = 'application';
            }else{
                $file_content = 'image';
            }
            $files = base64_encode(file_get_contents($loginBackgroundRequest->file('thumbnail')));
            $base64 = 'data:'.$file_content.'/' . $fileExtension . ';base64,' . $files;

            $thumbnailUrl = $base64;

        } else {
            $thumbnailUrl = $loginBackground->s_thumbnail;
        }

        $attr['s_thumbnail'] = $thumbnailUrl;

        $update = $loginBackground->update($attr);

        if($update){
            session()->flash('success', 'Background Login Berhasil diupdate!.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-login-background');
    }

    public function destroy(Request $request)
    {
        Storage::delete($request->s_thumbnail);
        LoginBackground::where('id', '=', $request->query('id'))->delete();
    }

}
