<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingPassController extends Controller
{
    //
    public function index()
    {
        return view('ubah-pass.index');
    }

    public function updatepass(Request $request){
        $a = Auth::user()->id ;
        $request->validate([
            'old_password' => 'required|min:8|max:100',
            'new_password' => 'required|min:8|max:100',
            'confirm_password' => 'required|same:new_password',
            ]);
            
            // $attr = $request->all();    
            $user = auth()->user();
            // dd(Hash::check($request->old_password, $user->password));
            // dd($user->getAuthPassword());
        if(Hash::check($request->old_password, $user->password )){
            $user->update([
                // $attr['password'] = Hash::make($attr['password']);
                'password' => bcrypt($request->new_password)
            ]);

            session()->flash('success','Password Berhasil di Perbarui');
        }else{
            session()->flash('error','Password lama tidak sama');
        }
        return redirect('ubah-pass/index');
    }
}
  