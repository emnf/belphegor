<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingProfileController extends Controller
{
    public function index()
    {
        return view('profile.index');
    }

    public function update(Request $request)
    {
        try {
            User::where('id', Auth::user()->id)->update([
                'name' => $request->name,
                'email' => $request->email
            ]);
            session()->flash('success', 'Profil Berhasil diupdate.');
        } catch (\Throwable $th) {
            session()->flash('error', $th->getMessage());
        }

        return redirect('profile');
    }
}
