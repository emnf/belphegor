<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Setting\SettingText;
use Illuminate\Http\Request;

class TextController extends Controller
{
    public function index()
    {
        return view('setting-text.index');
    }

    public function datagrid(Request $request)
    {
        $response = new SettingText();
        // dd($response);
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('id');
        }

        if ($request['filter']['name'] != null) {
            $response = $response->where('s_text', 'like', "%" . $request['filter']['name'] . "%");
        }
        if ($request['filter']['s_aktif'] != null) {
            if ($request['filter']['s_aktif'] == 1) {
                $status = true;
            } else {
                $status = false;
            }
            $response = $response->where('s_aktif', $status);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'name' => $v['s_text'],
                'guard_name' => ($v['s_aktif'] == true ? "Aktif" : 'Nonaktif'),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'javascript:showEditDialog(' . $v['id'] . ')',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['id'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function store(Request $request)
    {
        if (request('id') != null) {
            SettingText::whereId(request('id'))->update([
                's_text' => request('name'),
                's_aktif' => request('s_aktif') == 1 ? true : false,
            ]);
        } else {
            SettingText::create([
                's_text' => request('name'),
                's_aktif' => request('s_aktif') == 1 ? true : false,
            ]);
        }

        return back();
    }

    public function detail($id)
    {
        $response = SettingText::whereId($id)->first();
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function delete(Request $request)
    {
        SettingText::whereId($request->id)->delete();
    }
}
