<?php

namespace App\Http\Controllers;

use App\Http\Traits\FormatNOP;
use App\Models\Pbb\Dukuh;
use App\Models\Pbb\InputSpptTersampaikan;
use App\Models\Pbb\PembayaranSppt;
use App\Models\Pbb\RefKecamatan;
use App\Models\Pbb\RefKelurahan;
use App\Models\Pbb\Sppt;
use App\Models\RegisterNop\RegisterNop;
use Illuminate\Http\Request;
use App\Models\Setting\Pemda;
use App\Models\View\KetetapanKecamatanView;
use App\Models\View\KetetapanKelurahanView;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    use FormatNOP;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data_pemda = Pemda::orderBy('updated_at', 'desc')->first();

        return view('home', ['data_pemda' => $data_pemda]);
    }
}
