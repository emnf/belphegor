<?php

namespace App\Http\Controllers\Permissions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::get();
        $role = new Role;
        return view('permission.roles.index', compact('roles', 'role'));
    }

    public function store()
    {
        request()->validate([
            'name' => 'required'
        ]);

        Role::create([
            'name' => request('name'),
            'guard_name' => request('guard_name') ?? 'web'
        ]);

        return back();
    }

    public function edit(Role $role)
    {
        return view('permission.roles.edit', [
            'role' => $role,
            'submit' => 'Update'
        ]);
    }

    public function update(Role $role)
    {
        request()->validate([
            'name' => 'required'
        ]);

        $role->update([
            'name' => request('name'),
            'guard_name' => request('guard_name') ?? 'web'
        ]);

        return redirect()->route('roles.index');
    }

    public function datagrid(Request $request)
    {
        $response = new Role;
        // dd($response);
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('id');
        }

        if ($request['filter']['name'] != null) {
            $response = $response->where('name', 'like', "%" . $request['filter']['name'] . "%");
        }
        if ($request['filter']['guard_name'] != null) {
            $response = $response->where('guard_name', 'like', "%" . $request['filter']['guard_name'] . "%");
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'name' => $v['name'],
                'guard_name' => $v['guard_name'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'roles/' . $v['id'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['id'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $role = Role::where('id', '=', $request->query('id'))->get();
        return response()->json(
            $role,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function delete(Request $request){
        Role::where('id', '=', $request->query('id'))->delete();
    }
}
