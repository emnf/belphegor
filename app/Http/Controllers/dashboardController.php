<?php

namespace App\Http\Controllers;

use App\Models\Bphtb\RealisasiBphtb;
use App\Models\Pbb\RealisasiPBB;
use App\Models\Setting\Pemda;
use App\Models\Setting\SettingText;
use App\Models\Simpatda\Pembayaran;
use App\Models\Simpatda\Realisasi;
use App\Models\Simpatda\TargetRealisasi;
use App\Models\Simpatda\Transaksi;
use Illuminate\Http\Request;
use MenuHelper;

class dashboardController extends Controller
{
    public function index()
    {
        $data['data_target'] = (new TargetRealisasi())->target();
        $data['data_pemda'] = Pemda::orderBy('updated_at', 'desc')->first();
        return view('dashboard.index', $data);
    }
    public function penerimaanHarian(Request $request)
    {
        $data['text'] = SettingText::where('s_aktif', true)->get();

        $target = TargetRealisasi::whereId($request->target)->first();

        $data['perJenis'] = $this->perJenis($target);
        $data['perJenisHarian'] = $this->perJenisHarian($target);
        $data['simpatdaHarian'] = $this->simpatdaHarian();
        $data['bphtbHarian'] = $this->bphtbHarian();
        $data['pbbHarian'] = $this->pbbHarian();
        $data['tahun'] = $target->t_tahun;

        return view('dashboard.harian', $data);
    }

    public function perJenis($target)
    {
        $realSIMPATDA = (new Realisasi())->getDataRealisasiJenis($target)->toArray();
        $realPBB = (new RealisasiPBB())->dataRealPBB($target)->toArray();
        $realPBB['target'] = (new Realisasi())->targetPbb(10, $target)['target'];
        $realBPHTB = (new RealisasiBphtb())->getRealisasiBPHTB($target)->toArray();
        $realBPHTB['target'] = (new Realisasi())->targetPbb(11, $target)['target'];

        $realisasi = array_merge($realSIMPATDA, [$realPBB], [$realBPHTB]);

        usort($realisasi, function ($a, $b) {
            return $a['id'] - $b['id'];
        });

        $data = "<tbody id='dataPerjenis'>";
        $no = 1;
        $target = 0;
        $real = 0;
        foreach ($realisasi as $value) {
            $num = $no++;
            if ($value['target'] == 0) {
                if ($value['realisasi'] == 0) {
                    $persentase = 0;
                } else {
                    $persentase = 100;
                }
            } else {
                $persentase = $value['realisasi'] / $value['target'] * 100;
            }
            $persen = $persentase > 100 ? 100 : $persentase;
            $data .= "<tr>
                    <td style='background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: left;color: white;font-size: 12pt;margin-left: 20px;'>&nbsp;&nbsp;&nbsp;&nbsp;" . str_pad($num, 2, "0", STR_PAD_LEFT) . ". " . $value['s_nama'] . "</td>
                    <td style='background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: right;color: white;font-size: 12pt;'>" . number_format($value['target'], 0, ',', '.') . "&nbsp;&nbsp;</td>
                    <td style='background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: right;color: white;font-size: 12pt;'>" . number_format($value['realisasi'], 0, ',', '.') . "&nbsp;&nbsp;</td>
                    <td style='background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: center;color: white;font-size: 12pt;'>" . $persen . "</td>
                </tr>";
            $target += $value['target'];
            $real += $value['realisasi'];
        }
        if ($target == 0) {
            if ($real == 0) {
                $persentase2 = 0;
            } else {
                $persentase2 = 100;
            }
        } else {
            $persentase2 = $real / $target * 100;
        }
        $persen2 = $persentase2 > 100 ? 100 : $persentase2;
        $data .= "
            </tbody>
            <tfoot>
                <tr>
                    <td style='background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: yellow;font-size: 12pt;'>
                        Total Realisasi
                    </td>
                    <td style='background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: right;color: yellow;font-size: 12pt;'>
                        " . number_format($target, 0, ',', '.') . "&nbsp;&nbsp;
                    </td>
                    <td style='background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: right;color: yellow;font-size: 12pt;'>
                        " . number_format($real, 0, ',', '.') . "&nbsp;&nbsp;
                    </td>
                    <td style='background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: yellow;font-size: 12pt;'>
                        " . $persen2 . "
                    </td>
                </tr>
            </tfoot>
        ";

        return $data;
    }
    public function perJenisHarian($target)
    {
        $realSIMPATDA = (new Realisasi())->getDataRealisasiJenisHarian()->toArray();
        $realPBB = (new RealisasiPBB())->dataRealPBBHarian()->toArray();
        $realPBB['target'] = (new Realisasi())->targetPbb(10, $target)['target'];
        $realBPHTB = (new RealisasiBphtb())->getRealisasiBPHTBHarian()->toArray();
        $realBPHTB['target'] = (new Realisasi())->targetPbb(11, $target)['target'];

        $realisasi = array_merge($realSIMPATDA, [$realPBB], [$realBPHTB]);

        usort($realisasi, function ($a, $b) {
            return $a['id'] - $b['id'];
        });

        $data = "<tbody id='dataPerjenisPerhari'>";
        $no = 1;
        $target = 0;
        $real = 0;
        foreach ($realisasi as $value) {
            $num = $no++;
            if ($value['target'] == 0) {
                if ($value['realisasi'] == 0) {
                    $persentase = 0;
                } else {
                    $persentase = 100;
                }
            } else {
                $persentase = $value['realisasi'] / $value['target'] * 100;
            }
            $persen = $persentase > 100 ? 100 : $persentase;
            $data .= "
                    <tr>
                        <td style='width: 60%; border: 1px solid white; border-collapse: collapse; text-align: left; background:#bb00ffab;color: white;font-size: 15pt;'>&nbsp;&nbsp;&nbsp;&nbsp;
                            " . $value['s_nama_singkat'] . "
                        </td>
                        <td style='width: 5%; border: 1px solid white; border-collapse: collapse; text-align: right; background:#bb00ffab;color: white;font-size: 15pt;'>
                            " . number_format($value['realisasi'], 0, ',', '.') . "&nbsp;&nbsp;
                        </td>
                    </tr>
                ";
            $target += $value['target'];
            $real += $value['realisasi'];
        }
        if ($target == 0) {
            if ($real == 0) {
                $persentase2 = 0;
            } else {
                $persentase2 = 100;
            }
        } else {
            $persentase2 = $real / $target * 100;
        }
        $persen2 = $persentase2 > 100 ? 100 : $persentase2;
        $data .= "
            </tbody>
                <tfoot>
                <tr>
                    <td style='width: 5%; border: 1px solid white; border-collapse: collapse; text-align: center; background:#7706a0ef;color: yellow;font-size: 15pt;'>
                        Total
                    </td>
                    <td style='width: 5%; border: 1px solid white; border-collapse: collapse; text-align: center; background:#7706a0ef;color: yellow;font-size: 15pt;'>
                        " . number_format($real, 0, ',', '.') . "
                    </td>
                </tr>
            </tfoot>
        ";

        return $data;
    }

    public function simpatdaHarian()
    {
        $real = (new Transaksi())->harianSimpatda();
        $numRow = count($real);
        $remainingRows = 8 - $numRow;
        $data = "";
        for ($i = 0; $i < $remainingRows; $i++) {
            $real[] = array(
                't_nama' => '',
                's_nama' => '',
                'realisasi' => null,
                'id' => ''
            );
        }
        $totalHariIni = 0;
        foreach ($real as $item) {
            $namaWp = $item['t_nama'] ?? null;
            $realisasi = $item['realisasi'] != null ? 'Rp. ' . number_format($item['realisasi'], 0, ',', '.') : '';
            $totalHariIni += $item['realisasi'];
            $data .= "
                <tbody id='simpatdaHarian'>
                    <tr>
                        <td style=' background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: left;color: white;font-size: 12pt;'>
                            &nbsp;&nbsp;&nbsp;&nbsp;" . $namaWp . " <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;" . $item['s_nama'] . "
                        </td>
                        <td style=' background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: right;color: white;font-size: 12pt;'>
                             " . $realisasi . "&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </tbody>
                ";
        }
        $data .= "
                <tfoot>
                    <tr>
                        <td style=' background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: yellow;font-size: 12pt;'>
                            Total (Rp.)
                        </td>
                        <td style=' background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: right;color: yellow;font-size: 12pt;'>
                            " . number_format($totalHariIni, 0, ',', '.') . "&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </tfoot>
        ";
        return $data;
    }

    public function bphtbHarian()
    {
        $real = (new RealisasiBphtb())->harianBphtb();

        $numRow = count($real);
        $remainingRows = 8 - $numRow;
        $data = "";
        for ($i = 0; $i < $remainingRows; $i++) {
            $real[] = array(
                't_nama' => '',
                's_nama' => '',
                'realisasi' => null,
                'id' => ''
            );
        }
        $totalHariIni = 0;
        foreach ($real as $item) {
            $namaWp = $item['t_nama'] ?? null;
            $realisasi = $item['realisasi'] != null ? 'Rp. ' . number_format($item['realisasi'], 0, ',', '.') : '';
            $totalHariIni += $item['realisasi'];
            $data .= "
                <tbody id='bphtbHarian'>
                    <tr>
                        <td style=' background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: left;color: white;font-size: 12pt;'>
                            &nbsp;&nbsp;&nbsp;&nbsp;" . $namaWp . " <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;" . $item['s_nama'] . "
                        </td>
                        <td style=' background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: right;color: white;font-size: 12pt;'>
                             " . $realisasi . "&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </tbody>
                ";
        }
        $data .= "
                <tfoot>
                    <tr>
                        <td style=' background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: yellow;font-size: 12pt;'>
                            Total (Rp.)
                        </td>
                        <td style=' background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: right;color: yellow;font-size: 12pt;'>
                            " . number_format($totalHariIni, 0, ',', '.') . "&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </tfoot>
        ";
        return $data;
    }

    public function pbbHarian()
    {
        $real = (new RealisasiPbb())->HarianPbb();

        $numRow = count($real);
        $remainingRows = 8 - $numRow;
        $data = "";
        for ($i = 0; $i < $remainingRows; $i++) {
            $real[] = array(
                'NM_WP_SPPT' => '',
                's_nama' => '',
                'tahunsppt' => null,
                'realisasi' => null,
                'id' => ''
            );
        }
        $totalHariIni = 0;
        foreach ($real as $item) {
            $namaWp = $item['NM_WP_SPPT'] ?? null;
            $realisasi = $item['realisasi'] != null ? 'Rp. ' . number_format($item['realisasi'], 0, ',', '.') : '';
            $tahun = $item['tahunsppt'] != null ? ' | Tahun ' . $item['tahunsppt'] : '';
            $nop = $item['s_nama'] != '' ? (new MenuHelper())->nopString($item['s_nama']) : '';
            $totalHariIni += $item['realisasi'];

            $data .= "
                <tbody id='pbbHarian'>
                    <tr>
                        <td style=' background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: left;color: white;font-size: 12pt;'>
                            &nbsp;&nbsp;&nbsp;&nbsp;" . $namaWp . " <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;" . $nop . $tahun . "
                        </td>
                        <td style=' background:#bb00ffab;border: 1px solid white; border-collapse: collapse; text-align: right;color: white;font-size: 12pt;'>
                             " . $realisasi . "&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </tbody>
                ";
        }
        $data .= "
                <tfoot>
                    <tr>
                        <td style=' background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: yellow;font-size: 12pt;'>
                            Total (Rp.)
                        </td>
                        <td style=' background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: right;color: yellow;font-size: 12pt;'>
                            " . number_format($totalHariIni, 0, ',', '.') . "&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </tfoot>
        ";
        return $data;
    }
}
