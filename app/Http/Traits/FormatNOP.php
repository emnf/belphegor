<?php

namespace App\Http\Traits;

trait FormatNOP
{

    function nop($data)
    {

        $nop = $data['KD_PROPINSI']
            . '.' . $data['KD_DATI2']
            . '.' . $data['KD_KECAMATAN']
            . '.' . $data['KD_KELURAHAN']
            . '.' . $data['KD_BLOK']
            . '.' . $data['NO_URUT']
            . '.' . $data['KD_JNS_OP'];
        return $nop;
    }

    function nopString($data)
    {

        $nop = substr($data, 0, 2)
            . '.' . substr($data, 2, 2)
            . '.' . substr($data, 4, 3)
            . '.' . substr($data, 7, 3)
            . '.' . substr($data, 10, 3)
            . '.' . substr($data, 13, 4)
            . '.' . substr($data, 17, 1)
            . '.' . substr($data, 18, 4);
        return $nop;
    }

    function nopNotFormat($data)
    {

        $nop = $data['KD_PROPINSI']
            . $data['KD_DATI2']
            . $data['KD_KECAMATAN']
            . $data['KD_KELURAHAN']
            . $data['KD_BLOK']
            . $data['NO_URUT']
            . $data['KD_JNS_OP'];
        return $nop;
    }
}
