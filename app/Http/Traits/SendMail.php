<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Mail;

trait SendMail
{
    private function sendMail($name, $fileDirectory, $to, $fileName)
    {
        $data = [
            'name' => $name,
            'fileDirectory' => $fileDirectory,
            'fileName' => $fileName
        ];
        Mail::send('cek-status.mail.mailtemplate', $data, function ($message) use ($to, $fileDirectory) {
            $message->to($to)->subject('No-Reply : Elektronik File SPPT & STTS KABUPATEN FAKFAK');
            $message->attach($fileDirectory, ['mime' => 'application/pdf']);
        });
        return true;
    }
}
