<?php

namespace App\Http\Traits;

use App\Models\Pbb\TrxQrisIndividu;

trait ApiQRIS
{
    function createTokenQRIS()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://sandbox.bpddiy.co.id/auth',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "username": "bkad.bantul",
            "password": "l1pq*tlfsm"
        }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response, true);
    }

    public function getQR($createToken, $total)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://sandbox.bpddiy.co.id/qris/get_qr',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "nmid": "ID1234567890123",
            "terminal_label": "A20",
            "amount": "' . $total . '",
            "callback_url": "http://103.135.180.36:81/api/callbackPaymentPbb",
            "invoice_id": "' . $this->generateRandomInvoiceId(12) . '",
            "expired_time": "30"
        }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $createToken['token'],
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response, true);
    }

    function createQR($data, $trxqris)
    {
        $createToken = $this->createTokenQRIS();

        $total = $data->PBB_YG_HRS_DIBAYAR_SPPT + $data->denda;

        if ($trxqris) {
            if (date('Y-m-d H:i:s') < date('Y-m-d H:i:s', strtotime($trxqris->expired_qris))) {
                $message = 'QRIS masih aktif, Tanggal Jatuh Tempo QRIS ' . date('Y-m-d H:i:s', strtotime($trxqris->expired_qris));
                session()->flash('info', $message);
                $response = [
                    "qr_string" => $trxqris->qr_string,
                    "expired_qris" => $trxqris->expired_qris,
                    "invoice_id" => $trxqris->invoice_id
                ];
            } else {
                $decode = $this->getQR($createToken, $total);

                if ($decode['status'] == '00') {
                    TrxQrisIndividu::where([
                        ['KD_PROPINSI', '=', $data->KD_PROPINSI],
                        ['KD_DATI2', '=', $data->KD_DATI2],
                        ['KD_KECAMATAN', '=', $data->KD_KECAMATAN],
                        ['KD_KELURAHAN', '=', $data->KD_KELURAHAN],
                        ['KD_BLOK', '=', $data->KD_BLOK],
                        ['NO_URUT', '=', $data->NO_URUT],
                        ['KD_JNS_OP', '=', $data->KD_JNS_OP],
                        ['thn_pajak_sppt', '=', $data->thn_pajak_sppt]
                    ])->update([
                        'callback_token' => $createToken['callback_token'],
                        'qr_string' => $decode['qr_string'],
                        'invoice_id' => $decode['invoice_id'],
                        'expired_qris' => date('Y-m-d H:i:s', strtotime("+{$decode['expired_time']} minutes")),
                        'created_qris' => date('Y-m-d H:i:s', strtotime($decode['created_at']))
                    ]);

                    $response = [
                        "qr_string" => $decode['qr_string'],
                        "expired_qris" => date('Y-m-d H:i:s', strtotime("+{$decode['expired_time']} minutes")),
                        "invoice_id" => $decode['invoice_id'],
                        "expired_time" => $decode['expired_time'],
                        "created_at" => $decode['created_at']
                    ];
                } else {
                    $response = $decode['message'];
                    session()->flash('error', $response);
                }
            }
        } else {

            if ($createToken['status'] == '00') {

                $decode = $this->getQR($createToken, $total);

                if ($decode['status'] == '00') {

                    TrxQrisIndividu::insert([
                        'KD_PROPINSI' => $data->KD_PROPINSI,
                        'KD_DATI2' => $data->KD_DATI2,
                        'KD_KECAMATAN' => $data->KD_KECAMATAN,
                        'KD_KELURAHAN' => $data->KD_KELURAHAN,
                        'KD_BLOK' => $data->KD_BLOK,
                        'NO_URUT' => $data->NO_URUT,
                        'KD_JNS_OP' => $data->KD_JNS_OP,
                        'thn_pajak_sppt' => $data->thn_pajak_sppt,
                        'callback_token' => $createToken['callback_token'],
                        'qr_string' => $decode['qr_string'],
                        'invoice_id' => $decode['invoice_id'],
                        'expired_qris' => date('Y-m-d H:i:s', strtotime("+{$decode['expired_time']} minutes")),
                        'created_qris' => date('Y-m-d H:i:s', strtotime($decode['created_at']))
                    ]);

                    $response = [
                        "qr_string" => $decode['qr_string'],
                        "expired_qris" => date('Y-m-d H:i:s', strtotime("+{$decode['expired_time']} minutes")),
                        "invoice_id" => $decode['invoice_id'],
                        "expired_time" => $decode['expired_time'],
                        "created_at" => $decode['created_at']
                    ];
                } else {
                    $response = $decode['message'];
                    session()->flash('error', $response);
                }
            }
        }

        return $response;
    }

    function generateRandomInvoiceId($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
