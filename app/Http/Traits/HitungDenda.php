<?php

namespace App\Http\Traits;

trait HitungDenda
{

    function hitungDenda($tgltempo, $jmlhpajak) {
        $tgljatuhtempo = date('Y-m-d', strtotime($tgltempo));
        $tglsekarang = date('Y-m-d');
        $ts1 = strtotime($tgljatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);
        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        } else {
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        }

        return round($jmldenda);
    }
}