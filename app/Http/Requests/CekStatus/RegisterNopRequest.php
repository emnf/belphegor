<?php

namespace App\Http\Requests\CekStatus;

use Illuminate\Foundation\Http\FormRequest;

class RegisterNopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nop' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nop.required' => 'NOP tidak boleh kosong.'
        ];
    }
}
