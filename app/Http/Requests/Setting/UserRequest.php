<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->input('s_id_hakakses') == 3) {
            $rules = [
                'nik' => ['required', 'string', 'max:20'],
                'name' => ['required', 'string', 'max:255'],
                's_id_hakakses' => ['required']
            ];
        } elseif ($this->input('s_id_hakakses') == 4) {
            $rules = [
                'kecamatan' => ['required', 'string'],
                'kelurahan' => ['required', 'string'],
                'name' => ['required', 'string', 'max:255'],
                's_id_hakakses' => ['required']
            ];
        } elseif ($this->input('s_id_hakakses') == 6) {
            $rules = [
                'kecamatan' => ['required', 'string'],
                'name' => ['required', 'string', 'max:255'],
                's_id_hakakses' => ['required']
            ];
        } else {
            $rules = [
                'name' => ['required', 'string', 'max:255'],
                's_id_hakakses' => ['required']
            ];
        }

        if (!in_array($this->method(), ['PUT', 'PATCH'])) {
            $rules['username'] = ['required', 'string', 'unique:users,username', 'max:255'];
            $rules['password'] = ['required', 'string', 'min:8'];
            $rules['email'] = ['required', 'string', 'email', 'unique:users,email', 'max:255'];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Kolom harus diisi.',
            'username.required' => 'Kolom harus diisi.',
            'username.unique' => 'Username ini sudah digunakan',
            'email.required' => 'Kolom harus diisi.',
            'email.unique' => 'Email ini sudah terdaftar',
            'password.required' => 'Kolom harus diisi.',
            's_id_hakakses.required' => 'Kolom harus diisi.',
            'nik.required' => 'Kolom harus diisi.',
            'kelurahan.required' => 'Kolom harus diisi.',
            'kecamatan.required' => 'Kolom harus diisi.',
            'dukuh.required' => 'Kolom harus diisi.'
        ];
    }
}
