<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class DukuhRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'KD_KECAMATAN' => 'required',
            'KD_KELURAHAN' => 'required',
            'kd_dukuh' => 'required',
            'nm_dukuh' => 'required',
            'is_active' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'KD_KECAMATAN.required' => 'Kecamatan Harus diisi',
            'KD_KELURAHAN.required' => 'Kelurahan  Harus diisi',
            'kd_dukuh.required' => 'Kode Dukuh Harus diisi',
            'nm_dukuh.required' => 'Nama Dukuh Harus diisi',
            'is_active.required' => 'Status Harus diisi',
        ];
    }
}
