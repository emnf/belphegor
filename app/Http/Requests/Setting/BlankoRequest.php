<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class BlankoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thumbnail' => 'required|mimes:jpeg,jpg,png|max:1024'
        ];
    }

    public function messages()
    {
        return [
            'thumbnail.required' => 'Background boleh kosong',
            'mimes' => 'type file logo hanya boleh JPEG, JPG, & PNG',
            'uploaded' => 'Ukuran file tidak boleh lebih dari 1MB',
        ];
    }
}
