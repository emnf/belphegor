<?php

namespace App\Exports\InputSppt;

use App\Models\Pbb\Sppt;
use App\Models\Setting\Pemda;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class InputSpptExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $request;
    function __construct($request)
    {
        $this->request = $request;

    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class =>function(BeforeExport $event){
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class => function(AfterSheet $event){
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View{
        $response = (new Sppt())->getDataSpptTersampaikan($this->request)->get();

        $ar_pemda = Pemda::orderBy('updated_at', 'desc')->first();

        return view('input-sppt.exports.exportdata',[
            'pemda' => $ar_pemda,
            'data'   => $response,
            'request' => $this->request
        ]);
    }
}