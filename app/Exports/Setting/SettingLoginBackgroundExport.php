<?php

namespace App\Exports\Setting;

use Illuminate\Contracts\View\View;
use App\Models\Setting\LoginBackground;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingLoginBackgroundExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $s_thumbnail;
    protected $d_status_id;
    protected $created_at;
    function __construct($s_thumbnail, $d_status_id, $created_at)
    {
        $this->thumbnail = $s_thumbnail;
        $this->status = $d_status_id;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = LoginBackground::orderBy('id');
        if ($this->thumbnail != null) {
            $response = $response->where('s_thumbnail', 'like', "%" . $this->thumbnail . "%");
        }
        if ($this->status != null) {
            $response = $response->where('d_status_id', 'like', "%" . $this->status . "%");
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->get();
        return view('setting-login-background.exports.export', [
            'logins' => $response
        ]);
    }
}
