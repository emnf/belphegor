<?php

namespace App\Exports\Report;

use App\Models\Pbb\RefKecamatan;
use App\Models\Setting\Pemda;
use App\Models\View\KetetapanKelurahanView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RekapKetetapanBlokExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $request;
    function __construct($request)
    {
        $this->request = $request;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View
    {

        $response = (new KetetapanKelurahanView())->getRekapitulasiBlok($this->request);

        $ar_kec = RefKecamatan::where('KD_KECAMATAN', $this->request->kec)->first();
        $ar_pemda = Pemda::orderBy('updated_at', 'desc')->first();

        return view('report.exports.cetakrekapketetapanblok', [
            'pemda' => $ar_pemda,
            'kecamatan' => $ar_kec,
            'kelurahan' => $response[0]->nm_kelurahan,
            'data'   => $response,
            'request' => $this->request
        ]);
    }
}
