<?php

namespace App\Exports\Report;

use App\Models\Setting\Pemda;
use App\Models\View\RealisasiView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RekapRealisasiExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $request;
    function __construct($request)
    {
        $this->request = $request;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View
    {
        $response = (new RealisasiView())->getRekapitulasi($this->request);

        if ($this->request->kel != null) {
            $response = $response->where('sppt.KD_KELURAHAN', $this->request->kel);
        }
        $response->orderBy('KD_KELURAHAN');
        $response->orderBy('KD_BLOK');

        $ar_pemda = Pemda::orderBy('updated_at', 'desc')->first();

        return view('report.exports.cetakrekaprealisasi', [
            'pemda' => $ar_pemda,
            'data'   => $response->get(),
            'request' => $this->request
        ]);
    }
}
