<?php

namespace App\Exports\Report;

use App\Models\Pbb\Sppt;
use App\Models\Setting\Pemda;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Pbb\RefKecamatan;
use App\Models\Pbb\RefKelurahan;

class RekapRealisasiPernopExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $request;
    function __construct($request)
    {
        $this->request = $request;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View
    {
        $request = $this->request;
        $kecamatan = RefKecamatan::where([
            ['KD_KECAMATAN', $request->kec]
        ])->first();

        $kelurahan = RefKelurahan::where([
            ['KD_KECAMATAN',  $request->kec],
            ['KD_KELURAHAN', $request->kel]
        ])->first();
        $request['filter'] = [
            'kecamatan' => $request->kec,
            'kelurahan' => $request->kel,
            'tahun' => $request->tahun,
            'tglbayar' => $request->tglbayar,
        ];
        // $request['filter']=$request->all();
        // dd($request);
        $response = (new Sppt())->getAll($request, '1');

        $response = $response->orderBy('sppt.kd_kecamatan');
        $response = $response->orderBy('sppt.kd_kelurahan');
        $response = $response->orderBy('sppt.kd_blok');
        $response = $response->orderBy('sppt.no_urut');
        $response = $response->orderBy('sppt.thn_pajak_sppt');

        $ar_pemda = Pemda::orderBy('updated_at', 'desc')->first();

        return view('report.exports.cetakrekaprealisasipernop', [
            'pemda' => $ar_pemda,
            'data'   => $response->get(),
            'request' => $this->request,
            'kecamatan' => $kecamatan
        ]);
    }
}
