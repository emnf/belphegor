<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Models\Pbb\PembayaranKodebayar;
use App\Models\Pbb\LogPostingKolektif;

class RunTaskKolektif extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runtask:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data payment kolektif to Database PBB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        $date = date_create(date('d-m-Y H:i:s'));
        date_add($date, date_interval_create_from_date_string('-1 hours'));

        $paymentDate = date('d-m-Y');
        $startTime = date_format($date, 'H:i');
        $endTime = date('H:i');

        $payment = PembayaranKodebayar::select()
                ->whereRaw("TO_CHAR(TGL_BAYAR_BANK, 'DD-MM-YYYY HH24:MI') BETWEEN '" . $paymentDate . " " . $startTime . "' AND '" . $paymentDate . " " . $endTime . "' ")
                ->whereNull("FLAG_DB")
                ->get();

        if ($payment->count() == 0) {
            $message = 'TIDAK ADA DATA YANG DI UPDATE KE DATABASE';
        } else {
            $create = [];
            foreach ($payment as $row):

                $create = LogPostingKolektif::create([
                            'KODE_BAYAR' => $row->kode_bayar,
                            'TGL_BAYAR_BANK' => date('Y-m-d H:i:s', strtotime($row->tgl_bayar_bank)),
                            'TOTAL_POKOK' => $row->total_pokok,
                            'TOTAL_DENDA' => $row->total_denda,
                            'JUMLAH_TOTAL' => $row->jumlah_total,
                            'TGL_POSTING' => Carbon::now(),
                            'STATUS_RESPONSE' => 'SUCCESS',
                            'GW_REFNUM' => $row->gw_refnum,
                            'SWITCHER_ID' => $row->switcher_id,
                            'SETTLEMENT_DATE' => date('Y-m-d H:i:s', strtotime($row->settlement_date)),
                            'KD_BANK' => $row->kd_bank
                ]);

                PembayaranKodebayar::where('KODE_BAYAR', $row->kode_bayar)->update(['FLAG_DB' => 1]);
                
            endforeach;


            if (!empty($create)) {
                $message = 'PAYMENT BERHASIL DI POSTING KE DATABASE';
            } else {
                $message = 'TIDAK ADA DATA YANG DI UPDATE KE DATABASE';
            }
        }

        echo $message;
        Log::info($message);
//        echo 'RunTask Kolektif PBB Running.....';
//        Log::info("RunTask Kolektif PBB Running");
//        return 0;
    }

}
