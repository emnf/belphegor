<?php

class MenuHelper
{

    public static function namaBulan($a)
    {
        $aBulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'Nopember',
            '12' => 'Desember',
        ];
        return $aBulan[$a];
    }

    public static function nop($data)
    {

        $nop = $data['KD_PROPINSI']
            . '.' . $data['KD_DATI2']
            . '.' . $data['KD_KECAMATAN']
            . '.' . $data['KD_KELURAHAN']
            . '.' . $data['KD_BLOK']
            . '.' . $data['NO_URUT']
            . '.' . $data['KD_JNS_OP'];
        return $nop;
    }

    public static function nopFormat($data)
    {

        $nop = $data['KD_PROPINSI']
            . $data['KD_DATI2']
            . $data['KD_KECAMATAN']
            . $data['KD_KELURAHAN']
            . $data['KD_BLOK']
            . $data['NO_URUT']
            . $data['KD_JNS_OP'];
        return $nop;
    }

    public static function nopString($data)
    {

        $nop = substr($data, 0, 2)
            . '.' . substr($data, 2, 2)
            . '.' . substr($data, 4, 3)
            . '.' . substr($data, 7, 3)
            . '.' . substr($data, 10, 3)
            . '.' . substr($data, 13, 4)
            . '.' . substr($data, 17, 1);
        return $nop;
    }

    public static function hitungDenda($tgltempo, $jmlhpajak)
    {
        $tgljatuhtempo = date('Y-m-d', strtotime($tgltempo));
        $tglsekarang = date('Y-m-d');
        $ts1 = strtotime($tgljatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);
        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        } else {
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        }

        return round($jmldenda);
    }
}
