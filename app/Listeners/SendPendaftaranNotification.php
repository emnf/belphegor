<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
// use App\Notifications\PendaftaranNotification;
use App\Models\User;
use Illuminate\Support\Facades\Notification;

class SendPendaftaranNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // $id_user = $event->user;
        $admins = User::whereHas('roles', function ($query) {
            $query->where('id', 1);
        })->get();
        // dd(new PendaftaranNotification($event->user));
        // Notification::send($admins, new PendaftaranNotification($event->user));
    }
}
