<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingText extends Model
{
    use HasFactory;
    protected $table = 's_text';
    protected $fillable = [
        's_text',
        's_aktif',
        'created_at',
        'updated_at',
    ];
}
