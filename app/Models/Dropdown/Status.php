<?php

namespace App\Models\Dropdown;

use App\Models\Setting\LoginBackground;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    protected $table = 's_status';

    public function LoginBackgrounds()
    {
        return $this->hasMany(LoginBackground::class, 's_id_status', 's_id_status');
    }
}
