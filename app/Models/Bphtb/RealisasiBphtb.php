<?php

namespace App\Models\Bphtb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RealisasiBphtb extends Model
{
    use HasFactory;
    protected $connection = 'bphtb';
    protected $table = 't_pembayaran_bphtb';

    public function getRealisasiBPHTB($target)
    {
        $data = self::select(
            DB::raw('11 as id'),
            DB::raw("'Bea Perolehan Hak Atas Tanah dan Bangunan (BPHTB)' as s_nama"),
            DB::raw('0 as target'),
            DB::raw('COALESCE(SUM(t_pembayaran_bphtb.t_jmlhbayar_total), 0) as realisasi')
        )->whereYear('t_pembayaran_bphtb.t_tglpembayaran_pokok', '=', $target->t_tahun)->first();

        return $data;
    }

    public function getRealisasiBPHTBHarian()
    {
        $data = self::select(
            DB::raw('11 as id'),
            DB::raw("'BPHTB' as s_nama_singkat"),
            DB::raw('0 as target'),
            DB::raw('COALESCE(SUM(t_pembayaran_bphtb.t_jmlhbayar_total), 0) as realisasi')
        )->where('t_pembayaran_bphtb.t_tglpembayaran_pokok', '=', date('Y-m-d'))->first();

        return $data;
    }

    public function harianBphtb()
    {
        $data = self::select(
            't_pembayaran_bphtb.t_id_pembayaran AS id',
            't_spt.t_nama_pembeli as t_nama',
            's_jenistransaksi.s_namajenistransaksi as s_nama',
            't_pembayaran_bphtb.t_jmlhbayar_total as realisasi'
        )
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
            ->whereRaw('TO_CHAR(t_pembayaran_bphtb.t_tglpembayaran_pokok,\'YYYY-MM-DD\') = ? ', date('Y-m-d'))
            ->orderBy('t_pembayaran_bphtb.t_id_pembayaran', 'DESC')
            ->limit(7)
            ->get();
        return $data;
    }
}
