<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RealisasiPBB extends Model
{
    use HasFactory;
    protected $connection = 'sqlsrv';
    // protected $connection = 'oracle';
    protected $table = 'IPROTAXPBB.PEMBAYARAN_SPPT';

    public function dataRealPBB($target)
    {
        $data =
            self::select(
                DB::raw('10 as id'),
                DB::raw("'Pajak Bumi Dan Bangunan Perdesaan Dan Perkotaan' as s_nama"),
                DB::raw('0 as target'),
                DB::raw('COALESCE(SUM(PEMBAYARAN_SPPT.JML_PBB_YG_DIBAYAR), 0) as realisasi')
            )
            ->whereYear('PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT', '=', $target->t_tahun)
            ->first();

        return $data;
    }

    public function dataRealPBBHarian()
    {
        $data =
            self::select(
                DB::raw('10 as id'),
                DB::raw("'PBB P-2' as s_nama_singkat"),
                DB::raw('0 as target'),
                DB::raw('COALESCE(SUM(PEMBAYARAN_SPPT.JML_PBB_YG_DIBAYAR), 0) as realisasi')
            )
            ->where('PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT', '=', date('Y-m-d'))
            ->first();

        return $data;
    }

    public function HarianPbb()
    {
        $data
            = self::select(
                DB::raw('TOP 7 SPPT.NM_WP_SPPT'),
                DB::raw('concat(PEMBAYARAN_SPPT.KD_PROPINSI,PEMBAYARAN_SPPT.KD_DATI2,PEMBAYARAN_SPPT.KD_KECAMATAN,PEMBAYARAN_SPPT.KD_KELURAHAN,PEMBAYARAN_SPPT.KD_BLOK,PEMBAYARAN_SPPT.NO_URUT,PEMBAYARAN_SPPT.KD_JNS_OP) AS s_nama'),
                'PEMBAYARAN_SPPT.THN_PAJAK_SPPT as tahunsppt',
                'PEMBAYARAN_SPPT.JML_PBB_YG_DIBAYAR AS realisasi'
            )->leftJoin('SPPT', function ($join) {
                $join->whereRaw('concat(SPPT.KD_PROPINSI,SPPT.KD_DATI2,SPPT.KD_KECAMATAN,SPPT.KD_KELURAHAN,SPPT.KD_BLOK,SPPT.NO_URUT,SPPT.KD_JNS_OP,SPPT.THN_PAJAK_SPPT) = concat(PEMBAYARAN_SPPT.KD_PROPINSI,PEMBAYARAN_SPPT.KD_DATI2,PEMBAYARAN_SPPT.KD_KECAMATAN,PEMBAYARAN_SPPT.KD_KELURAHAN,PEMBAYARAN_SPPT.KD_BLOK,PEMBAYARAN_SPPT.NO_URUT,PEMBAYARAN_SPPT.KD_JNS_OP,PEMBAYARAN_SPPT.THN_PAJAK_SPPT)');
            })->where('PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT', '=', date('Y-m-d'))->orderBy('PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT', 'DESC')->get();
        return $data;
    }
}
