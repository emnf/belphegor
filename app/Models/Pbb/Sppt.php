<?php

namespace App\Models\Pbb;

use App\Models\Himbauan\Himbauan;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Sppt extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv';
    // protected $connection = 'oracle';
    protected $table = 'IPROTAXPBB.SPPT';
    protected $fillable = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'NO_URUT',
        'KD_JNS_OP',
        'THN_PAJAK_SPPT',
        'SIKLUS_SPPT',
        'JLN_OP_SPPT',
        'BLOK_KAV_NO_OP_SPPT',
        'RW_OP_SPPT',
        'RT_OP_SPPT',
        'NM_WP_SPPT',
        'JLN_WP_SPPT',
        'BLOK_KAV_NO_WP_SPPT',
        'RW_WP_SPPT',
        'RT_WP_SPPT',
        'KELURAHAN_WP_SPPT',
        'KECAMATAN_WP_SPPT',
        'KOTA_WP_SPPT',
        'KD_POS_WP_SPPT',
        'STATUS_WP_SPPT',
        'NPWP_SPPT',
        'NO_PERSIL_SPPT',
        'LUAS_BUMI_SPPT',
        'LUAS_BNG_SPPT',
        'KD_KLS_TANAH',
        'THN_AWAL_KLS_TANAH',
        'KD_KLS_BNG',
        'THN_AWAL_KLS_BNG',
        'NJOP_BUMI_SPPT',
        'NJOP_BNG_SPPT',
        'NJOP_SPPT',
        'NJOPTKP_SPPT',
        'NJKP_SPPT',
        'TARIF_SPPT',
        'PBB_TERHUTANG_SPPT',
        'FAKTOR_PENGURANG_SPPT',
        'PBB_YG_HRS_DIBAYAR_SPPT',
        'TGL_JATUH_TEMPO_SPPT',
        'KD_BANK_TUNGGAL',
        'KD_BANK_PERSEPSI',
        'KD_TP',
        'STATUS_PEMBAYARAN_SPPT',
        'STATUS_TAGIHAN_SPPT',
        'STATUS_CETAK_SPPT',
        'TGL_TERBIT_SPPT',
        'TGL_CETAK_SPPT',
        'NIP_PENCETAK_SPPT',
        'STATUS_BILLING_KOLEKTIF',

    ];

    public $timestamps = false;

    public function datagrid($request)
    {
        $sql = self::select(
            'SPPT.KD_PROPINSI',
            'SPPT.KD_DATI2',
            'SPPT.KD_KECAMATAN',
            'SPPT.KD_KELURAHAN',
            'SPPT.KD_BLOK',
            'SPPT.NO_URUT',
            'SPPT.KD_JNS_OP',
            'SPPT.THN_PAJAK_SPPT',
            'SPPT.NM_WP_SPPT',
            'SPPT.JLN_WP_SPPT',
            'SPPT.PBB_YG_HRS_DIBAYAR_SPPT',
            'SPPT.TGL_JATUH_TEMPO_SPPT',
            'SPPT.STATUS_PEMBAYARAN_SPPT'
        )
            ->selectRaw("IPROTAXPBB.HITUNG_DENDA(SPPT.TGL_JATUH_TEMPO_SPPT, SPPT.PBB_YG_HRS_DIBAYAR_SPPT) AS DENDA")
            ->where([
                ['KD_KECAMATAN', (Auth::user()->s_id_hakakses == 4) ? Auth::user()->kecamatan : $request['filter']['kecamatan']],
                ['KD_KELURAHAN', (Auth::user()->s_id_hakakses == 4) ? Auth::user()->kelurahan : $request['filter']['kelurahan']],
                ['THN_PAJAK_SPPT', $request['filter']['tahun']]
            ])
            ->whereIn('STATUS_PEMBAYARAN_SPPT', [0, 1]);
        return $sql;
    }

    public function datagridInput($request)
    {
        $sql = self::select(
            'SPPT.KD_PROPINSI',
            'SPPT.KD_DATI2',
            'SPPT.KD_KECAMATAN',
            'SPPT.KD_KELURAHAN',
            'SPPT.KD_BLOK',
            'SPPT.NO_URUT',
            'SPPT.KD_JNS_OP',
            'SPPT.THN_PAJAK_SPPT',
            'SPPT.NM_WP_SPPT',
            'SPPT.JLN_WP_SPPT',
            'SPPT.PBB_YG_HRS_DIBAYAR_SPPT',
            'SPPT.TGL_JATUH_TEMPO_SPPT',
            'TTR_SPPT.TGL_TERIMA_SPPT',
            // 'TTR_SPPT.KD_DUKUH',
            'TTR_SPPT.NM_YG_MENERIMA_SPPT',
            'TTR_SPPT_KETERANGAN.CREATED_AT'
        )
            ->selectRaw("IPROTAXPBB.HITUNG_DENDA(SPPT.TGL_JATUH_TEMPO_SPPT, SPPT.PBB_YG_HRS_DIBAYAR_SPPT) AS DENDA")
            ->leftJoin('TTR_SPPT', [
                ['TTR_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['TTR_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['TTR_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['TTR_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['TTR_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['TTR_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['TTR_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['TTR_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT']
            ])
            ->leftJoin('IPROTAXPBB.TTR_SPPT_KETERANGAN', [
                ['TTR_SPPT_KETERANGAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['TTR_SPPT_KETERANGAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['TTR_SPPT_KETERANGAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['TTR_SPPT_KETERANGAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['TTR_SPPT_KETERANGAN.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['TTR_SPPT_KETERANGAN.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['TTR_SPPT_KETERANGAN.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['TTR_SPPT_KETERANGAN.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT']
            ])
            ->where('SPPT.KD_KECAMATAN', (in_array(Auth::user()->s_id_hakakses, [4, 5])) ? Auth::user()->kecamatan : $request['filter']['kecamatan'])
            ->where('SPPT.KD_KELURAHAN', (in_array(Auth::user()->s_id_hakakses, [4, 5])) ? Auth::user()->kelurahan : $request['filter']['kelurahan'])
            ->where('SPPT.THN_PAJAK_SPPT', $request['filter']['tahun'])
            ->whereIn('SPPT.STATUS_PEMBAYARAN_SPPT', [0, 1]);

        return $sql;
    }

    public function getDataArray($request)
    {
        $response = self::select(
            'SPPT.KD_PROPINSI',
            'SPPT.KD_DATI2',
            'SPPT.KD_KECAMATAN',
            'SPPT.KD_KELURAHAN',
            'SPPT.KD_BLOK',
            'SPPT.NO_URUT',
            'SPPT.KD_JNS_OP',
            'SPPT.THN_PAJAK_SPPT',
            'SPPT.NM_WP_SPPT',
            'SPPT.JLN_WP_SPPT',
            'SPPT.STATUS_PEMBAYARAN_SPPT',
            'SPPT.PBB_YG_HRS_DIBAYAR_SPPT',
            'REF_KECAMATAN.NM_KECAMATAN',
            'REF_KELURAHAN.NM_KELURAHAN'
        )
            ->selectRaw("IPROTAXPBB.HITUNG_DENDA(SPPT.TGL_JATUH_TEMPO_SPPT, SPPT.PBB_YG_HRS_DIBAYAR_SPPT) AS DENDA")
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->whereIn('SPPT.STATUS_PEMBAYARAN_SPPT', [0, 1]);

        if ($request->kec != null) {
            $response = ($request->kec != null) ? $response->where('SPPT.KD_KECAMATAN', $request->kec) : null;
        }
        if ($request->kel != null) {
            $response = $response->where('SPPT.KD_KELURAHAN', $request->kel);
        }
        if ($request->tahun != null) {
            $response = $response->where('SPPT.THN_PAJAK_SPPT', $request->tahun);
        }

        if ($request->blok != null) {
            $response = $response->where('SPPT.KD_BLOK', $request->blok);
        }

        if ($request->nop != null) {
            $response = $response->whereRaw("KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP like '" . str_ireplace('.', '', $request->nop) . "%' ");
        }
        if ($request->nama != null) {
            $response = $response->where('NM_WP_SPPT', 'like', "%" . strtoupper($request->nama) . "%");
        }
        if ($request->alamat != null) {
            $response = $response->where('JLN_WP_SPPT', 'like', "%" . strtoupper($request->alamat) . "%");
        }

        if ($request->status != null) {
            if ($request->status == 1) {
                $response = $response->where('STATUS_PEMBAYARAN_SPPT', '=', 0);
            } else {
                $response = $response->where('STATUS_PEMBAYARAN_SPPT', '=', 1);
            }
        }
        $response = $response->orderBy('KD_BLOK');
        $response = $response->orderBy('NO_URUT');
        return $response;
    }

    public function getDataSpptTersampaikan($request)
    {
        $response = self::select(
            'SPPT.KD_PROPINSI',
            'SPPT.KD_DATI2',
            'SPPT.KD_KECAMATAN',
            'SPPT.KD_KELURAHAN',
            'SPPT.KD_BLOK',
            'SPPT.NO_URUT',
            'SPPT.KD_JNS_OP',
            'SPPT.THN_PAJAK_SPPT',
            'SPPT.NM_WP_SPPT',
            'SPPT.JLN_WP_SPPT',
            'SPPT.PBB_YG_HRS_DIBAYAR_SPPT',
            'SPPT.STATUS_PEMBAYARAN_SPPT',
            'REF_KECAMATAN.NM_KECAMATAN',
            'REF_KELURAHAN.NM_KELURAHAN',
            'TTR_SPPT_KETERANGAN.CREATED_AT'
        )
            ->leftJoin('TTR_SPPT', [
                ['TTR_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['TTR_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['TTR_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['TTR_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['TTR_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['TTR_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['TTR_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['TTR_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT']
            ])
            ->leftJoin('IPROTAXPBB.TTR_SPPT_KETERANGAN', [
                ['TTR_SPPT_KETERANGAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['TTR_SPPT_KETERANGAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['TTR_SPPT_KETERANGAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['TTR_SPPT_KETERANGAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['TTR_SPPT_KETERANGAN.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['TTR_SPPT_KETERANGAN.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['TTR_SPPT_KETERANGAN.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['TTR_SPPT_KETERANGAN.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT']
            ])
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->whereIn('SPPT.STATUS_PEMBAYARAN_SPPT', [0, 1]);

        if ($request->kec != null) {
            $response = ($request->kec != null) ? $response->where('SPPT.KD_KECAMATAN', $request->kec) : null;
        }
        if ($request->kel != null) {
            $response = $response->where('SPPT.KD_KELURAHAN', $request->kel);
        }

        if ($request->tahun != null) {
            $response = $response->where('SPPT.THN_PAJAK_SPPT', $request->tahun);
        }

        if ($request->nop != null) {
            $response = $response->whereRaw("KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP like '" . str_ireplace('.', '', $request->nop) . "%' ");
        }
        if ($request->nama != null) {
            $response = $response->where('NM_WP_SPPT', 'like', "%" . strtoupper($request->nama) . "%");
        }
        if ($request->alamat != null) {
            $response = $response->where('JLN_WP_SPPT', 'like', "%" . strtoupper($request->alamat) . "%");
        }
        if ($request->status != null) {
            if ($request->status == 1) {
                $response = $response->whereNotNull('TTR_SPPT_KETERANGAN.CREATED_AT');
            } else {
                $response = $response->whereNull('TTR_SPPT_KETERANGAN.CREATED_AT');
            }
        }

        if ($request->blok != null) {
            $response = $response->where('SPPT.KD_BLOK', $request->blok);
        }
        $response->orderby('KD_BLOK', 'ASC');
        $response->orderby('NO_URUT', 'ASC');


        return $response;
    }

    public function tagihan($request)
    {
        if (Auth::user()->s_id_hakakses == 5) {

            $sql = self::select('SPPT.*')
                ->selectRaw("IPROTAXPBB.HITUNG_DENDA(SPPT.TGL_JATUH_TEMPO_SPPT, SPPT.PBB_YG_HRS_DIBAYAR_SPPT) AS DENDA")
                ->leftJoin('Z_INPUT_SPPT_TERSAMPAIKAN', [
                    ['Z_INPUT_SPPT_TERSAMPAIKAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                    ['Z_INPUT_SPPT_TERSAMPAIKAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                    ['Z_INPUT_SPPT_TERSAMPAIKAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                    ['Z_INPUT_SPPT_TERSAMPAIKAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                    ['Z_INPUT_SPPT_TERSAMPAIKAN.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                    ['Z_INPUT_SPPT_TERSAMPAIKAN.NO_URUT', '=', 'SPPT.NO_URUT'],
                    ['Z_INPUT_SPPT_TERSAMPAIKAN.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                    ['Z_INPUT_SPPT_TERSAMPAIKAN.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT']
                ])
                ->leftJoin('SPPT_BILLING_KOLEKTIF', [
                    ['SPPT_BILLING_KOLEKTIF.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                    ['SPPT_BILLING_KOLEKTIF.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                    ['SPPT_BILLING_KOLEKTIF.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                    ['SPPT_BILLING_KOLEKTIF.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                    ['SPPT_BILLING_KOLEKTIF.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                    ['SPPT_BILLING_KOLEKTIF.NO_URUT', '=', 'SPPT.NO_URUT'],
                    ['SPPT_BILLING_KOLEKTIF.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                    ['SPPT_BILLING_KOLEKTIF.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT']
                ])
                ->where([
                    ['SPPT.KD_KECAMATAN', Auth::user()->kecamatan],
                    ['SPPT.KD_KELURAHAN', Auth::user()->kelurahan],
                    ['SPPT.THN_PAJAK_SPPT', $request->tahun],
                    ['SPPT.STATUS_PEMBAYARAN_SPPT', 0],

                    ['SPPT_BILLING_KOLEKTIF.STATUS_BILLING_KOLEKTIF', NULL]
                ]);
            // ->whereNull('SPPT_BILLING_KOLEKTIF.STATUS_BILLING_KOLEKTIF')
            // ->where('SPPT_BILLING_KOLEKTIF.STATUS_BILLING_KOLEKTIF', null);
        } else {

            $sql = self::select('SPPT.*')
                ->selectRaw("IPROTAXPBB.HITUNG_DENDA(SPPT.TGL_JATUH_TEMPO_SPPT, SPPT.PBB_YG_HRS_DIBAYAR_SPPT) AS DENDA")
                ->leftJoin('IPROTAXPBB.SPPT_BILLING_KOLEKTIF', [
                    ['SPPT_BILLING_KOLEKTIF.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                    ['SPPT_BILLING_KOLEKTIF.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                    ['SPPT_BILLING_KOLEKTIF.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                    ['SPPT_BILLING_KOLEKTIF.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                    ['SPPT_BILLING_KOLEKTIF.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                    ['SPPT_BILLING_KOLEKTIF.NO_URUT', '=', 'SPPT.NO_URUT'],
                    ['SPPT_BILLING_KOLEKTIF.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                    ['SPPT_BILLING_KOLEKTIF.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT']
                ])
                ->where([
                    ['SPPT.KD_KECAMATAN', (Auth::user()->s_id_hakakses == 4) ? Auth::user()->kecamatan : $request->kecamatan],
                    ['SPPT.KD_KELURAHAN', (Auth::user()->s_id_hakakses == 4) ? Auth::user()->kelurahan : $request->kelurahan],
                    ['SPPT.THN_PAJAK_SPPT', $request->tahun],
                    ['SPPT.STATUS_PEMBAYARAN_SPPT', 0],

                    ['SPPT_BILLING_KOLEKTIF.STATUS_BILLING_KOLEKTIF', NULL]
                ]);
            // ->whereNull('SPPT_BILLING_KOLEKTIF.STATUS_BILLING_KOLEKTIF')
            // ->where('SPPT_BILLING_KOLEKTIF.STATUS_BILLING_KOLEKTIF', null);
        }
        if ($request->blok) {
            $sql->where('SPPT.KD_BLOK', $request->blok);
        }
        $sql->orderBy('SPPT.KD_KECAMATAN');
        $sql->orderBy('SPPT.KD_KELURAHAN');
        $sql->orderBy('SPPT.KD_BLOK');
        return $sql;
    }

    public function getByNop($nop)
    {
        $KD_PROV = substr($nop, 0, 2);
        $KD_DATI2 = substr($nop, 2, 2);
        $KD_KEC = substr($nop, 4, 3);
        $KD_KEL = substr($nop, 7, 3);
        $KD_BLOK = substr($nop, 10, 3);
        $NO_URUT = substr($nop, 13, 4);
        $KD_JNS_OP = substr($nop, 17, 1);
        $THN_PAJAK_SPPT = substr($nop, 18, 4);

        return self::select('SPPT.*')
            ->selectRaw("IPROTAXPBB.HITUNG_DENDA(SPPT.TGL_JATUH_TEMPO_SPPT, SPPT.PBB_YG_HRS_DIBAYAR_SPPT) AS DENDA")
            ->where([
                ['SPPT.KD_PROPINSI', '=', $KD_PROV],
                ['SPPT.KD_DATI2', '=', $KD_DATI2],
                ['SPPT.KD_KECAMATAN', '=', $KD_KEC],
                ['SPPT.KD_KELURAHAN', '=', $KD_KEL],
                ['SPPT.KD_BLOK', '=', $KD_BLOK],
                ['SPPT.NO_URUT', '=', $NO_URUT],
                ['SPPT.KD_JNS_OP', '=', $KD_JNS_OP],
                ['SPPT.THN_PAJAK_SPPT', '=', $THN_PAJAK_SPPT]
            ])->first();
    }

    public function getNop($nop)
    {
        $nop = explode('.', $nop);
        $KD_PROV = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KEC = $nop[2];
        $KD_KEL = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS = $nop[6];

        return self::where([
            ['SPPT.KD_PROPINSI', '=', $KD_PROV],
            ['SPPT.KD_DATI2', '=', $KD_DATI2],
            ['SPPT.KD_KECAMATAN', '=', $KD_KEC],
            ['SPPT.KD_KELURAHAN', '=', $KD_KEL],
            ['SPPT.KD_BLOK', '=', $KD_BLOK],
            ['SPPT.NO_URUT', '=', $NO_URUT],
            ['SPPT.KD_JNS_OP', '=', $KD_JNS]
        ])
            ->whereIn('SPPT.STATUS_PEMBAYARAN_SPPT', ['0', '1'])->get();
    }

    public function getCodingsppt($nop, $tahun, $sppt)
    {
        $KD_PROV = substr($nop, 0, 2);
        $KD_DATI2 = substr($nop, 2, 2);
        $KD_KEC = substr($nop, 4, 3);
        $KD_KEL = substr($nop, 7, 3);
        $KD_BLOK = substr($nop, 10, 3);
        $NO_URUT = substr($nop, 13, 4);
        $KD_JNS_OP = substr($nop, 17, 1);

        $KD_ZNT = DatOpBumi::select('KD_ZNT')
            ->where([
                ['KD_PROPINSI', '=', $KD_PROV],
                ['KD_DATI2', '=', $KD_DATI2],
                ['KD_KECAMATAN', '=', $KD_KEC],
                ['KD_KELURAHAN', '=', $KD_KEL],
                ['KD_BLOK', '=', $KD_BLOK],
                ['NO_URUT', '=', $NO_URUT],
                ['KD_JNS_OP', '=', $KD_JNS_OP]
            ])->first();
        $NIR = NIR::select('NIR')->where([
            ['KD_PROPINSI', '=', $KD_PROV],
            ['KD_DATI2', '=', $KD_DATI2],
            ['KD_KECAMATAN', '=', $KD_KEC],
            ['KD_KELURAHAN', '=', $KD_KEL],
            ['KD_ZNT', '=', $KD_ZNT->KD_ZNT],
            ['THN_NIR_ZNT', '=', $tahun]
        ])->first();


        $KD_JPB = DatOpBng::select('KD_JPB')->where([
            ['KD_PROPINSI', '=', $KD_PROV],
            ['KD_DATI2', '=', $KD_DATI2],
            ['KD_KECAMATAN', '=', $KD_KEC],
            ['KD_KELURAHAN', '=', $KD_KEL],
            ['KD_BLOK', '=', $KD_BLOK],
            ['NO_URUT', '=', $NO_URUT],
            ['KD_JNS_OP', '=', $KD_JNS_OP],
            ['NO_BNG', '=', 1]
        ])->first();
        if (!$KD_JPB) {
            $KD_JPB =  DatOpBumi::select('JNS_BUMI')
                ->where([
                    ['KD_PROPINSI', '=', $KD_PROV],
                    ['KD_DATI2', '=', $KD_DATI2],
                    ['KD_KECAMATAN', '=', $KD_KEC],
                    ['KD_KELURAHAN', '=', $KD_KEL],
                    ['KD_BLOK', '=', $KD_BLOK],
                    ['NO_URUT', '=', $NO_URUT],
                    ['KD_JNS_OP', '=', $KD_JNS_OP],
                    ['NO_BUMI', '=', 1]
                ])->first();

            $KD_JPB->KD_JPB = "0" . $KD_JPB->JNS_BUMI;
        }


        $v_pbb_hrsbyr = $sppt->PBB_YG_HRS_DIBAYAR_SPPT;
        $v_c1 = substr($NIR->NIR, 0, 1);
        $v_c2 = date('dmyhi');
        $v_c3 = substr($v_pbb_hrsbyr, 0, 1);
        $v_c4 = substr($sppt->NM_WP_SPPT, 0, 1);
        $v_c5 = substr($KD_ZNT->KD_ZNT, 0, 1);
        $v_c6 = substr($sppt->NM_WP_SPPT, strlen($sppt->NM_WP_SPPT), 1);
        $v_c7 = abs(strlen(abs($v_pbb_hrsbyr)) - 3);
        $v_c8 = substr($KD_ZNT->KD_ZNT, 1, 1);
        $v_c9 = strlen($NIR->NIR);
        $v_c10 = strlen($v_pbb_hrsbyr);
        $v_c11 = $KD_JPB->KD_JPB;
        $v_c12 = "OL";

        $res = $v_c1 . $v_c2 . $v_c3 . $v_c4 . $v_c5 . $v_c6 . $v_c7 . $v_c8 . $v_c9 . $v_c10 . $v_c11 . $v_c12;
        $data['coding_sppt'] = $res;

        return $data;
    }

    public function getAngkaKontrol($nop, $siklus, $tahun)
    {
        $vgi_digit = 3 * substr($nop, 0, 1);
        $vgi_digit = $vgi_digit + (5 * number_format((int)substr($nop, 2, 1)));
        $vgi_digit = $vgi_digit + (7 * number_format((int)substr($nop, 3, 1)));
        $vgi_digit = $vgi_digit + (13 * number_format((int)substr($nop, 4, 1)));
        $vgi_digit = $vgi_digit + (17 * number_format((int)substr($nop, 5, 1)));
        $vgi_digit = $vgi_digit + (19 * number_format((int)substr($nop, 6, 1)));
        $vgi_digit = $vgi_digit + (23 * number_format((int)substr($nop, 7, 1)));
        $vgi_digit = $vgi_digit + (29 * number_format((int)substr($nop, 8, 1)));
        $vgi_digit = $vgi_digit + (31 * number_format((int)substr($nop, 9, 1)));
        $vgi_digit = $vgi_digit + (37 * number_format((int)substr($nop, 10, 1)));
        $vgi_digit = $vgi_digit + (41 * number_format((int)substr($nop, 11, 1)));
        $vgi_digit = $vgi_digit + (43 * number_format((int)substr($nop, 12, 1)));
        $vgi_digit = $vgi_digit + (47 * number_format((int)substr($nop, 13, 1)));
        $vgi_digit = $vgi_digit + (53 * number_format((int)substr($nop, 14, 1)));
        $vgi_digit = $vgi_digit + (59 * number_format((int)substr($nop, 15, 1)));
        $vgi_digit = $vgi_digit + (61 * number_format((int)substr($nop, 16, 1)));
        $vgi_digit = $vgi_digit + (67 * number_format((int)substr($nop, 17, 1)));
        $vgi_digit = $vgi_digit + (71 * number_format((int)substr($tahun, 4, 1)));

        $vgs_siklus = $siklus;

        if ($vgs_siklus == 1) {
            $vgs_siklus = "0" . $vgs_siklus;
        } elseif ($vgs_siklus > 1) {
            $vgs_siklus = substr($vgs_siklus, strlen($vgs_siklus) - 1, 2);
        }

        $vgi_digit = $vgi_digit + (61 * (int)(substr($vgs_siklus, 1, 1)));
        $vgi_digit = $vgi_digit + (67 * (int)(substr($vgs_siklus, 2, 1)));

        $vgi_digit = fmod($vgi_digit, 11);

        $vgs_digit = strval($vgi_digit);
        // dd($vgs_digit);


        if (strlen($vgs_digit) > 1) {
            $vgs_digit = substr($vgs_digit, strlen($vgs_digit), 1);
        }

        $vgs_digit = $vgs_siklus . $vgs_digit;

        $data['angka_kontrol'] = $vgs_digit;
        return $data;
        // return DB::connection('oracle')->select("SELECT ANGKA_KONTROL(" . $nop . ", " . $siklus . ", " . $tahun . ") AS angka_kontrol FROM DUAL");
    }

    public function getAll($request, $status)
    {
        $response = self::selectRaw("concat(SPPT.KD_PROPINSI,'.',SPPT.KD_DATI2,'.',SPPT.KD_KECAMATAN,'.',SPPT.KD_KELURAHAN,'.',SPPT.KD_BLOK,'-',SPPT.NO_URUT,'.',SPPT.KD_JNS_OP) as nop,
            SPPT.KD_PROPINSI,
            SPPT.KD_DATI2,
            SPPT.KD_KECAMATAN,
            SPPT.KD_KELURAHAN,
            SPPT.KD_BLOK,
            SPPT.NO_URUT,
            SPPT.KD_JNS_OP,
            SPPT.THN_PAJAK_SPPT,
            SPPT.SIKLUS_SPPT,
            SPPT.KD_BANK_TUNGGAL,
            SPPT.KD_BANK_PERSEPSI,
            SPPT.KD_TP,
            SPPT.NM_WP_SPPT,
            SPPT.JLN_WP_SPPT,
            SPPT.BLOK_KAV_NO_WP_SPPT,
            SPPT.RW_WP_SPPT,
            SPPT.RT_WP_SPPT,
            SPPT.KELURAHAN_WP_SPPT,
            SPPT.KOTA_WP_SPPT,
            SPPT.KD_POS_WP_SPPT,
            SPPT.NPWP_SPPT,
            SPPT.NO_PERSIL_SPPT,
            SPPT.KD_KLS_TANAH,
            SPPT.THN_AWAL_KLS_TANAH,
            SPPT.KD_KLS_BNG,
            SPPT.THN_AWAL_KLS_BNG,
            SPPT.TGL_JATUH_TEMPO_SPPT,
            SPPT.LUAS_BUMI_SPPT,
            SPPT.LUAS_BNG_SPPT,
            SPPT.NJOP_BUMI_SPPT,
            SPPT.NJOP_BNG_SPPT,
            SPPT.NJOP_SPPT,
            SPPT.NJOPTKP_SPPT,
            SPPT.NJKP_SPPT,
            SPPT.PBB_TERHUTANG_SPPT,
            SPPT.FAKTOR_PENGURANG_SPPT,
            SPPT.PBB_YG_HRS_DIBAYAR_SPPT,
            SPPT.STATUS_PEMBAYARAN_SPPT,
            SPPT.STATUS_TAGIHAN_SPPT,
            SPPT.STATUS_CETAK_SPPT,
            SPPT.TGL_TERBIT_SPPT,
            SPPT.TGL_CETAK_SPPT,
            SPPT.NIP_PENCETAK_SPPT,
            SPPT_BILLING_KOLEKTIF.STATUS_BILLING_KOLEKTIF,

            PEMBAYARAN_SPPT.JML_PBB_YG_DIBAYAR,
            PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT,
            PEMBAYARAN_SPPT.KODE_BAYAR")
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['SPPT.KD_PROPINSI', '=', 'PEMBAYARAN_SPPT.KD_PROPINSI'],
                ['SPPT.KD_DATI2', '=', 'PEMBAYARAN_SPPT.KD_DATI2'],
                ['SPPT.KD_KECAMATAN', '=', 'PEMBAYARAN_SPPT.KD_KECAMATAN'],
                ['SPPT.KD_KELURAHAN', '=', 'PEMBAYARAN_SPPT.KD_KELURAHAN'],
                ['SPPT.KD_BLOK', '=', 'PEMBAYARAN_SPPT.KD_BLOK'],
                ['SPPT.NO_URUT', '=', 'PEMBAYARAN_SPPT.NO_URUT'],
                ['SPPT.KD_JNS_OP', '=', 'PEMBAYARAN_SPPT.KD_JNS_OP'],
                ['SPPT.THN_PAJAK_SPPT', '=', 'PEMBAYARAN_SPPT.THN_PAJAK_SPPT'],
            ])->leftJoin('IPROTAXPBB.SPPT_BILLING_KOLEKTIF', [
                ['SPPT.KD_PROPINSI', '=', 'SPPT_BILLING_KOLEKTIF.KD_PROPINSI'],
                ['SPPT.KD_DATI2', '=', 'SPPT_BILLING_KOLEKTIF.KD_DATI2'],
                ['SPPT.KD_KECAMATAN', '=', 'SPPT_BILLING_KOLEKTIF.KD_KECAMATAN'],
                ['SPPT.KD_KELURAHAN', '=', 'SPPT_BILLING_KOLEKTIF.KD_KELURAHAN'],
                ['SPPT.KD_BLOK', '=', 'SPPT_BILLING_KOLEKTIF.KD_BLOK'],
                ['SPPT.NO_URUT', '=', 'SPPT_BILLING_KOLEKTIF.NO_URUT'],
                ['SPPT.KD_JNS_OP', '=', 'SPPT_BILLING_KOLEKTIF.KD_JNS_OP'],
                ['SPPT.THN_PAJAK_SPPT', '=', 'SPPT_BILLING_KOLEKTIF.THN_PAJAK_SPPT'],
            ]);
        if ($status == '0') {
            $response = $response->whereRaw("SPPT.STATUS_PEMBAYARAN_SPPT='0'");
        }

        if ($status == '1') {
            $response = $response->whereRaw("SPPT.STATUS_PEMBAYARAN_SPPT='1'");
            if (!empty($request['filter']['tglbayar']) || $request['filter']['tglbayar'] != '') {
                // $response = $response->whereRaw("TO_CHAR(PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT,'YYYYMMDD')='" . Carbon::parse($request['filter']['tglbayar'])->format('Ymd') . "'");
                $response = $response->whereRaw("CONVERT(varchar, PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT, 112)='" . Carbon::parse($request['filter']['tglbayar'])->format('Ymd') . "'");
            }
        }

        if ($request['filter']['kecamatan'] != null) {
            $response = $response->whereRaw("SPPT.KD_KECAMATAN='" . $request['filter']['kecamatan'] . "'");
        }
        if ($request['filter']['kelurahan'] != null) {
            $response = $response->whereRaw("SPPT.KD_KELURAHAN='" . $request['filter']['kelurahan'] . "'");
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->whereRaw("SPPT.THN_PAJAK_SPPT='" . $request['filter']['tahun'] . "'");
        }

        // dd($response->toSql());
        return $response;
    }
}
