<?php

namespace App\Models\Simpatda;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Realisasi extends Model
{
    use HasFactory;
    protected $connection = 'simpatda';
    protected $table = 's_jenis_objek';

    public function getDataRealisasiJenis($target)
    {
        $data = self::select(
            's_jenis_objek.id',
            's_jenis_objek.s_nama',
            DB::raw('( SELECT COALESCE ( SUM ( t_jumlah ), 0 ) FROM t_target_detail
            LEFT JOIN rekening_view A ON A.id = t_target_detail.t_id_rekening
            LEFT JOIN t_target_header B on B.id = t_target_detail.t_id_target_header
            WHERE A.s_id_jenis_objek = s_jenis_objek.id
            and B.id = ' . $target->id . '
            ) AS target'),
            DB::raw('( SELECT COALESCE( sum(t_jumlah_pembayaran),0) FROM t_pembayaran
            left join rekening_view a on a.id = t_pembayaran.t_id_rekening_pembayaran
            where a.s_id_jenis_objek = s_jenis_objek.id
            and EXTRACT(YEAR from t_pembayaran.t_tgl_pembayaran) = ' . $target->t_tahun . '
            ) as realisasi')
        )->orderBy('s_jenis_objek.id', 'ASC')->get();
        return $data;
    }

    public function getDataRealisasiJenisHarian()
    {
        $data = self::select(
            's_jenis_objek.id',
            's_jenis_objek.s_nama_singkat',
            DB::raw('( SELECT COALESCE ( SUM ( t_jumlah ), 0 ) FROM t_target_detail LEFT JOIN rekening_view A ON A.id = t_target_detail.t_id_rekening LEFT JOIN t_target_header B on B.id = t_target_detail.t_id_target_header WHERE A.s_id_jenis_objek = s_jenis_objek.id and B.id = 1 ) AS target'),
            DB::raw('( SELECT COALESCE( sum(t_jumlah_pembayaran),0) FROM t_pembayaran left join rekening_view a on a.id = t_pembayaran.t_id_rekening_pembayaran where a.s_id_jenis_objek = s_jenis_objek.id and t_pembayaran.t_tgl_pembayaran = ? ) as realisasi'), // Use "?" as a placeholder for the date parameter
        )
            ->orderBy('s_jenis_objek.id', 'ASC')
            ->setBindings([date('Y-m-d')]) // Bind the date parameter to the query
            ->get();
        return $data;
    }

    public function targetPbb($id, $target)
    {
        $data = self::select(
            DB::raw('( SELECT COALESCE ( SUM ( t_jumlah ), 0 ) FROM t_target_detail
            LEFT JOIN rekening_view A ON A.id = t_target_detail.t_id_rekening
            LEFT JOIN t_target_header B on B.id = t_target_detail.t_id_target_header
            WHERE A.s_id_jenis_objek = ' . $id . ' and B.id = ' . $target->id . ' ) AS target'),
        )->orderBy('s_jenis_objek.id', 'ASC')->first()->toArray();
        return $data;
    }
}
