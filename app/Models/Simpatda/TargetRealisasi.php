<?php

namespace App\Models\Simpatda;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetRealisasi extends Model
{
    use HasFactory;
    protected $connection = 'simpatda';
    protected $table = 't_target_header';

    public function target()
    {
        $data = self::select('t_target_header.*', 's_jenis_target.s_nama')->leftJoin('s_jenis_target', 's_jenis_target.id', '=', 't_target_header.t_id_jenis_target')->orderBy('t_target_header.id', 'ASC')->orderBy('t_target_header.t_tahun', 'DESC')->get();
        return $data;
    }
}
