<?php

namespace App\Models\Simpatda;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transaksi extends Model
{
    use HasFactory;
    protected $connection = 'simpatda';
    protected $table = 't_transaksi';

    public function harianSimpatda()
    {
        $data = self::distinct()->select(
            'wp_view.t_nama',
            's_jenis_objek.s_nama',
            't_pembayaran.t_no_pembayaran',
            DB::raw('( SELECT COALESCE(sum(a.t_jumlah_pembayaran),0) from t_pembayaran a where a.t_tgl_pembayaran = t_pembayaran.t_tgl_pembayaran and a.t_id_transaksi = t_transaksi.id ) as realisasi')
        )
            ->leftJoin('s_jenis_objek', 's_jenis_objek.id', '=', 't_transaksi.t_id_jenis_objek')
            ->leftJoin('wp_objek_view', 'wp_objek_view.id', '=', 't_transaksi.t_id_objek')
            ->leftJoin('wp_view', 'wp_view.id', '=', 'wp_objek_view.t_id_wp')
            ->leftJoin('t_pembayaran', 't_pembayaran.t_id_transaksi', '=', 't_transaksi.id')->where('t_pembayaran.t_tgl_pembayaran', '=', date('Y-m-d'))->orderBy('t_pembayaran.t_no_pembayaran', 'desc')->limit(7)->get();

        return $data;
    }
}
