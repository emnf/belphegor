@extends('layouts.master')

@section('judulkiri')
    Menu Dashboard
@endsection

@section('judulkanan')
    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-6">
            <div class="card card-outline card-success">
                <div class="card-header">
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <dl class="row">
                                {{-- <dd class="col-sm-3">Periode Pajak </dd>
                                <dt class="col-sm-9">
                                    <input type="text" id="periode_pajak" class="form-control form-control-sm" value="{{date('Y')}}">
                                </dt> --}}
                                <dd class="col-sm-3">Target Realisasi </dd>
                                <dt class="col-sm-9 mb-2">
                                    <select id="target_realisasi" class="form-control form-control-sm">
                                        @foreach ($data_target as $val)
                                            <option value="{{$val->id}}">{{$val->t_tahun}} || {{$val->s_nama}}</option>
                                        @endforeach
                                    </select>
                                </dt>
                                <dt class="col-sm-12 text-right">
                                    <button type="button" class="btn btn-primary btn-sm" onclick="tampilDashboard()"><i class="fas fa-tachometer-alt"></i> Tamplikan Dashboard</button>
                                </dt>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        function tampilDashboard() {
            var target = $('#target_realisasi').val();
            window.open('dashboard/penerimaan-harian?target=' + target);
        }

    </script>
@endpush
