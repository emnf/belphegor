<!DOCTYPE html>
<html lang="en">
@php
    $pemda = Illuminate\Support\Facades\DB::table('s_pemda')->first();
    $footerYear = date('Y') == '2021' ? date('Y') : '2021-' . date('Y');
@endphp

<head>
    @include('layouts.meta')
    {{-- <title>MY-SPPT | {{ strtoupper($pemda->s_namakabkot) }}</title> --}}

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/responsive.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">

    <style>
        body {
           background-image: url( {{asset('dist/landing/images/2210665445.jpg')}});
           position: relative;
           background-position: center center;
           background-repeat: no-repeat;
           background-size: cover;
           overflow: hidden;
        }

        .header_dashboard {
            color: #fff;
            font-size: 25pt;
            padding: 0px;
            text-align: center;
            border-bottom: 8px double red;
            position: fixed;
            background: #91adf2;
            color: #fff;
            /* Old browsers */
            background: -moz-linear-gradient(top, #91adf2 0%, #9127e7 10%, #470380 31%, #6a1caa 69%, #9127e7 92%, #91adf2 100%);
            /* FF3.6-15 */
            background: -webkit-linear-gradient(top, #91adf2 0%, #9127e7 10%, #470380 31%, #6a1caa 69%, #9127e7 92%, #91adf2 100%);
            /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, #91adf2 0%, #9127e7 10%, #470380 31%, #6a1caa 69%, #9127e7 92%, #91adf2 100%);
            /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#91adf2', endColorstr='#91adf2', GradientType=0);
            /* IE6-9 */
            width: 100%;
            margin: 0;
            z-index: 1000;
            top: auto;
            height: 100px;
	    }
        .jam {
            font-size: 20pt;
            font-family: Arial Black;
            color: yellow;
            font-weight: bold;
            left: 0;
            right: 0;
            bottom: 0;
            position: fixed;
            padding: 2px 15px 15px 20px;
            height: 45px;
            background: rgb(81, 49, 241);
            /* Old browsers */
            background: -moz-linear-gradient(top, #91adf2 0%, #9127e7 10%, #470380 31%, #6a1caa 69%, #9127e7 92%, #91adf2 100%);
            /* FF3.6-15 */
            background: -webkit-linear-gradient(top, #91adf2 0%, #9127e7 10%, #470380 31%, #6a1caa 69%, #9127e7 92%, #91adf2 100%);
            /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, #91adf2 0%, #9127e7 10%, #470380 31%, #6a1caa 69%, #9127e7 92%, #91adf2 100%);
            /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#5131f1', endColorstr='#f2f2f2', GradientType=0);
        }

        .footer_dashboard {
            left: 13%;
            right: 0;
            bottom: 0;
            position: fixed;
            height: 45px;
            padding: 2px 15px 15px 20px;
            color: #fff;
            font-size: 20pt;
            background: rgb(213, 213, 213);
            /* Old browsers */
            background: -moz-linear-gradient(top, rgba(213, 213, 213, 1) 0%, rgba(174, 174, 174, 1) 0%, rgba(1, 1, 1, 1) 10%, rgba(9, 9, 9, 1) 85%, rgba(174, 174, 174, 1) 94%, rgba(174, 174, 174, 1) 10%, rgba(213, 213, 213, 1) 100%);
            /* FF3.6-15 */
            background: -webkit-linear-gradient(top, rgba(213, 213, 213, 1) 0%, rgba(174, 174, 174, 1) 0%, rgba(1, 1, 1, 1) 10%, rgba(9, 9, 9, 1) 85%, rgba(174, 174, 174, 1) 94%, rgba(174, 174, 174, 1) 10%, rgba(213, 213, 213, 1) 100%);
            /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, rgba(213, 213, 213, 1) 0%, rgba(174, 174, 174, 1) 0%, rgba(1, 1, 1, 1) 10%, rgba(9, 9, 9, 1) 85%, rgba(174, 174, 174, 1) 94%, rgba(174, 174, 174, 1) 10%, rgba(213, 213, 213, 1) 100%);
            /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d5d5d5', endColorstr='#d5d5d5', GradientType=0);
        }

    </style>

</head>

<body>
    <div class="header_dashboard">
        <div style=" position: absolute; top: 10px;left: 10px;">
            <img src="{{ $pemda->s_logo }}" width="60px">
        </div>
        <span style="color: white; text-shadow: 1px 1px 2px black, 0 0 25px white, 0 0 5px darkblue;">
            <b style="font-size:20pt;">
                <?= strtoupper('PEMERINTAH ' . $pemda->s_namakabkot . ' ' . $pemda->s_namaibukotakabkot . '</b><BR><b>' . $pemda->s_namainstansi) ?><b>
        </span>
    </div>
    <br><br>
    <div class="wrapper">
    <!-- Content Wrapper -->
    <div class="content-wrapper" style="margin-top: 40px; margin-right: 10px; margin-left: 10px;background-color: #a9b6e4ab;">
      <!-- Main content -->
      <section class="content">
        <div>
          <div class="row">
            <div class="col-md-12">
              <!-- Carousel -->
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators" style="bottom: 0rem;top: 47rem">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                </ol>
                <!-- Slides -->
                <div class="carousel-inner" style="height: 100%;margin-bottom: 10px;margin-top: 20px;">
                  <div class="carousel-item active">
                    <div class="row">
                        <div class="col-md-8">
                            <table cellpadding="4" cellspacing="8" style="font-size:12px; padding-top: 10px; border: 1px solid white; border-collapse: collapse; width: 100%;">
                                <tr>
                                    <th colspan="8" style="width: 5%; border: 1px solid white; border-collapse: collapse; text-align: center; background:#bb00ffab;color: white;font-size: 20pt;">
                                        Data Realisasi PAD Tahun {{$tahun}}
                                    </th>
                                </tr>
                            </table>

                            <table cellpadding="4" cellspacing="8" style="font-size:12px; padding-top: 10px; border: 1px solid white; border-collapse: collapse; width: 100%;margin-top: 5px">
                                <thead>
                                    <tr>
                                        <th style=" background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: white;font-size: 12pt;">
                                            Jenis Pajak
                                        </th>
                                        <th style=" background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: white;font-size: 12pt;">
                                            Target (Rp)
                                        </th>
                                        <th style=" background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: white;font-size: 12pt;">
                                            Realisasi (Rp)
                                        </th>
                                        <th style=" background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: white;font-size: 12pt;">
                                            Realisasi (%)
                                        </th>
                                    </tr>
                                </thead>
                                <?= $perJenis ?>

                            </table>
                        </div>
                        <div class="col-md-4">
                            <table cellpadding="4" cellspacing="8" style="font-size:12px; padding-top: 10px; border: 1px solid white; border-collapse: collapse; width: 100%;">
                                <tr>
                                    <th colspan="2" style="width: 5%; border: 1px solid white; border-collapse: collapse; text-align: center; background:#bb00ffab;color: white;font-size: 15pt;">
                                        Data Realisasi Harian Tanggal {{date('d-m-Y')}}
                                    </th>
                                </tr>
                            </table>
                            <table cellpadding="4" cellspacing="8" style="font-size:12px; padding-top: 10px; border: 1px solid white; border-collapse: collapse; width: 100%;margin-top: 5px">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="width: 5%; border: 1px solid white; border-collapse: collapse; text-align: center; background:#7706a0ef;color: white;font-size: 15pt;">
                                            Realisasi (Rp.)
                                        </th>
                                    </tr>
                                </thead>
                                <?= $perJenisHarian ?>
                            </table>
                        </div>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-12">
                            <table cellpadding="4" cellspacing="8" style="font-size:12px; padding-top: 10px; border: 1px solid white; border-collapse: collapse; width: 100%;">
                                <tr>
                                    <th colspan="8" style="width: 5%; border: 1px solid white; border-collapse: collapse; text-align: center; background:#bb00ffab;color: white;font-size: 20pt;">
                                        Monitoring Penerimaan Harian
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-4" style="margin-top: 5px">
                            <table cellpadding="4" cellspacing="8" style="font-size:12px; padding-top: 10px; border: 1px solid white; border-collapse: collapse; width: 100%;">
                                <thead>
                                    <tr>
                                        <th colspan="2" style=" background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: white;font-size: 12pt;">
                                            SIMPATDA
                                        </th>
                                    </tr>
                                </thead>

                                <?= $simpatdaHarian ?>
                            </table>
                        </div>
                        <div class="col-md-4" style="margin-top: 5px;">
                            <table cellpadding="4" cellspacing="8" style="font-size:12px; padding-top: 10px; border: 1px solid white; border-collapse: collapse; width: 100%;">
                                <thead>
                                    <tr>
                                        <th colspan="4" style=" background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: white;font-size: 12pt;">
                                            BPHTB
                                        </th>
                                    </tr>
                                </thead>
                                <?= $bphtbHarian ?>
                            </table>
                        </div>
                        <div class="col-md-4" style="margin-top: 5px">
                            <table cellpadding="4" cellspacing="8" style="font-size:12px; padding-top: 10px; border: 1px solid white; border-collapse: collapse; width: 100%;">
                                <thead>
                                    <tr>
                                        <th colspan="4" style=" background:#7706a0ef;border: 1px solid white; border-collapse: collapse; text-align: center;color: white;font-size: 12pt;">
                                            PBB
                                        </th>
                                    </tr>
                                </thead>
                                <?= $pbbHarian ?>

                            </table>
                        </div>
                    </div>
                  </div>
                </div>
                <!-- Controls -->
                <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                  <span class="" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                  <span class="" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

<div class="jam" id="clockDisplay" style="text-shadow: 1px 1px 2px black, 0 0 25px black, 0 0 5px darkgreen;"></div>
<div class="footer_dashboard" id="body_text">
	<marquee scrollamount="8" loop="infinite">
		<span id="refresh_text">
			<?php
			foreach ($text as $vt) {
				echo '<img src="' . $pemda->s_logo . '" width="25px" style="margin-left:20px;margin-right:10px;">';
				echo strtoupper($vt['s_text']);
			}
			?>
			<img src="<?= $pemda->s_logo ?>" width="25px" style="margin-left:20px;margin-right:10px;">
		</span>
	</marquee>
</div>



    <script src="{{ asset('dist/landing/js/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/TweenMax.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/wow.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/swiper.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.easing.min.js') }}"></script>

    <script src="{{ asset('dist/landing/js/theme.js') }}"></script>

    <script src="{{ asset('/js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('/js/form-masking.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        function renderTime() {
            var currentTime = new Date();
            var h = currentTime.getHours();
            var m = currentTime.getMinutes();
            var s = currentTime.getSeconds();
            if (h == 0) {
                h = 24;
            }
            if (h < 10) {
                h = "0" + h;
            }
            if (m < 10) {
                m = "0" + m;
            }
            if (s < 10) {
                s = "0" + s;
            }
            var myClock = document.getElementById('clockDisplay');
            myClock.textContent = h + ":" + m + ":" + s + "";
            setTimeout('renderTime()', 1000);
        }
        renderTime();
    </script>
</body>

</html>
