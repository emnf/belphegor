@extends('layouts.master')

@section('judulkiri')
    PENGATURAN USER [EDIT]
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card mb-4">
                <div class="card-header">Edit User</div>
                <div class="card-body">
                    <form action="{{ route('setting-user.edit', $user->uuid) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="col-sm-12 row mb-2">
                            <label class="col-sm-2" for="username">Username</label>
                            <div class="col-md-4">
                                <input id="username" type="username"
                                    class="form-control form-control-sm @error('username') is-invalid @enderror"
                                    name="username" value="{{ old('username') ?? $user->username }}" required autofocus>
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('username'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('username') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 row mb-2">
                            <label class="col-sm-2" for="password">Password</label>
                            <div class="col-md-4">
                                <input id="password" type="password"
                                    class="form-control form-control-sm @error('password') is-invalid @enderror"
                                    name="password" required autocomplete="new-password">
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('password'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 row mb-2">
                            <label class="col-sm-2" for="hakakses">Hak Akses</label>
                            <div class="col-md-3">
                                <select type="text" name="s_id_hakakses"
                                    class="form-control form-control-sm @error('s_id_hakakses') is-invalid @enderror"
                                    id="s_id_hakakses" onchange="getRoleAkses()">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($role as $role)
                                        <option
                                            {{ (old('s_id_hakakses') ?? $role->id) == $user->s_id_hakakses ? 'selected' : '' }}
                                            value="{{ $role->id }}">{{ $role->id . ' | ' . $role->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('s_id_hakakses'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('s_id_hakakses') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-12 row mb-2" id="div_nik" style="display: none;">
                            <label class="col-sm-2" for="name">NIK</label>
                            <div class="col-md-4">
                                <input id="nik" type="text"
                                    class="form-control form-control-sm @error('nik') is-invalid @enderror" name="nik"
                                    value="{{ old('nik') ?? $user->nik }}" autocomplete="nik">
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('nik'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('nik') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 row mb-2">
                            <label class="col-sm-2" for="name">Nama Lengkap</label>
                            <div class="col-md-4">
                                <input id="name" type="text"
                                    class="form-control form-control-sm @error('name') is-invalid @enderror" name="name"
                                    value="{{ old('name') ?? $user->name }}" required autocomplete="name">
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('name'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 row mb-2">
                            <label class="col-sm-2" for="email">Email</label>
                            <div class="col-md-4">
                                <input id="email" type="email"
                                    class="form-control form-control-sm @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') ?? $user->email }}" required autocomplete="email">
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('email'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 row mb-2" id="div_kecamatan" style="display: none;">
                            <label class="col-sm-2" for="kecamatan">{{ $data_pemda->s_namakecamatan }}</label>
                            <div class="col-md-4">
                                <select type="text" name="kecamatan"
                                    class="form-control form-control-sm @error('kecamatan') is-invalid @enderror"
                                    id="kecamatan" onchange="getComboKelurahan()">
                                    <option value="">-- Silahkan Pilih --</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('kecamatan'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('kecamatan') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 row mb-2" id="div_kelurahan" style="display: none;">
                            <label class="col-sm-2" for="kelurahan">{{ $data_pemda->s_namakelurahan }}</label>
                            <div class="col-md-4">
                                <select type="text" name="kelurahan"
                                    class="form-control form-control-sm @error('kelurahan') is-invalid @enderror"
                                    id="kelurahan">
                                    <option value="">-- Silahkan Pilih --</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('kelurahan'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('kelurahan') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 row mb-2" id="div_dukuh" style="display: none;">
                            <label class="col-sm-2" for="kelurahan">Dukuh/Dusun</label>
                            <div class="col-md-4">
                                <select type="text" name="dukuh"
                                    class="form-control form-control-sm @error('dukuh') is-invalid @enderror"
                                    id="dukuh">
                                    <option value="">-- Silahkan Pilih --</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                @if ($errors->has('dukuh'))
                                    <div class="text-danger mt-2">
                                        {{ $errors->first('dukuh') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <a href="{{ route('setting-user') }}" class="btn btn-danger" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Kembali</a>
                        <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i>
                            Update</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            getRoleAkses();
        });

        function getRoleAkses() {
            var hak_akses = $('#s_id_hakakses').val();
            if (hak_akses == 3) {
                $('#div_nik').show();
                $('#div_kecamatan').hide();
                $('#div_kelurahan').hide();
                $('#div_dukuh').hide();
            } else if (hak_akses == 4) {
                $('#div_nik').hide();
                $('#div_kecamatan').show();
                $('#div_kelurahan').show();
                $('#div_dukuh').hide();
                comboKecamatan();
            } else if (hak_akses == 5) {
                $('#div_nik').hide();
                $('#div_kecamatan').show();
                $('#div_kelurahan').show();
                $('#div_dukuh').show();
                comboKecamatan();
            } else {
                $('#div_nik').hide();
                $('#div_kecamatan').hide();
                $('#div_kelurahan').hide();
                $('#div_dukuh').hide();
            }
        }

        const kecamatan = JSON.parse('{!! $kecamatan !!}');

        function comboKecamatan() {
            var option = '<option value="">-- Silahkan Pilih --</option>';
            kecamatan.forEach(el => {
                if (el.KD_KECAMATAN == '{{ $user->kecamatan }}') {
                    option +=
                        `<option value="${ el.KD_KECAMATAN }" selected>${ el.KD_KECAMATAN + ' || ' + el.NM_KECAMATAN }</option>`;
                } else {
                    option +=
                        `<option value="${ el.KD_KECAMATAN }">${ el.KD_KECAMATAN + ' || ' + el.NM_KECAMATAN }</option>`;
                }
            });

            $('#kecamatan').html(option);
            comboKelurahan($('#kecamatan').val());
        }

        $("#kecamatan").change(function() {
            comboKelurahan($('#kecamatan').val());
        });

        function comboKelurahan(a) {
            $.ajax({
                url: '{{ url('setting-user/comboKelurahan') }}',
                type: 'GET',
                data: {
                    id: a
                }
            }).then(function(data) {
                let option = '<option value="">-- Silahkan Pilih --</option>';
                data.forEach(el => {
                    if (el.KD_KELURAHAN == '{{ $user->kelurahan }}') {
                        option +=
                            `<option value="${ el.KD_KELURAHAN }" selected>${ el.KD_KELURAHAN + ' || ' + el.NM_KELURAHAN }</option>`;
                    } else {
                        option +=
                            `<option value="${ el.KD_KELURAHAN }">${ el.KD_KELURAHAN + ' || ' + el.NM_KELURAHAN }</option>`;
                    }
                });
                $('#kelurahan').html(option);
                comboDukuh($('#kecamatan').val(), $('#kelurahan').val());
            });
        }

        $("#kelurahan").change(function() {
            comboDukuh($('#kecamatan').val(), $('#kelurahan').val());
        });

        function comboDukuh(a, b) {
            $.ajax({
                url: '{{ url('setting-user/comboDukuh') }}',
                type: 'GET',
                data: {
                    id: a,
                    key: b
                }
            }).then(function(data) {
                let option = '<option value="">-- Silahkan Pilih --</option>';
                data.forEach(el => {
                    if (el.kd_dukuh == '{{ $user->dukuh }}') {
                        option +=
                            `<option value="${ el.kd_dukuh }" selected>${ el.kd_dukuh + ' || ' + el.nm_dukuh }</option>`;
                    } else {
                        option +=
                            `<option value="${ el.kd_dukuh }">${ el.kd_dukuh + ' || ' + el.nm_dukuh }</option>`;
                    }
                });
                $('#dukuh').html(option);
            });
        }
    </script>
@endpush
