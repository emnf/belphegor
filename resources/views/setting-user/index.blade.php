@extends('layouts.master')

@section('judulkiri')
PENGATURAN USER
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-success">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="{{ route('setting-user.tambah') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> Tambah</a>
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="" class="bg-success text-center">NO</th>
                                <th style="" class="bg-success text-center">Name</th>
                                <th style="" class="bg-success text-center">Username</th>
                                <th style="" class="bg-success text-center">Email</th>
                                <th style="" class="bg-success text-center">Hak Akses</th>
                                <th style="" class="bg-success text-center">Create At</th>
                                <th style="width: 160px;" class="bg-success text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-name"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-username"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-email"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-hakakses"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-created_at" readonly></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="7"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function() {

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'setting-user/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "name"
            },
            {
                sortable: true,
                name: "username"
            },
            {
                sortable: true,
                name: "email"
            },
            {
                sortable: true,
                name: "s_id_hakakses"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"></i> Edit'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"></i> Hapus'
            }
        ]

    });

    $("#filter-created_at").change(function() {
        search();
    });

    $("#filter-name, #filter-username, #filter-email, #filter-hakakses").keyup(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            name: $("#filter-name").val(),
            username: $("#filter-username").val(),
            email: $("#filter-email").val(),
            hakakses: $("#filter-hakakses").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
        title: 'Yakin anda akan menghapus?',
        text: "Data akan dihapus secara permanen.",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya',
        reverseButtons: true
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    url: 'setting-user/delete/{' + a + '}',
                    method: "GET",
                    data: {
                        id: a
                    },
                    error: function(request, error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Gagal',
                            text: 'Data Gagal Dihapus.',
                            showConfirmButton: false,
                            timer: 2500,
                            timerProgressBar: true,
                        })
                    },
                    success: function() {
                        Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data anda berhasil dihapus secara permanen.',
                            showConfirmButton: false,
                            timer: 2500,
                            timerProgressBar: true,
                        })
                        datatables.reload();
                    }
                });

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            )
            {
                swalWithBootstrapButtons.fire(
                'Dibatalkan',
                'Data tidak dihapus secara permanen.',
                'error'
                )
            }
        })
    }

</script>
@endpush
