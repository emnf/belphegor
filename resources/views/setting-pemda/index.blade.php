@extends('layouts.master')

@section('judulkiri')
    PENGATURAN PEMDA
@endsection

@section('judulkanan')
    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Pemda</li>
@endsection

@section('content')
    <div class="card card-outline card-success shadow-sm">
        <div class="card-body">
            <form action="{{ route('setting-pemda.createOrUpdate') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <input type="hidden" class="form-control" name="s_idpemda" id="s_idpemda"
                    value="{{ optional($pemda)->s_idpemda }}">
                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Nama Provinsi</label>
                        <input type="text" class="form-control" name="s_namaprov" id="s_namaprov"
                            value="{{ old('s_namaprov', optional($pemda)->s_namaprov) }}" readonly>
                        @if ($errors->has('s_namaprov'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_namaprov') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Kode Provinsi</label>
                        <input type="text" class="form-control col-lg-2 col-sm-2" name="s_kodeprovinsi"
                            id="s_kodeprovinsi" onkeypress="return numberOnly(event)"
                            value="{{ old('s_kodeprovinsi', optional($pemda)->s_kodeprovinsi) }}" maxlength="2" readonly>
                        @if ($errors->has('s_kodeprovinsi'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_kodeprovinsi') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Nama Kabupaten/Kota</label>
                        <input type="text" class="form-control" name="s_namakabkot" id="s_namakabkot"
                            value="{{ old('s_namakabkot', optional($pemda)->s_namakabkot) }}" readonly>
                        @if ($errors->has('s_namakabkot'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_namakabkot') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Kode Kabupaten/Kota</label>
                        <input type="text" class="form-control col-lg-2 col-sm-2" name="s_kodekabkot" id="s_kodekabkot"
                            onkeypress="return numberOnly(event)"
                            value="{{ old('s_kodekabkot', optional($pemda)->s_kodekabkot) }}" maxlength="2" readonly>
                        @if ($errors->has('s_kodekabkot'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_kodekabkot') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Nama Ibukota Kabupaten/Kota </label>
                        <input type="text" class="form-control" name="s_namaibukotakabkot" id="s_namaibukotakabkot"
                            value="{{ old('s_namaibukotakabkot', optional($pemda)->s_namaibukotakabkot) }}" readonly>
                        @if ($errors->has('s_namaibukotakabkot'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_namaibukotakabkot') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Nama Instansi </label>
                        <input type="text" class="form-control" name="s_namainstansi" id="s_namainstansi"
                            value="{{ old('s_namainstansi', optional($pemda)->s_namainstansi) }}" readonly>
                        @if ($errors->has('s_namainstansi'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_namainstansi') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Nama Singkatan Instansi </label>
                        <input type="text" class="form-control col-6" name="s_namasingkatinstansi"
                            id="s_namasingkatinstansi"
                            value="{{ old('s_namasingkatinstansi', optional($pemda)->s_namasingkatinstansi) }}" readonly>
                        @if ($errors->has('s_namasingkatinstansi'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_namasingkatinstansi') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Alamat Kantor Instansi </label>
                        <textarea class="form-control" name="s_alamatinstansi" id="s_alamatinstansi" cols="30" rows="2" readonly>{{ old('s_alamatinstansi', optional($pemda)->s_alamatinstansi) }}</textarea>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>No. Telp. Kantor </label>
                        <input type="text" class="form-control col-6" name="s_notelinstansi" id="s_notelinstansi"
                            value="{{ old('s_notelinstansi', optional($pemda)->s_notelinstansi) }}" readonly>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Kode Pos </label>
                        <input type="text" class="form-control col-4" name="s_kodepos" id="s_kodepos"
                            onkeypress="return numberOnly(event)"
                            value="{{ old('s_kodepos', optional($pemda)->s_kodepos) }}" maxlength="5" readonly>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Surel </label>
                        <input type="text" class="form-control col-6" name="s_email" id="s_email"
                            value="{{ old('s_email', optional($pemda)->s_email) }}" readonly>
                        @if ($errors->has('s_email'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_email') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Nama Bank </label>
                        <input type="text" class="form-control" name="s_namabank" id="s_namabank"
                            value="{{ old('s_namabank', optional($pemda)->s_namabank) }}" readonly>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Kode Rekening Bank </label>
                        <input type="text" class="form-control col-6" name="s_norekbank" id="s_norekbank"
                            onkeypress="return numberOnly(event)"
                            value="{{ old('s_norekbank', optional($pemda)->s_norekbank) }}" readonly>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Nama Cabang </label>
                        <input type="text" class="form-control" name="s_namacabang" id="s_namacabang"
                            value="{{ old('s_namacabang', optional($pemda)->s_namacabang) }}" readonly>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Kode Cabang </label>
                        <input type="text" class="form-control col-6" name="s_kodecabang" id="s_kodecabang"
                            value="{{ old('s_kodecabang', optional($pemda)->s_kodecabang) }}" readonly>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Penamaan Kecamatan</label>
                        <input type="text" class="form-control" name="s_namakecamatan" id="s_namakecamatan"
                            value="{{ old('s_namakecamatan', optional($pemda)->s_namakecamatan) }}" readonly>
                        @if ($errors->has('s_namakecamatan'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_namakecamatan') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Penamaan Desa/Kelurahan</label>
                        <input type="text" class="form-control" name="s_namakelurahan" id="s_namakelurahan"
                            value="{{ old('s_namakelurahan', optional($pemda)->s_namakelurahan) }}" readonly>
                        @if ($errors->has('s_namakelurahan'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_namakelurahan') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>URL APPS </label>
                        <input type="text" class="form-control" name="s_urlapps" id="s_urlapps"
                            value="{{ old('s_urlapps', optional($pemda)->s_urlapps) }}" readonly>
                    </div>
                </div>

                <div class="row" id="upload_row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label>Logo Kabupaten/Kota </label>
                        <input type="file" class="form-control col-6" name="s_logo" id="s_logo"
                            value="{{ old('s_logo') }}">
                        @if ($errors->has('s_logo'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_logo') }}
                            </div>
                        @else
                            <div class="text-info mt-2">
                                Ukuran maximal file 1MB.
                            </div>
                        @endif
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <img src="{{ $pemda->s_logo }}" id="preview" class="img-fluid" alt="logo-pemda"
                        style="max-width: 200px">
                </div>


        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-md float-right" id="submit">
                <i class="fas fa-save"></i>
                Simpan
            </button>
            <a href="{{ url('setting-pemda') }}" class="btn btn-danger btn-md float-left" id="batalBtn">
                <i class="fas fa-redo"></i>
                Batal
            </a>

            <button type="button" class="btn btn-warning btn-md btn-block" id="editBtn">
                <i class="fas fa-edit"></i>
                Edit
            </button>

            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(() => {

            $('#upload_row').css('display', 'none');
            $('#submit').css('display', 'none');
            $('#batalBtn').css('display', 'none');

            @if (count($errors) > 0)
                $('#upload_row').css('display', 'block');
                $('#submit').css('display', 'block');
                $('#batalBtn').css('display', 'block');
                $('#editBtn').css('display', 'none');
                $('input').removeAttr('readonly');
                $('textarea').removeAttr('readonly');
            @endif
        });

        $(document).on("click", "#s_logo", () => {
            var file = $(this).parents().find("#file");
            file.trigger("click");
        });

        $('input[type="file"]').change((e) => {
            var input = document.querySelector('input[type=file]');
            var file = input.files[0];

            var reader = new FileReader();
            reader.onload = (e) => {
                document.getElementById("preview").src = e.target.result;
            };

            reader.readAsDataURL(file);
        });

        $(".alert").fadeTo(2000, 500).slideUp(500, function() {
            $(".alert").slideUp(500);
        });

        $('#editBtn').on("click", () => {

            $('#upload_row').css('display', 'block');
            $('#submit').css('display', 'block');
            $('#batalBtn').css('display', 'block');
            $('#editBtn').css('display', 'none');
            $('input').removeAttr('readonly');
            $('textarea').removeAttr('readonly');
        })

        function numberOnly(e) {
            var input = (e.which) ? e.which : e.keyCode

            if (input > 31 && (input < 48 || input > 57))
                return false;
            return true;
        }
    </script>
@endpush
