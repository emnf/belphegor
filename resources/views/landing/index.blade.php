<!DOCTYPE html>
<html lang="en">
@php
    $pemda = Illuminate\Support\Facades\DB::table('s_pemda')->first();
    $footerYear = date('Y') == '2021' ? date('Y') : '2021-' . date('Y');
@endphp

<head>
    @include('layouts.meta')
    {{-- <title>MY-SPPT | {{ strtoupper($pemda->s_namakabkot) }}</title> --}}

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/responsive.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">

    <style>
        body {
           background-image: url( {{asset('dist/landing/images/2210665445.jpg')}});
           position: relative;
           background-position: center center;
           background-repeat: no-repeat;
           background-size: cover;
           overflow: hidden;
        }
        /*  */
    </style>

</head>

<body>
    <div class="page-wrapper">
        {{-- <header class="">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation">
                <div class="container clearfix">
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="{{ $pemda->s_logo }}" width="40">
                        </a>
                        <button class="menu-toggler" data-target=".main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div>

                    <div class="main-navigation">
                        <ul class=" one-page-scroll-menu navigation-box">
                            <li class="current scrollToLink">
                                <a href="#halaman-utama">Beranda</a>
                            </li>
                        </ul>
                    </div>
                    <div class="right-side-box">
                    </div>
                </div>

            </nav>
        </header> --}}
        <br><br>
        <section class="section-three" id="fitur">
            <div class="container">
                <div class="block-title text-center">
                    <p style="font-size: 22px;color: white">PEMERINTAH {{ strtoupper($pemda->s_namakabkot)}}</p>
                    <p style="font-size: 40px;color: white">{{ strtoupper($pemda->s_namainstansi)}}</p>
                    <p style="color: white">Aplikasi Pendapatan Daerah</p>
                </div>
                <div class="service-one__content-wrap mb-lg-5">
                    <div class="service-one__single">
                        <a href="http://mysimpatda.fakfakkab.go.id/" target="_blank">
                        <div class="service-one__inner">
                            <i class="fas fa-money-check fa-2x"></i>
                            <h3 class="service-one__title">MySIMPATDA</h3>
                            <div class="mt-4 p-2">
                                {{-- <span>Sistem Manajemen Pendapatan Pajak Daerah.</span> --}}
                            </div>
                        </div>
                    </a>
                    </div>
                    <div class="service-one__single">
                        <a href="http://mysptpd.fakfakkab.go.id/" target="_blank">
                        <div class="service-one__inner">
                            <i class="fas fa-paper-plane fa-2x"></i>
                            <h3 class="service-one__title">MySPTPD</h3>
                            <div class="mt-4 p-2">
                                {{-- <span>Pelaporan Surat Pemberitahuan Pajak Daerah.</span> --}}
                            </div>
                        </div>
                    </a>
                    </div>
                    <div class="service-one__single">
                        <a href="http://mysppt.fakfakkab.go.id/" target="_blank">
                        <div class="service-one__inner">
                            <i class="fas fa-newspaper fa-2x"></i>
                            <h3 class="service-one__title">MySPPT</h3>
                            <div class="mt-4 p-2">
                                {{-- <span>Surat Pemberitahuan Pajak Terutang.</span> --}}
                            </div>
                        </div>
                    </a>
                    </div>
                    <div class="service-one__single">
                        <a href="http://mybphtb.fakfakkab.go.id/" target="_blank">
                        <div class="service-one__inner">
                            <i class="fas fa-map-marked-alt fa-2x"></i>
                            <h3 class="service-one__title">MyBPHTB</h3>
                            <div class="mt-4 p-2">
                                {{-- <span>Bea Perolehan Hak atas Tanah dan Bangunan adalah pajak atas perolehan hak atas tanah dan/atau bangunan Online.</span> --}}
                            </div>
                        </div>
                    </a>
                    </div>
                    <div class="service-one__single">
                        <a href="{{ url('dashboard') }}" target="_blank">
                        <div class="service-one__inner">
                            <i class="fas fa-chart-line fa-2x"></i>
                            <h3 class="service-one__title">MyDASHBOARD</h3>
                            <div class="mt-4 p-2">
                                {{-- <span>Bea Perolehan Hak atas Tanah dan Bangunan adalah pajak atas perolehan hak atas tanah dan/atau bangunan Online.</span> --}}
                            </div>
                        </div>
                    </a>
                    </div>

                </div>
            </div>
        </section>



    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <script src="{{ asset('dist/landing/js/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/TweenMax.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/wow.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/swiper.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.easing.min.js') }}"></script>

    <script src="{{ asset('dist/landing/js/theme.js') }}"></script>

    <script src="{{ asset('/js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('/js/form-masking.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">

    </script>
</body>

</html>
