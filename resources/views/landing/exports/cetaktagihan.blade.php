@php
// $tanggal = date('d', strtotime($data->t_tglpermohonan));
// $bulan = MenuHelper::namaBulan(date('m', strtotime($data->t_tglpermohonan))); // date('m', strtotime($data->t_tglpermohonan));
// $tahun = date('Y', strtotime($data->t_tglpermohonan));
@endphp
<style>
    h3 {
        text-align: center;
    }

    .background-gray-50 {
        background: rgb(226, 225, 225);
    }

    .background-black {
        background: black;
        color: white;
    }

    .text-right {
        text-align: right;
    }

    .text-center {
        text-align: center;
    }

    .border-left {
        border-left: 1px solid #000;
    }

    .border-right {
        border-right: 1px solid #000;
    }

    .border-top {
        border-top: 1px solid #000;
    }

    .border-bottom {
        border-bottom: 1px solid #000;
    }
</style>
<div>
    @include('components.kop')

    <table width="100%" cellspacing="2" cellpadding="5"
        style=" margin-bottom: 20px;border-collapse: collapse;font-size:10pt;">
        <tr>
            <td class="text-center" colspan="8" style="padding:10px;">
                <h2>DAFTAR TAGIHAN</h2>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                {{-- NIK : {{ $data->first()['subjek_pajak_id'] }} --}}
                NOP : {{ $nop }}
            </td>
        </tr>
        <tr style="background: #f2f2f2;">
            <th style="border:1px solid black;width:5%;">No.</th>
            <th style="border:1px solid black;width:10%;">Tahun</th>
            <th style="border:1px solid black;width:15%;">Tanggal<br>Jatuh Tempo</th>
            <th style="border:1px solid black;width:13%;">Pembayaran Ke</th>
            <th style="border:1px solid black;width:15%;">Pokok<br>(Rp)</th>
            <th style="border:1px solid black;width:10%;">Denda<br>(Rp)</th>
            <th style="border:1px solid black;width:15%;">Total<br>(Rp)</th>
            <th style="border:1px solid black;width:10%;">Status<br>Bayar</th>
        </tr>
        @php
            $counter = 1;
            $totalPokok = 0;
            $totalDenda = 0;
            $totalBayar = 0;
        @endphp
        @foreach ($data as $v)
            :
            <?php
            $tgljatuhtempo = date('Y-m-d', strtotime($v['TGL_JATUH_TEMPO_SPPT']));
            $tglsekarang = date('Y-m-d');
            $ts1 = strtotime($tgljatuhtempo);
            $ts2 = strtotime($tglsekarang);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $day1 = date('d', $ts1);
            $day2 = date('d', $ts2);
            if ($day1 < $day2) {
                $tambahanbulan = 1;
            } else {
                $tambahanbulan = 0;
            }

            $jmlbulan = ($year2 - $year1) * 12 + ($month2 - $month1 + $tambahanbulan);
            if ($jmlbulan > 24) {
                $jmlbulan = 24;
                $jmldenda = (($jmlbulan * 2) / 100) * $v['PBB_YG_HRS_DIBAYAR_SPPT'];
            } elseif ($jmlbulan <= 0) {
                $jmlbulan = 0;
                $jmldenda = (($jmlbulan * 2) / 100) * $v['PBB_YG_HRS_DIBAYAR_SPPT'];
            } else {
                $jmldenda = (($jmlbulan * 2) / 100) * $v['PBB_YG_HRS_DIBAYAR_SPPT'];
            }

            $pembayaran_ke = $v['TGL_PEMBAYARAN_SPPT'] ? 'Ke-' . $v['PEMBAYARAN_SPPT_KE'] . ' (' . date('d-m-Y', strtotime($v['TGL_PEMBAYARAN_SPPT'])) . ')' : '-';
            $pembayaran = $v['TGL_PEMBAYARAN_SPPT'] ? number_format($v['JML_PBB_YG_DIBAYAR'], 0, ',', '.') : 'Belum';
            $jumlahdenda = $v['TGL_PEMBAYARAN_SPPT'] ? $v['JML_DENDA_SPPT'] : round($jmldenda);
            ?>
            <tr>
                <td style="border:1px solid black;text-align: center;">{{ $counter++ }}</td>
                <td style="border:1px solid black;text-align: center;">{{ $v['THN_PAJAK_SPPT'] }}</td>
                <td style="border:1px solid black;text-align: center;">
                    {{ date('d-m-Y', strtotime($v['TGL_JATUH_TEMPO_SPPT'])) }}</td>
                <td style="border:1px solid black;text-align: center;">{{ $pembayaran_ke }}</td>
                <td style="border:1px solid black;text-align: right;">
                    {{ number_format($v['PBB_YG_HRS_DIBAYAR_SPPT'], 0, ',', '.') }}</td>
                <td style="border:1px solid black;text-align: right;">{{ number_format($jumlahdenda, 0, ',', '.') }}
                </td>
                <td style="border:1px solid black;text-align: right;">
                    {{ number_format($v['PBB_YG_HRS_DIBAYAR_SPPT'] + $jumlahdenda, 0, ',', '.') }}</td>
                <td style="border:1px solid black;text-align: right;">{{ $pembayaran }}</td>
            </tr>
            <?php
            $totalPokok += $v['PBB_YG_HRS_DIBAYAR_SPPT'];
            $totalDenda += $jumlahdenda;
            $totalBayar += $v['TGL_PEMBAYARAN_SPPT'] ? $v['JML_PBB_YG_DIBAYAR'] : 0;
            ?>
        @endforeach;
        <tr style="background: #f2f2f2;">
            <th style="border:1px solid black;" colspan="4">Total Tunggakan (Rp)</th>
            <th style="border:1px solid black;text-align: right;">{{ number_format($totalPokok, 0, ',', '.') }}</th>
            <th style="border:1px solid black;text-align: right;">{{ number_format($totalDenda, 0, ',', '.') }}</th>
            <th style="border:1px solid black;text-align: right;">
                {{ number_format($totalPokok + $totalDenda, 0, ',', '.') }}</th>
            <th style="border:1px solid black;text-align: right;">{{ number_format($totalBayar, 0, ',', '.') }}</th>
        </tr>
    </table>

    <small>
        * Denda 2% per bulan <br>
        * Denda Maksimal 48% (24 bulan)<br>
        * Denda yang tertulis diatas, dihitung dengan posisi tanggal {{ date('d-m-Y H:i:s') }}<br>

        <div style="margin-top: 10px;">{!! str_ireplace('<?xml version="1.0" encoding="UTF-8"?>', '', $qrcode) !!}</>
    </small>

</div>
