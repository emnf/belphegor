<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Info SPPT||{{ strtoupper($pemda->s_namakabkot) . ' - ' . $nop }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ $pemda->s_logo }}" rel="icon">

    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/ionicons-2.0.1/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition layout-top-nav">

    <div class="content-wrapper">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Data Objek Pajak</h2>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-bordered table-hover" width="100%">
                                    <tr>
                                        <td width="30%">1. NOP</td>
                                        <td colspan="3"><strong>{{ $nop }} <i class="fa fa-check text-green"></i></strong></td>
                                    </tr>
                                    <tr>
                                        <td>2. Nama Wajib Pajak</td>
                                        <td colspan="3"><strong>{{ $data->nm_wp }} <i class="fa fa-check text-green"></i></strong></td>
                                    </tr>
                                    <tr>
                                        <td>3. Alamat Wajib Pajak</td>
                                        <td colspan="3"><strong>{{ $data->jalan_wp }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>4. Lokasi Objek Pajak</td>
                                        <td colspan="3"><strong>{{ $data->jalan_op }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>5. Kelurahan Objek Pajak</td>
                                        <td width="20%"><strong>{{ $data->nm_kelurahan }}</strong></td>
                                        <td width="30%">6. Kecamatan Objek Pajak</td>
                                        <td width="20%"><strong>{{ $data->nm_kecamatan }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>7. Tgl Pendataan</td>
                                        <td><strong>{{ date('d-m-Y', strtotime($data->tgl_pendataan_op)) }}</strong></td>
                                        <td>8. Tgl Perekaman</td>
                                        <td><strong>{{ date('d-m-Y', strtotime($data->tgl_perekaman_op)) }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>9. a. Luas Bumi (M<sup>2</sup>)</td>
                                        <td class="text-right"><strong>{{ number_format($data->total_luas_bumi, 0, ',', '.') }}</strong></td>
                                        <td>10. a. Luas Bangunan (M<sup>2</sup>)</td>
                                        <td class="text-right"><strong>{{ number_format($data->total_luas_bng, 0, ',', '.') }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>9. b. NJOP Bumi/M<sup>2</sup> (Rp.)</td>
                                        <td class="text-right"><strong>{{ number_format($data->njop_bumi / $data->total_luas_bumi, 0, ',', '.') }}</strong></td>
                                        <td>10. b. NJOP Bumi/M<sup>2</sup> (Rp.)</td>
                                        <td class="text-right"><strong>{{ number_format(($data->total_luas_bng > 0) ? ($data->njop_bng / $data->total_luas_bng) : 0, 0, ',', '.') }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>9. c. Total NJOP Bumi (Rp.)</td>
                                        <td class="text-right"><strong>{{ number_format($data->njop_bumi, 0, ',', '.') }}</strong></td>
                                        <td>10. c. Total NJOP Bumi (Rp.)</td>
                                        <td class="text-right"><strong>{{ number_format($data->njop_bng, 0, ',', '.') }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>11. Total NJOP SPPT ( 9c + 10c ) (Rp.)</td>
                                        <td colspan="3" class="text-right"><strong>{{ number_format($data->njop_bumi + $data->njop_bng, 0, ',', '.') }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>12. PBB yang terhutang (Rp.)</td>
                                        <td colspan="3" class="text-right"><strong>{{ number_format($data->pbb_terhutang_sppt, 0, ',', '.') }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>13. Faktor Pengurang SPPT</td>
                                        <td colspan="3" class="text-right"><strong>{{ number_format($data->faktor_pengurang_sppt, 0, ',', '.') }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>14. PBB yang harus dibayar (Rp.)</td>
                                        <td colspan="3" class="text-right"><strong>{{ number_format($data->PBB_YG_HRS_DIBAYAR_SPPT, 0, ',', '.') }}</strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Rincian Informasi Objek Pajak - Bumi (Tanah)</h2>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-condensed table-hover" width="100%">
                                    <tr>
                                        <td width="40%">No. Bumi</td>
                                        <td><strong></strong></td>
                                    </tr>
                                    <tr>
                                        <td>Guna Bumi</td>
                                        <td><strong></strong></td>
                                    </tr>
                                    <tr>
                                        <td>Luas Bumi (M<sup>2</sup>)</td>
                                        <td><strong>{{ number_format($data->total_luas_bumi, 0, ',', '.') }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>ZNT</td>
                                        <td><strong>{{ $data->kd_kls_tanah }}</strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Rincian Informasi Objek Pajak - Bangunan</h2>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-bordered table-hover" width="100%">
                                    <tr>
                                        <td width="25%">1. No. Bangunan</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                        <td width="25%">8. Konstruksi</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">2. Guna Bangunan</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                        <td width="25%">9. Jenis Atap</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">3. Tahun Bangunan</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                        <td width="25%">10. Dinding</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">4. Tahun Renovasi</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                        <td width="25%">11. Lantai</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">5. Luas Bangunan</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                        <td width="25%">12. Langit-langit</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">6. Jumlah Lantai</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                        <td width="25%">13. Tanggal Pendataan</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">7. Kondisi Bangunan</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                        <td width="25%">14. Tanggal Perekaman</td>
                                        <td width="25%"><strong>Belum Terekam</strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Risalah Pembayaran PBB-P2</h2>
                                <p>PBB-P2 — Pajak Bumi dan Bangunan Pedesaan dan Perkotaan</p>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr class="bg-green">
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center">Tahun</th>
                                            <th class="text-center">L.Bumi</th>
                                            <th class="text-center">Jth.Tempo</th>
                                            <th class="text-center">Jml.Ketetapan</th>
                                            <th class="text-center">Tgl.Bayar</th>
                                            <th class="text-center">Jmlh.Bayar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($riwayat as $row)
                                        @if ($row->tgl_pembayaran_sppt)
                                        <tr>
                                            <td class="text-center">{{ $i++ }}.</td>
                                            <td>{{ $row->NM_WP_SPPT }}</td>
                                            <td class="text-center">{{ $row->thn_pajak_sppt }}</td>
                                            <td class="text-right">{{ number_format($row->luas_bumi_sppt, 0, ',', '.') }}</td>
                                            <td class="text-center">{{ date('d/m/Y', strtotime($row->tgl_jatuh_tempo_sppt)) }}</td>
                                            <td class="text-right">{{ number_format($row->PBB_YG_HRS_DIBAYAR_SPPT, 0, ',', '.') }}</td>
                                            <td class="text-center">{{ date('d/m/Y', strtotime($row->tgl_pembayaran_sppt)) }}</td>
                                            <td class="text-right">{{ number_format($row->JML_PBB_YG_DIBAYAR, 0, ',', '.') }}</td>
                                        </tr>
                                        @else
                                        <tr class="bg-red">
                                            <td class="text-center">{{ $i++ }}.</td>
                                            <td>{{ $row->NM_WP_SPPT }}</td>
                                            <td class="text-center">{{ $row->thn_pajak_sppt }}</td>
                                            <td class="text-right">{{ number_format($row->luas_bumi_sppt, 0, ',', '.') }}</td>
                                            <td class="text-center">{{ date('d/m/Y', strtotime($row->tgl_jatuh_tempo_sppt)) }}</td>
                                            <td class="text-right">{{ number_format($row->PBB_YG_HRS_DIBAYAR_SPPT, 0, ',', '.') }}</td>
                                            <td class="text-center" colspan="2">Belum Bayar</td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
</body>
</html>
