<div class="modal fade" id="modal-tunggakan">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5>Detail NOP SPPT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h5>NOP &nbsp;&nbsp;&nbsp; : &nbsp;<b><span id="noptampil"></span></b></h5>
                        <h5>Nama : &nbsp;<b><span id="namawp"></span></b></h5>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-vertical-center table-sm" id="datagrid-table">
                                <thead>
                                    <tr class="bg-warning">
                                        <th class="text-center">No</th>
                                        <th class="text-center">Tahun<br>Pajak</th>
                                        <th class="text-center">Tanggal<br>Jatuh Tempo</th>
                                        <th class="text-center">Pembayaran Ke</th>
                                        <th class="text-center">Pokok <br>(Rp.)</th>
                                        <th class="text-center">Denda <br>(Rp.)</th>
                                        <th class="text-center">Total <br>(Rp.)</th>
                                        <th class="text-center">Jumlah Bayar <br>(Rp.)</th>
                                    </tr>
                                </thead>
                                <tbody id="gridBody"></tbody>
                            </table>
                        </div>

                        <h5>Catatan :</h5>
                        <p class="text-red">
                            <small>
                            <i class="fa fa-info-circle"></i> Denda 2% per bulan<br>
                            <i class="fa fa-info-circle"></i> Denda Maksimal 48% (24 bulan) <br>
                            <i class="fa fa-info-circle"></i> Denda yang tertulis diatas, dihitung dengan posisi tanggal {{ date('d-m-Y') }}
                            </small>
                        </p>

                        <div class="row mt-4">
                            <div class="col-12">
                                <button type="button" class="btn btn-danger" onclick="cetaktagihan()"><i class="fa fa-file-pdf"></i> CETAK TAGIHAN</button>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
