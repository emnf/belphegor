@extends('layouts.master')
{{-- <meta content="Divi v.4.7.0" name="generator"/> --}}

@section('judulkiri')
    PENGATURAN UBAH PASSWORD
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-success card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{ asset('dist/img/avatar5.png') }}"
                            alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{ strtoupper(Auth::user()->name) }}</h3>

                    <p class="text-muted text-center">{{ Auth::user()->getRoleNames()->first() }}</p>

                    @if (Auth::user()->s_id_hakakses == 3)
                        <p class="text-muted text-center">NIK : {{ Auth::user()->nik }}</p>
                    @endif

                    {{-- <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
              <b>Followers</b> <a class="float-right">1,322</a>
            </li>
            <li class="list-group-item">
              <b>Following</b> <a class="float-right">543</a>
            </li>
            <li class="list-group-item">
              <b>Friends</b> <a class="float-right">13,287</a>
            </li>
          </ul> --}}

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card card-outline card-success">
                <form action="{{ route('ubah-pass.updatepass') }}" id="change_password_form" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-3 col-form-label">Password Lama</label>
                            <div class="col-sm-9">
                                <input id="old_password" type="password"
                                    class="form-control @error('old_password') is-invalid @enderror" name="old_password"
                                    autocomplete="old_password">
                                @error('old_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-3 col-form-label">Password Baru</label>
                            <div class="col-sm-9">
                                <input id="new_password" type="password"
                                    class="form-control @error('new_password') is-invalid @enderror" name="new_password"
                                    autocomplete="new_password">
                                @error('new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-3 col-form-label">Ulangi Password Baru</label>
                            <div class="col-sm-9">
                                <input id="confirm_password" type="password"
                                    class="form-control @error('confirm_password') is-invalid @enderror"
                                    name="confirm_password" autocomplete="confirm_password">
                                @error('confirm_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                <button type="submit" class="btn btn-outline-warning">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push('script')
    <script type="text/javascript">
        var masukanpass = document.getElementById('old_password');
        var input = document.getElementById('old_password');
        icon = document.getElementById('icon');

        icon.onclick = function() {
            if (input.className == 'active') {
                input.setAttribute('type', 'text');
                icon.className = 'fa fa-eye';
                input.className = '';

            } else {
                input.setAttribute('type', 'password');
                icon.className = 'fa fa-eye-slash';
                input.className = 'active';
            }
        }

        $('#change_password_form').validate({
            ignore: '.ignore',
            errorClass: 'invalid',
            validClass: 'success',
            rules: {
                old_password: {
                    required: true,
                    minlength: 8,
                    maxlength: 100
                },
                new_password: {
                    required: true,
                    minlength: 8,
                    maxlength: 100
                },
                confirm_password: {
                    required: true,
                    equalTo: '#new_password'
                },
            },
            message: {

                old_password: {
                    required: "Masukkan Password Lama"
                },
                new_password: {
                    required: "Masukkan Password Baru"
                },
                confirm_password: {
                    required: "ulangi Password Baru"
                },

            },
            submitHandler: function(form) {
                $.LoadingOverlay("show");
                form.submit();
            }

        });
    </script>
@endpush
