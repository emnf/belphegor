<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Gambar</label>
    <div class="col-md-4">
        <input type="file" name="thumbnail" id="thumbnail" class="form-control @error('thumbnail') is-invalid @enderror">
    </div>
    <div class="col-md-4">   
        @if ($errors->has('thumbnail'))
        <div class="text-danger mt-2">
            {{ $errors->first('thumbnail') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Status</label>
    <div class="col-md-4">
        <select type="text" name="status" class="form-control @error('status') is-invalid @enderror" id="status">
            <option disabled selected value="">Choose One</option>
            @foreach ($status as $status)
            <option {{ (old('status') ?? $logins->s_id_status) == $status->s_id_status ? 'selected' : '' }} value="{{ $status->s_id_status }}">{{ $status->s_nama_status }}
            </option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">   
        @if ($errors->has('status'))
        <div class="text-danger mt-2">
            {{ $errors->first('status') }}
        </div>
        @endif
    </div>
</div>
<a href="/setting-login-background" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>
