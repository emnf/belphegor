@extends('layouts.master')

@section('judulkiri')
PENGATURAN LOGIN BACKGROUND
@endsection

@section('judulkanan')
<li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
<li class="breadcrumb-item active">Datagrid</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-success">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="{{ route('setting_login_background.create') }}" class="btn btn-sm btn-primary">
                        <i class="fas fa-plus"></i> Tambah
                    </a>
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="" class="text-center bg-success">NO</th>
                                <th style="" class="text-center bg-success">Image</th>
                                <th style="" class="text-center bg-success">Status</th>
                                <th style="" class="text-center bg-success">Create At</th>
                                <th style="width:180px" class="text-center bg-success">Aksi</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-thumbnail">
                                </th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-status">
                                        <option value="">All</option>
                                        @foreach ($status as $status)
                                        <option value="{{ $status->s_id_status }}">{{ $status->s_nama_status }}</option>
                                        @endforeach
                                    </select>
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-created_at"
                                        readonly>
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="5"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'setting-login-background/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "thumbnail"
            },
            {
                sortable: true,
                name: "status"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"></i> Edit'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"></i> Hapus'
            }
        ]

    });

    $("#filter-thumbnail, #filter-status, #filter-status, #filter-created_at").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            thumbnail: $("#filter-thumbnail").val(),
            status: $("#filter-status").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
        title: 'Yakin anda akan menghapus?',
        text: "Data akan dihapus secara permanen.",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya',
        reverseButtons: true
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    url: 'setting-login-background/delete/{' + a + '}',
                    method: "GET",
                    data: {
                        id: a
                    },
                    error: function(request, error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Gagal',
                            text: 'Data Gagal Dihapus.',
                            showConfirmButton: false,
                            timer: 2500,
                            timerProgressBar: true,
                        })
                    },
                    success: function() {
                        Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Data anda berhasil dihapus secara permanen.',
                            showConfirmButton: false,
                            timer: 2500,
                            timerProgressBar: true,
                        })
                        datatables.reload();
                    }
                });

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            )
            {
                swalWithBootstrapButtons.fire(
                'Dibatalkan',
                'Data tidak dihapus secara permanen.',
                'error'
                )
            }
        })
    }
</script>
@endpush
