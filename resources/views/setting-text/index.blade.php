@extends('layouts.master')
@section('judulkiri')
    PENGATURAN ROLES
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">
                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#createModal">
                                Create New Text
                            </button>
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">

                        <!-- /.card-header -->
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                                <thead>
                                    <tr>
                                        <th style="" class="text-center bg-success">No</th>
                                        <th style="" class="text-center bg-success">Text</th>
                                        <th style="" class="text-center bg-success">Status</th>
                                        <th style="" class="text-center bg-success">Aksi</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th><input type="text" class="form-control form-control-sm" id="filter-name"></th>
                                        <th>
                                            <select id="filter-s_aktif" class="form-control form-control-sm">
                                                <option value="1">Aktif</option>
                                                <option value="0">Nonaktif</option>
                                            </select>
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="4"> Tidak ada data.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer clearfix pagination-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5 class="modal-title" id="createModalLabel">Create Text</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('setting-text.store') }}" method="post">
                    @csrf
                    {{-- @include('permission.roles.partials.form-control', ['submit' => 'Create']) --}}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="hidden" name="id" id="id" class="form-control">
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Status</label>
                        <select name="s_aktif" id="s_aktif" class="form-control">
                            <option value="1">Aktif</option>
                            <option value="0">Nonaktif</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Name</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="nameHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span> Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var datatables = datagrid({
        url: 'setting-text/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: false,
                name: "name"
            },
            {
                sortable: false,
                name: "guard_name"
            },
            {
                sortable: false,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"></i> Edit'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"></i> Hapus'
            }
        ]

    });

    $(" #filter-s_aktif").change(function() {
        search();
    });
    $("#filter-name").keyup(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            name: $("#filter-name").val(),
            s_aktif: $("#filter-s_aktif").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'setting-text/detail/'+ a ,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data.id);
            $("#nameHapus").val(data.s_text);
        });
        $("#modal-delete").modal('show');
    }

    function showEditDialog(a) {
        $.ajax({
            url: 'setting-text/detail/'+ a ,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#id").val(data.id);
            $("#name").val(data.s_text);
            if (data.s_aktif == true) {
                $("#s_aktif").val(1);
            } else {
                $("#s_aktif").val(0);
            }
        });
        $("#createModal").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'setting-text/delete/' + id ,
            method: "DELETE",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            datatables.reload();
        });
    });
</script>
@endpush
