@extends('layouts.master')
@section('judulkiri')
    PENGATURAN ASSIGN ROLE TO USER
@endsection

@section('content')
@if(session('success'))
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="card">
            <div class="card-header">Sync Roles</div>
            <div class="card-body">
                <form action="{{ route('assign.user.edit', $user) }}" method="post">
                    @method("PUT")
                    @csrf
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ $user->email }}"></input>
                    </div>
                    <div class="form-group">
                        <label for="roles">Pick Roles</label>
                        <select name="roles[]" id="roles" class="form-control duallistbox" multiple style="height: 400px;">
                            @foreach($roles as $role)
                            <option {{ $user->roles()->find($role->id) ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Assign</button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
    $(function () {
        $('.duallistbox').bootstrapDualListbox();
    });
</script>
@endpush