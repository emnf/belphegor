<div class="form-group">
    <label for="role">Role Name</label>
    <select name="role" id="role" class="form-control">
        <option disabled selected value="">Choose One</option>
        @foreach($roles as $role)
        <option value="{{ $role->id }}">{{ $role->name }}</option>
        @endforeach
    </select>
    @error('role')
    <div class="text-danger mt2 d-block">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="permissions">Permissions Name</label>
    <select name="permissions[]" id="permissions" class="form-control duallistbox" multiple style="height: 400px;">
        @foreach($permissions as $permission)
        <option value="{{ $permission->id }}">{{ $permission->name }}</option>
        @endforeach
    </select>
    @error('permissions')
    <div class="text-danger mt2 d-block">{{ $message }}</div>
    @enderror
</div>
<button type="submit" class="btn btn-primary">{{ $submit }}</button>

@push('scripts')
<script type="text/javascript">
    $(function () {
        $('.duallistbox').bootstrapDualListbox();
    });
</script>
@endpush