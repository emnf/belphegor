@extends('layouts.master')
@section('judulkiri')
    PENGATURAN ASSIGN PERMISSION
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <div class="card">
            <div class="card-header">Assign Permission</div>
            <div class="card-body">
                <form action="{{ route('assign.edit', $role) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="role">Role Name</label>
                        <select name="role" id="role" class="form-control">
                            <option disabled selected value="">Choose One</option>
                            @foreach($roles as $item)
                            <option {{ $role->id == $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('role')
                        <div class="text-danger mt2 d-block">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="permissions">Permissions Name</label>
                        <select name="permissions[]" id="permissions" class="form-control duallistbox" multiple style="height: 400px;">
                            @foreach($permissions as $permission)
                            <option {{ $role->permissions()->find($permission->id) ? "selected" : "" }} value="{{ $permission->id }}">{{ $permission->name }}</option>
                            @endforeach
                        </select>
                        @error('permissions')
                        <div class="text-danger mt2 d-block">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Sync</button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
    $(function () {
        $('.duallistbox').bootstrapDualListbox();
    });
</script>
@endpush