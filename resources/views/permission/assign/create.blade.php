@extends('layouts.master')
@section('judulkiri')
    PENGATURAN ASSIGN PERMISSION
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="card">
            <div class="card-header">Assign Permission</div>
            <div class="card-body">
                <form action="{{ route('assign.create') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="role">Role Name</label>
                        <select name="role" id="role" class="form-control">
                            <option disabled selected value="">Choose One</option>
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                        @error('role')
                        <div class="text-danger mt2 d-block">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Permissions Name</label>
                        <select name="permissions[]" id="permissions" class="form-control duallistbox" multiple="multiple"  style="height: 400px;">
                            @foreach($permissions as $permission)
                            <option value="{{ $permission->id }}">{{ $permission->name }}</option>
                            @endforeach
                        </select>
                        @error('permissions')
                        <div class="text-danger mt2 d-block">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Assign</button>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-header">Role & Permissions Table</div>
            <div class="card-body">
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Guard Name</th>
                        <th>The Permissions</th>
                        <th>Act</th>
                    </tr>
                    @foreach($roles as $index => $role)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->guard_name }}</td>
                        <td>{{ implode(", ", $role->getPermissionNames()->toArray()) }}</td>
                        <td><a href="{{ route('assign.edit', $role) }}">Sync</a></td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.duallistbox').bootstrapDualListbox();
    });
</script>
@endsection