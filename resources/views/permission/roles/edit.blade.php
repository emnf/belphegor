@extends('layouts.master')
@section('judulkiri')
    PENGATURAN ROLES
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card mb-4">
            <div class="card-header">Edit Role</div>
            <div class="card-body">
                <form action="{{ route('roles.edit', $role) }}" method="post">
                    @csrf
                    @method('PUT')
                    @include('permission.roles.partials.form-control')
                    <a href="{{ route('roles.index') }}" class="btn btn-danger float-right">Kembali</a>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection