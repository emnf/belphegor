<aside class="main-sidebar sidebar-light-primary elevation-3">
    <a href="{{ route('home') }}" class="brand-link">
        <img src="{{ $pemda->s_logo }}" class="brand-image">
        <span class="brand-text"><strong>My</strong>Dashboard</span>
    </a>
    <div class="sidebar text-sm">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image align-self-center">
                <img src="{{ asset('dist/img/avatar5.png') }}" class="img-rounded" alt="User Image">
            </div>
            <div class="align-self-center ml-3">
                <a href="#" class=""><b>{{ strtoupper(Auth::user()->name) }}</b> <br>
                    <span class="text-sm">
                        <?php $roles = App\Models\Setting\Roles::where('id',Auth::user()->s_id_hakakses)->first('name'); ?>
                        <i class="fas fa-circle text-green"></i> {{$roles->name}}
                    </span>
                </a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link {{ request()->is('home') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-th-large"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('dashboard') }}" class="nav-link {{ request()->is('dashboard') ? 'active' : '' }}">
                        <i class="fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                {{-- <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-cogs"></i>
                            <p>
                                Dashboard
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="{{ url('dashboard/penerimaan-harian') }}"
                                        class="nav-link {{ request()->is('dashboard/penerimaan-harian') ? 'active' : '' }}" target="_blank">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Penerimaan Harian</p>
                                    </a>
                                </li>

                        </ul> --}}
                    </li>



                @can('SettingPemda')
                    <li class="nav-item has-treeview {{ request()->is('setting*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('setting*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-cogs"></i>
                            <p>
                                Aktifitas Admin
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('SettingPemda')
                                <li class="nav-item">
                                    <a href="{{ url('setting-pemda') }}"
                                        class="nav-link {{ request()->is('setting-pemda') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Pemerintah Daerah</p>
                                    </a>
                                </li>
                            @endcan
                                <li class="nav-item">
                                    <a href="{{ url('setting-text') }}"
                                        class="nav-link {{ request()->is('setting-text') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Setting Text</p>
                                    </a>
                                </li>

                            @can('SettingLoginBackground')
                                <li class="nav-item">
                                    <a href="{{ url('setting-login-background') }}"
                                        class="nav-link {{ request()->is('setting-login-background*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Login Background</p>
                                    </a>
                                </li>
                            @endcan
                            @can('SettingUser')
                                <li class="nav-item">
                                    <a href="{{ url('setting-user') }}"
                                        class="nav-link {{ request()->is('setting-user*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Users</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('SettingRole')
                    <li class="nav-item has-treeview {{ request()->is('role*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('role*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-tools"></i>
                            <p>
                                Role & Permission
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('SettingRole')
                                <li class="nav-item">
                                    <a href="{{ url('role-and-permission/roles') }}"
                                        class="nav-link {{ request()->is('role-and-permission/roles*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Roles</p>
                                    </a>
                                </li>
                            @endcan
                            @can('SettingPermission')
                                <li class="nav-item">
                                    <a href="{{ url('role-and-permission/permissions') }}"
                                        class="nav-link {{ request()->is('role-and-permission/permissions*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Permissions</p>
                                    </a>
                                </li>
                            @endcan
                            @can('SettingAssignPermissionToRole')
                                <li class="nav-item">
                                    <a href="{{ url('role-and-permission/assignable') }}"
                                        class="nav-link {{ request()->is('role-and-permission/assignable*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Assign Permission</p>
                                    </a>
                                </li>
                            @endcan
                            @can('SettingAssignRoleToUser')
                                <li class="nav-item">
                                    <a href="{{ url('role-and-permission/assign/user') }}"
                                        class="nav-link {{ request()->is('role-and-permission/assign/user*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Role User</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('SettingPass')
                    <li class="nav-item">
                        <a href="{{ url('ubah-pass/index') }}"
                            class="nav-link {{ request()->is('ubah-pass*') ? 'active' : '' }}">
                            <i class="fa fa-key nav-icon"></i>
                            <p>Ubah Password</p>
                        </a>
                    </li>
                @endcan
            </ul>
        </nav>

    </div>
</aside>
