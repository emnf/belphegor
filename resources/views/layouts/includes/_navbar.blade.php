<nav class="main-header navbar navbar-expand navbar-primary navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        {{-- @guest
        <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
        @endif
        @else
        <li class="nav-item">
            <a href="#" class="nav-link" >
                <i class="fas fa-user"></i>
                {{ Auth::user()->name }}
            </a>
        </li>
        @endguest --}}
        {{-- <li class="nav-item">
            <a href="#" class="btn btn-md btn-info text-red" data-slide="true" title="Keluar" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="color: tomato;">
                <i class="fas fa-sign-out-alt"></i>
                Keluar
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li> --}}
        <li class="nav-item dropdown">
            {{-- <a class="nav-link" data-toggle="dropdown" href="#">

            </a> --}}
            <div class="btn btn-sm w-auto btn-clean d-flex align-items-center px-2" data-toggle="dropdown">
                <span class="text-dark-60 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ Auth::user()->name }}</span>
                <img src="{{ asset('dist/img/avatar5.png') }}" class="img-rounded elevation-3" style="height: 30px;" alt="User Image">
            </div>
            <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right">
                <div class="p-3 elevation-3">
                    <div class="d-flex justify-content-start">
                        <img src="{{ asset('dist/img/avatar5.png') }}" class="img-rounded elevation-3" style="height: 90px;"
                        alt="User Image">
                        <div class="px-3">
                            <h5>{{ Auth::user()->name }}</h5>
                            <h6 class="text-muted text-sm">{{ Auth::user()->getRoleNames()->first() }}</h6>
                            <a href="{{ url('profile') }}" class="btn btn-sm btn-outline-primary"><i class="fas fa-user"></i> Profile</a>
                            <a href="#" class="btn btn-sm btn-outline-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> Keluar</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </li>
    </ul>

</nav>
