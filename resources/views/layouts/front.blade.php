<!DOCTYPE html>
<html lang="en">
<?php
use App\Models\Setting\Pemda;
use App\Models\Setting\LoginBackground;
$pemda = Pemda::first();
$background = LoginBackground::where('s_id_status', 1)->first();
// $pemda = Illuminate\Support\Facades\DB::table('s_pemda')->first();
// $background = Illuminate\Support\Facades\DB::table('s_background')
//     ->where('s_id_status', 1)
//     ->first();
// ?>

<head>

    @include('layouts.meta')

    {{-- <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet"> --}}

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

    <link rel="stylesheet" href="{{ asset('dist/css/animate.css') }}">

    <!-- Custom style -->
    <link rel="stylesheet" href="{{ asset('dist/css/custom.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    {!! NoCaptcha::renderJs() !!}
    <style>
        .bd-example-modal-lg .modal-dialog {
            display: table;
            position: relative;
            margin: 0 auto;
            top: calc(50% - 20px);
        }

        .bd-example-modal-lg .modal-dialog .modal-content {
            background-color: transparent;
            border: none;
            box-shadow: none;
        }
    </style>

</head>

<body class="hold-transition">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 p-5 p-sm-4 p-xl-5 p-lg-5 login-section-wrapper animated slideInLeft">

                @yield('content')

            </div>
            <div class="col-sm-8 px-0 d-none d-sm-block">
                <div class="login-img elevation-3"
                    style="background: linear-gradient(to right, rgba(10, 255, 18, 0.654), rgba(24, 80, 222, 0.61)), url({{ $background->s_thumbnail }}) no-repeat center center;background-size: cover;">
                    <div class="d-flex flex-column justify-content-end align-items-end h-auto">
                        <div class="text-center mt-md-5 mt-sm-4">
                            <div class="d-flex align-content-center ">
                                <div class="d-flex flex-column align-self-center p-3">

                                    <h3 class="font-weight-bolder text-white text-right animated fadeIn">Pemerintah
                                        {{ $pemda->s_namakabkot }}</h3>
                                    <h4 class="text-white font-weight-normal animated fadeIn text-right">
                                        {{ $pemda->s_namainstansi }}</h4>
                                </div>
                                <div class="p-3 align-self-center border-left">
                                    <img src="{{ $pemda->s_logo }}" alt="LOGO" class="animated rubberBand"
                                        style="width: 70px;">
                                </div>

                            </div>

                        </div>
                        <div class="mt-5 animated slideInRight">
                            <h4 class="text-white font-weight-normal text-right px-sm-4 ">
                                Aplikasi Dashboard Monitoring PAD
                            </h4>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="loading" data-backdrop="static" data-keyboard="false"
        tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="col-12 text-center">
                    <span class="spinner-grow text-muted"></span>
                    <span class="spinner-grow text-primary"></span>
                    <span class="spinner-grow text-success"></span>
                    <span class="spinner-grow text-info"></span>
                    <span class="spinner-grow text-warning"></span>
                    <span class="spinner-grow text-danger"></span>
                    <span class="spinner-grow text-dark"></span><br>
                    <small class="text-white">Mohon Menunggu...</small>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(() => {

            @if (session('active_account'))
                Swal.fire({
                    icon: 'success',
                    title: 'Verifikasi Email Berhasil.',
                    text: '{{ session('active_account') }}'
                })
            @endif

            @if (session('status'))
                Swal.fire({
                    icon: 'success',
                    title: 'Reset Password',
                    text: '{{ session('status') }}'
                })
            @endif

            @if (session('resent'))
                Swal.fire({
                    icon: 'success',
                    title: 'Verifikasi Email Dikirim.',
                    text: 'Link Verifikasi Email Sudah Kami Kirim Kembali, Silahkan Cek Email anda.'
                })
            @endif

            @if (session('registered'))
                Swal.fire({
                    icon: 'success',
                    title: 'Registrasi Akun',
                    text: '{{ session('registered') }}'
                })
            @endif

        });

        $('#submitbutton').click(function() {
            var myForm = document.getElementById('formsubmit');
            myForm.onsubmit = function() {
                var allInputs = myForm.getElementsByTagName('input');
                var input, i;

                for (i = 0; input = allInputs[i]; i++) {
                    if (input.getAttribute('name') && !input.value) {

                    } else {
                        jQuery('#loading').modal('show');
                    }
                }
            };
        });
    </script>
</body>

</html>
