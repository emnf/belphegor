<!DOCTYPE html>
<html lang="en">
@php
    $pemda = Illuminate\Support\Facades\DB::table('s_pemda')->first();
    $footerYear = date('Y') == '2022' ? date('Y') : '2021-' . date('Y');
@endphp

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MyDashboard ||{{ $pemda->s_namakabkot }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ $pemda->s_logo }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ $pemda->s_logo }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ $pemda->s_logo }}">
    <link rel="manifest" href="{{ asset('depan/assets/images/favicons/site.webmanifest') }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    {{-- <link rel="stylesheet" href="{{ asset('plugins/ionicons-2.0.1/css/ionicons.min.css') }}"> --}}
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
        href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- Bbootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-sans.css') }}">
    <link rel="stylesheet" href="{{ asset('datagrid/datagrid.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <!--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">-->

    <!-- CSS -->
    {{-- <link href="{{ asset('plugins/jquery-smartwizard/dist/css/smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" /> --}}

    <style>
        .bd-example-modal-lg .modal-dialog {
            display: table;
            position: relative;
            margin: 0 auto;
            top: calc(50% - 20px);
        }

        .bd-example-modal-lg .modal-dialog .modal-content {
            background-color: transparent;
            border: none;
            box-shadow: none;
        }
    </style>
    @stack('styles')

    <script src="{{ asset('js/jquery-2.2.4.js') }}"></script>
</head>

<body class="hold-transition sidebar-mini accent-success layout-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        @include('layouts.includes._navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('layouts.includes._sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h4>@yield('judulkiri')</h4>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                @yield('judulkanan')
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.0.5
            </div>
            <strong>Copyright &copy; {{ $footerYear }} <a href="https://mitraprimautama.id">Mitra Prima
                    Utama</a>.</strong> All rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <div class="modal fade bd-example-modal-lg" id="loading" data-backdrop="static" data-keyboard="false"
        tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="col-12 text-center">
                    <span class="spinner-grow text-muted"></span>
                    <span class="spinner-grow text-primary"></span>
                    <span class="spinner-grow text-success"></span>
                    <span class="spinner-grow text-info"></span>
                    <span class="spinner-grow text-warning"></span>
                    <span class="spinner-grow text-danger"></span>
                    <span class="spinner-grow text-dark"></span><br>
                    <small class="text-white">Mohon Menunggu...</small>
                </div>
            </div>
        </div>
    </div>
    <!-- ./wrapper -->
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- jQuery -->
    <!-- <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Bootstrap datepicker -->
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>

    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Media JS -->
    <script src="{{ asset('/js/jquery.media.js') }}" type="text/javascript"></script>

    <!-- Toastr -->
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="{{ asset('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- page script -->
    {{-- <script src="{{asset('/datagrid/datagrid.js')}}"></script> --}}

    <!-- JavaScript -->
    <script src="{{ asset('/js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('/js/form-masking.js') }}"></script>
    <script src="{{ asset('/js/drop.js') }}"></script>
    </script>
    @stack('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker-date').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy"
            });

            $(".datepicker-year").datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                autoclose: true,
            });

            @if (session('success'))
                // toastr.success('{{ session('success') }}')
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: '{{ session('success') }}',
                    // showConfirmButton: false,
                    // timer: 2500,
                    // timerProgressBar: true,
                })
            @endif

            @if (session('error'))
                // toastr.error('{{ session('error') }}')
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: '{{ session('error') }}',
                    // showConfirmButton: false,
                    // timer: 2500,
                    // timerProgressBar: true,
                })
            @endif

            @if (session('info'))
                // toastr.info('{{ session('info') }}')
                Swal.fire({
                    icon: 'info',
                    title: 'Perhatian',
                    text: '{{ session('info') }}',
                    // showConfirmButton: false,
                    // timer: 2500,
                    // timerProgressBar: true,
                })
            @endif
        });

        $(".rupiah").on("keyup", function() {
            $(this).val(formatRupiah(this.value, null));
        });

        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }

        function numbersonly(myfield, e, dec) {
            var key;
            var keychar;
            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;
            keychar = String.fromCharCode(key);
            // control keys
            if ((key == null) || (key == 0) || (key == 8) ||
                (key == 9) || (key == 13) || (key == 27))
                return true;
            // numbers
            else if ((("0123456789").indexOf(keychar) > -1))
                return true;
            // decimal point jump
            else if (dec && (keychar == ".")) {
                myfield.form.elements[dec].focus();
                return false;
            } else
                return false;
        }

        function formatCurrency(num) {
            if (num != "" || num != "undefined") {
                //                    num = num.replace(/\$|\,00/g, '').replace(/\$|\./g, '');
                num = num.replace(/\$|\,00/g, '').replace(/\$|\./g, '');
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num * 100 + 0.50000000001);
                cents = num % 100;
                num = Math.floor(num / 100).toString();
                if (cents < 10)
                    cents = "0" + cents;
                for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                    num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
                return (((sign) ? '' : '-') + num);
            }
        }

        function unformatCurrency(num) {
            if (num != "" || num != "undefined")
                num = num.replace(/\$|\,00/g, '').replace(/\$|\./g, '');
            if (num == 0)
                num = '';
            return num;
        }

        function currencyFormatDE(num) {
            return num
                .toFixed(0) // always two decimal digits
                .replace(".", ",") // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") // use . as a separator
        }


        $('#submitbutton').click(function() {
            var myForm = document.getElementById('formsubmit');
            myForm.onsubmit = function() {
                var allInputs = myForm.getElementsByTagName('input');
                var input, i;

                for (i = 0; input = allInputs[i]; i++) {
                    if (input.getAttribute('name') && !input.value) {

                    } else {
                        jQuery('#loading').modal('show');
                    }
                }
            };
        });
    </script>

    @stack('scriptsbawah')

</body>

</html>
