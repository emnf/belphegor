<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{ strtoupper($pemda->s_namainstansi.' '.strtolower($pemda->s_namakabkot)) }}</title>

<!-- Favicons -->
<link href="{{ $pemda->s_logo }}" rel="icon">
<link href="{{ $pemda->s_logo }}" rel="apple-touch-icon">
<link rel="manifest" href="{{ asset('dist/landing/images/favicons/manifest.json') }}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
