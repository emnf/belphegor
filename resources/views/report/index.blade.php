@extends('layouts.master')

@section('judulkiri')
    LAPORAN
@endsection

@section('judulkanan')
    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Index</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-success">
                <div class="card-body">
                    @if (in_array(Auth::user()->s_id_hakakses, [1, 2]))
                        <a href="{{ url('report/rekap-ketetapan-by-kec') }}">
                            <div class="callout callout-warning p-2 elevation-2" style="color: rgb(50, 49, 49);">
                                <i class="fas fa-print"></i> Laporan Rekapitulasi Ketetapan dan Realisasi Per
                                {{ $data_pemda->s_namakecamatan }}
                            </div>
                        </a>
                    @endif
                    @if (in_array(Auth::user()->s_id_hakakses, [1, 2, 6]))
                        <a href="{{ url('report/rekap-ketetapan-by-kel') }}">
                            <div class="callout callout-warning p-2 elevation-2" style="color: rgb(50, 49, 49);">
                                <i class="fas fa-print"></i> Laporan Rekapitulasi Ketetapan dan Realisasi Per
                                {{ $data_pemda->s_namakelurahan }}
                            </div>
                        </a>
                    @endif
                    @if (in_array(Auth::user()->s_id_hakakses, [1, 2, 4]))
                        <a href="{{ url('report/rekap-ketetapan-by-blok') }}">
                            <div class="callout callout-warning p-2 elevation-2" style="color: rgb(50, 49, 49);">
                                <i class="fas fa-print"></i> Laporan Rekapitulasi Ketetapan dan Realisasi Per Blok
                            </div>
                        </a>
                    @endif
                    @if (in_array(Auth::user()->s_id_hakakses, [1, 5]))
                        <a href="{{ url('report/rekap-realisasi-pernop') }}">
                            <div class="callout callout-success p-2 elevation-2" style="color: rgb(50, 49, 49);">
                                <i class="fas fa-print"></i> Laporan Realisasi Per NOP (Per tanggal)
                            </div>
                        </a>
                    @endif
                    {{-- @if (in_array(Auth::user()->s_id_hakakses, [1, 2, 4, 5]))
                        <a href="{{ url('report/rekap-ketetapan-by-dukuh') }}">
                            <div class="callout callout-warning p-2 elevation-2" style="color: rgb(50, 49, 49);">
                                <i class="fas fa-print"></i> Laporan Rekapitulasi Ketetapan Per Dukuh
                            </div>
                        </a>
                    @endif --}}
                    {{-- @if (in_array(Auth::user()->s_id_hakakses, [1, 2, 4]))
                        <a href="{{ url('report/rekap-realisasi') }}">
                            <div class="callout callout-info p-2 elevation-2" style="color: rgb(50, 49, 49);">
                                <i class="fas fa-print"></i> Laporan Rekapitulasi Realisasi Per Blok
                            </div>
                        </a>
                    @endif --}}
                    {{-- @if (in_array(Auth::user()->s_id_hakakses, [1, 2, 4]))
                        <a href="{{ url('report/rekap-tunggakan') }}">
                            <div class="callout callout-danger p-2 elevation-2" style="color: rgb(50, 49, 49);">
                                <i class="fas fa-print"></i> Laporan Rekapitulasi Tunggakan Per Blok
                            </div>
                        </a>
                    @endif --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript"></script>
@endpush
