@extends('layouts.master')

@section('judulkiri')
    LAPORAN REKAP TUNGGAKAN PER BLOK
@endsection

@section('judulkanan')
    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ url('report') }}">Laporan</a></li>
    <li class="breadcrumb-item active">Index</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-success">
                <div class="card-header">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <dl class="row">
                                <dd class="col-sm-3">{{ strtoupper($data_pemda->s_namakecamatan) }} </dd>
                                <dt class="col-sm-9">
                                    @if (Auth::user()->s_id_hakakses == 4)
                                        {{ $kecamatan->KD_KECAMATAN . ' - ' . $kecamatan->nm_kecamatan }}
                                        <input type="hidden" id="filter-kecamatan" value="{{ $kecamatan->KD_KECAMATAN }}">
                                    @else
                                        <select id="filter-kecamatan" class="form-control form-control-sm">
                                            <option value="">-- Silahkan Pilih --</option>
                                        </select>
                                    @endif
                                </dt>
                                <dd class="col-sm-3">{{ strtoupper($data_pemda->s_namakelurahan) }} </dd>
                                <dt class="col-sm-9">
                                    @if (Auth::user()->s_id_hakakses == 4)
                                        {{ $kelurahan->KD_KELURAHAN . ' - ' . $kelurahan->nm_kelurahan }}
                                        <input type="hidden" id="filter-kelurahan" value="{{ $kelurahan->KD_KELURAHAN }}">
                                    @else
                                        <select id="filter-kelurahan" class="form-control form-control-sm">
                                            <option value="">-- Silahkan Pilih --</option>
                                        </select>
                                    @endif
                                </dt>
                                <dd class="col-sm-3">TAHUN </dd>
                                <dt class="col-sm-3">
                                    <select id="filter-tahun" class="form-control form-control-sm">
                                        @for ($i = 0; $i <= 6; $i++)
                                            @php
                                                $tahun = date('Y', strtotime("-$i years"));
                                            @endphp
                                            <option value="{{ $tahun }}">{{ $tahun }}</option>
                                        @endfor
                                    </select>
                                </dt>
                                <dt class="col-sm-6">
                                    <button type="button" class="btn btn-danger btn-sm" onclick="exportData('PDF')"><i
                                            class="fas fa-file-pdf"></i>
                                        Export Data (PDF)</button>
                                    <button type="button" class="btn btn-success btn-sm" onclick="exportData('XLS')"><i
                                            class="fas fa-file-excel"></i>
                                        Export Data (XLS)</button>
                                </dt>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-vertical-center"
                            id="datagrid-table">
                            <thead>
                                <tr class="bg-green">
                                    <th class="text-center">NO</th>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Kelurahan</th>
                                    <th class="text-center">Blok</th>
                                    <th class="text-center">Jumlah SPPT</th>
                                    <th class="text-center">Jumlah Ketetapan (Rp)</th>
                                    <th class="text-center">Jumlah Data Realisasi</th>
                                    <th class="text-center">Jumlah Realisasi (Rp)</th>
                                    <th class="text-center">Jumlah Data Tunggakan</th>
                                    <th class="text-center">Jumlah Tunggakan (Rp)</th>
                                    <th class="text-center">Persentase (%)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="10">Tidak ada data</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix pagination-footer">
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('/datagrid/datagrid.js') }}"></script>
    <script type="text/javascript">
        setTimeout(() => {
            comboKecamatan();
        }, 1000);

        var datatables = datagrid({
            url: '{{ url('report/datagrid-rekap-tunggakan') }}',
            table: "#datagrid-table",
            columns: [{
                    class: "text-center"
                },
                {
                    class: ""
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-right"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-right"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-right"
                },
                {
                    class: "text-center"
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: true,
                    name: ""
                },
                {
                    sortable: true,
                    name: ""
                },
                {
                    sortable: true,
                    name: ""
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
            ],

        });

        $("#filter-kecamatan, #filter-kelurahan, #filter-tahun").change(function() {
            search();
        });

        function search() {
            datatables.setFilters({
                kecamatan: $("#filter-kecamatan").val(),
                kelurahan: $("#filter-kelurahan").val(),
                tahun: $("#filter-tahun").val()
            });
            datatables.reload();
        }
        search();

        const kecamatan = JSON.parse('{!! $kecamatan !!}');

        function comboKecamatan() {
            var option = '<option value="">-- Silahkan Pilih --</option>';
            kecamatan.forEach(el => {
                option +=
                    `<option value="${ el.KD_KECAMATAN }">${ el.KD_KECAMATAN + ' || ' + el.NM_KECAMATAN }</option>`;
            });

            $('#filter-kecamatan').html(option);
        }

        $("#filter-kecamatan").change(function() {
            comboKelurahan($('#filter-kecamatan').val());
        });

        function comboKelurahan(a) {
            $.ajax({
                url: '{{ url('report/comboKelurahan') }}',
                type: 'GET',
                data: {
                    id: a
                }
            }).then(function(data) {
                let option = '<option value="">-- Silahkan Pilih --</option>';
                data.forEach(el => {
                    option +=
                        `<option value="${ el.KD_KELURAHAN }">${ el.KD_KELURAHAN + ' || ' + el.NM_KELURAHAN }</option>`;
                });
                $('#filter-kelurahan').html(option);
            });
        }

        function exportData(a) {

            var kec = $('#filter-kecamatan').val();
            var kel = $('#filter-kelurahan').val();
            var tahun = $('#filter-tahun').val();

            window.open('cetakrekaptunggakan?format=' + a + '&kec=' + kec + '&kel=' + kel + '&tahun=' + tahun);
        }
    </script>
@endpush
