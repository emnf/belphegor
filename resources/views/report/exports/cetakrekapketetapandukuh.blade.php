@php
error_reporting(0);
ini_set('pcre.backtrack_limit', 10000000);
@endphp
<style>
    h3 {
        text-align: center;
    }

    .background-gray-50 {
        background: rgb(226, 225, 225);
    }

    .background-black {
        background: black;
        color: white;
    }

    .text-right {
        text-align: right;
    }

    .text-center {
        text-align: center;
    }

    .border-left {
        border-left: 1px solid #000;
    }

    .border-right {
        border-right: 1px solid #000;
    }

    .border-top {
        border-top: 1px solid #000;
    }

    .border-bottom {
        border-bottom: 1px solid #000;
    }
</style>
<div>
    @if ($request->format == 'PDF')
        @include('components.kop')
    @endif



    <table width="100%" cellspacing="2" cellpadding="2" style="border-collapse: collapse;font-size:10pt;margin:10px;">
        <tr>
            <th colspan="14">
                <strong>DATA REKAPITULASI KETETAPAN PER DUKUH</strong>
            </th>
        </tr>
        <tr>
            <th colspan="14">
                <strong>{{ strtoupper($pemda->s_namakecamatan . ' ' . $kecamatan->nm_kecamatan) }}</strong>
                <strong>{{ strtoupper($pemda->s_namakelurahan . ' ' . $kelurahan) }}</strong>
            </th>
        </tr>
        <tr>
            <th colspan="14">
                <strong>TAHUN {{ $request->tahun }}</strong>
            </th>
        </tr>
    </table>
    <table width="100%" cellspacing="2" cellpadding="5" style="border-collapse: collapse;font-size:10pt;">
        <thead>
            <tr class="background-gray-50">
                <th class="border-top border-right border-bottom border-left" rowspan="2" width="5%">NO</th>
                <th class="border-top border-right border-bottom border-left" colspan="3" width="15%">DUKUH
                </th>
                <th class="border-top border-right border-bottom border-left" colspan="2">KETETAPAN</th>
                <th class="border-top border-right border-bottom border-left" colspan="4">PEMBAYARAN</th>
                <th class="border-top border-right border-bottom border-left" colspan="4">SISA</th>
            </tr>
            <tr class="background-gray-50">
                <th class="border-top border-right border-bottom border-left">KODE</th>
                <th class="border-top border-right border-bottom border-left">DUKUH</th>
                <th class="border-top border-right border-bottom border-left">BLOK</th>
                <th class="border-top border-right border-bottom border-left">SPPT</th>
                <th class="border-top border-right border-bottom border-left">JML (Rp)</th>
                <th class="border-top border-right border-bottom border-left">SPPT</th>
                <th class="border-top border-right border-bottom border-left">%</th>
                <th class="border-top border-right border-bottom border-left">JML (Rp)</th>
                <th class="border-top border-right border-bottom border-left">%</th>
                <th class="border-top border-right border-bottom border-left">SPPT</th>
                <th class="border-top border-right border-bottom border-left">%</th>
                <th class="border-top border-right border-bottom border-left">JML (Rp)</th>
                <th class="border-top border-right border-bottom border-left">%</th>
            </tr>
        </thead>
        <tbody>
            @php
                $counter = 1;
                $totalSppt = 0;
                $totalSpptLunas = 0;
                $totalSpptSisa = 0;
                $totalKetetapan = 0;
                $totalRealisasi = 0;
                $totalSisa = 0;
                $totalPersenSpptLunas = 0;
                $totalPersenSpptSisa = 0;
                $totalPersenRealiasasi = 0;
                $totalPersenSisa = 0;
            @endphp
            @foreach ($data as $key => $row)
                @php
                    $jmlhSppt = $row->countsppt;
                    $jmlhSpptLunas = $row->countreal;
                    $jmlhSpptSisa = $jmlhSppt - $jmlhSpptLunas;

                    $persenSpptLunas = $jmlhSpptLunas > 0 ? ($jmlhSpptLunas / $jmlhSppt) * 100 : 0;
                    $persenSpptSisa = $jmlhSpptSisa > 0 ? ($jmlhSpptSisa / $jmlhSppt) * 100 : 0;

                    $jmlhKetetapan = $row->ketetapan;
                    $jmlhRealisasi = $row->realisasi - $row->denda;
                    $jmlhSisa = $jmlhKetetapan - $jmlhRealisasi;

                    $persenRealiasasi = $jmlhRealisasi > 0 ? ($jmlhRealisasi / $jmlhKetetapan) * 100 : 0;
                    $persenSisa = $jmlhSisa > 0 ? ($jmlhSisa / $jmlhKetetapan) * 100 : 0;
                @endphp
                <tr>
                    <td class="border-top border-right border-bottom border-left text-center">{{ $counter++ }}</td>
                    <td class="border-top border-right border-bottom border-left text-center">{{ $row->kd_dukuh }}
                    </td>
                    <td class="border-top border-right border-bottom border-left text-left">{{ $row->nm_dukuh }}
                    </td>
                    <td class="border-top border-right border-bottom border-left text-left">{{ $row->KD_BLOK }}
                    </td>
                    <td class="border-top border-right border-bottom border-left text-center">
                        {{ $request->format == 'PDF' ? number_format($jmlhSppt, 0, ',', '.') : $jmlhSppt }}</td>
                    <td class="border-top border-right border-bottom border-left text-right">
                        {{ $request->format == 'PDF' ? number_format($jmlhKetetapan, 0, ',', '.') : $jmlhKetetapan }}
                    </td>
                    <td class="border-top border-right border-bottom border-left text-center">
                        {{ $request->format == 'PDF' ? number_format($jmlhSpptLunas, 0, ',', '.') : $jmlhSpptLunas }}
                    </td>
                    <td class="border-top border-right border-bottom border-left text-center">
                        {{ number_format($persenSpptLunas, 2, ',', '.') }}</td>
                    <td class="border-top border-right border-bottom border-left text-right">
                        {{ $request->format == 'PDF' ? number_format($jmlhRealisasi, 0, ',', '.') : $jmlhRealisasi }}
                    </td>
                    <td class="border-top border-right border-bottom border-left text-center">
                        {{ number_format($persenRealiasasi, 2, ',', '.') }}</td>
                    <td class="border-top border-right border-bottom border-left text-center">
                        {{ $request->format == 'PDF' ? number_format($jmlhSpptSisa, 0, ',', '.') : $jmlhSpptSisa }}
                    </td>
                    <td class="border-top border-right border-bottom border-left text-center">
                        {{ number_format($persenSpptSisa, 2, ',', '.') }}</td>
                    <td class="border-top border-right border-bottom border-left text-right">
                        {{ $request->format == 'PDF' ? number_format($jmlhSisa, 0, ',', '.') : $jmlhSisa }}</td>
                    <td class="border-top border-right border-bottom border-left text-center">
                        {{ number_format($persenSisa, 2, ',', '.') }}</td>
                </tr>
                @php
                    $totalSppt += $jmlhSppt;
                    $totalSpptLunas += $jmlhSpptLunas;
                    $totalSpptSisa += $jmlhSpptSisa;
                    $totalKetetapan += $jmlhKetetapan;
                    $totalRealisasi += $jmlhRealisasi;
                    $totalSisa += $jmlhSisa;

                    $totalPersenSpptLunas = $totalSpptLunas > 0 ? ($totalSpptLunas / $totalSppt) * 100 : 0;
                    $totalPersenSpptSisa = $totalSpptSisa > 0 ? ($jmlhSpptSisa / $totalSppt) * 100 : 0;

                    $totalPersenRealiasasi = $totalRealisasi > 0 ? ($totalRealisasi / $totalKetetapan) * 100 : 0;
                    $totalPersenSisa = $totalSisa > 0 ? ($totalSisa / $totalKetetapan) * 100 : 0;
                @endphp
            @endforeach
        </tbody>
        <tfoot>
            <tr class="background-gray-50">
                <th class="border-top border-right border-bottom border-left text-center" colspan="4">
                    TOTAL REKAPITULASI </th>
                <th class="border-top border-right border-bottom border-left text-center">
                    {{ $request->format == 'PDF' ? number_format($totalSppt, 0, ',', '.') : $totalSppt }}</th>
                <th class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($totalKetetapan, 0, ',', '.') : $totalKetetapan }}
                </th>
                <th class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($totalSpptLunas, 0, ',', '.') : $totalSpptLunas }}
                </th>
                <th class="border-top border-right border-bottom border-left text-center">
                    {{ number_format($totalPersenSpptLunas, 2, ',', '.') }}</th>
                <th class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($totalRealisasi, 0, ',', '.') : $totalRealisasi }}
                </th>
                <th class="border-top border-right border-bottom border-left text-center">
                    {{ number_format($totalPersenRealiasasi, 2, ',', '.') }}</th>
                <th class="border-top border-right border-bottom border-left text-center">
                    {{ $request->format == 'PDF' ? number_format($totalSpptSisa, 0, ',', '.') : $totalSpptSisa }}</th>
                <th class="border-top border-right border-bottom border-left text-center">
                    {{ number_format($totalPersenSpptSisa, 2, ',', '.') }}</th>
                <th class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($totalSisa, 0, ',', '.') : $totalSisa }}</th>
                <th class="border-top border-right border-bottom border-left text-center">
                    {{ number_format($totalPersenSisa, 2, ',', '.') }}</th>
            </tr>
        </tfoot>
    </table>
</div>
