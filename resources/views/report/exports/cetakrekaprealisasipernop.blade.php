@php
error_reporting(0);
ini_set("pcre.backtrack_limit", "50000000");
ini_set('memory_limit', '-1');

@endphp


<style>
    h3 {
        text-align: center;
    }

    .background-gray-50 {
        background: rgb(226, 225, 225);
    }

    .background-black {
        background: black;
        color: white;
    }

    .text-right {
        text-align: right;
    }

    .text-center {
        text-align: center;
    }

    .border-left {
        border-left: 1px solid #000;
    }

    .border-right {
        border-right: 1px solid #000;
    }

    .border-top {
        border-top: 1px solid #000;
    }

    .border-bottom {
        border-bottom: 1px solid #000;
    }
</style>
<div>
    @if ($request->format == 'PDF')
        @include('components.kop')
    @endif

    <table width="100%" cellspacing="2" cellpadding="2" style="border-collapse: collapse;font-size:10pt;">
        <tr>
            <th colspan="2" style="padding: 10px;">
                <h3>REKAPITULASI PEMBAYARAN SPPT PER-NOP TAHUN {{ $request->tahun }}</h3>
            </th>
        </tr>
        <tr>
            <td width="150px">{{ $pemda->s_namakecamatan }}</td>
            <td>: {{ $request->kec != '' ? $kecamatan->KD_KECAMATAN . ' - ' . $kecamatan->NM_KECAMATAN : 'Seluruh' }}
            </td>
        </tr>
        <tr>
            <td>{{ $pemda->s_namakelurahan }}</td>
            <td>:  {{ $request->kel != '' ? $kelurahan->KD_KELURAHAN . ' - ' . $kelurahan->NM_KELURAHAN : 'Seluruh' }}</td>
        </tr>
        <tr>
            <td>Tahun Pajak</td>
            <td>:
                @if($request->tahun=='')
                seluruh
                @else
                {{ $request->tahun }}
                @endif
            </td>
        </tr>
         <tr>
            <td>Tgl. Pembayaran</td>
            <td>:
                @if($request->tglbayar=='')
                seluruh
                @else
                {{ $request->tglbayar }}
                @endif
            </td>
        </tr>

    </table>
    <table width="100%" cellspacing="2" cellpadding="2" style="border-collapse: collapse;font-size:10pt;">
        <tr class="background-gray-50">
            <th class="border-top border-right border-bottom border-left">No</th>
            <th class="border-top border-right border-bottom border-left" style="width: 35%">NOP</th>
            <th class="border-top border-right border-bottom border-left">TAHUN</th>
            <th style="width: 20%" class="border-top border-right border-bottom border-left">Nama</th>
            <th class="border-top border-right border-bottom border-left">Pokok</th>
            <th class="border-top border-right border-bottom border-left">Denda</th>
            <th class="border-top border-right border-bottom border-left">Total</th>
            <th class="border-top border-right border-bottom border-left">Tgl. Pembayaran</th>
            <th class="border-top border-right border-bottom border-left">Jenis Bayar</th>
        </tr>
         @php
            $counter = 1;
             $totalPokok = 0;
             $totalDenda = 0;
             $totalJumlah = 0;
        @endphp
        @foreach ($data as $key => $row)

        @php

            $denda=$row->JML_PBB_YG_DIBAYAR - $row->PBB_YG_HRS_DIBAYAR_SPPT;

        @endphp
            <tr>
                <td class="border-top border-right border-bottom border-left text-center">{{ $counter++ }}</td>
                <td class="border-top border-right border-bottom border-left text-center" style="width: 15%">{{ MenuHelper::nop($row) }}
                </td>
                <td class="border-top border-right border-bottom border-left text-center" style="width: 5%">{{ $row->THN_PAJAK_SPPT }}
                </td>
                <td class="border-top border-right border-bottom border-left" style="width: 20%">{{ $row->NM_WP_SPPT }}</td>

                <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($row->PBB_YG_HRS_DIBAYAR_SPPT, 0, ',', '.') : $row->PBB_YG_HRS_DIBAYAR_SPPT }}
                </td>
                <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($denda, 0, ',', '.') : $denda }}
                </td>
                <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($JML_PBB_YG_DIBAYAR, 0, ',', '.') : $row->JML_PBB_YG_DIBAYAR }}
                </td>
                <td class="border-top border-right border-bottom border-left text-center" style="width: 10%">
                    {{ date('d-m-Y', strtotime($row->TGL_PEMBAYARAN_SPPT));}}
                </td>
                <td class="border-top border-right border-bottom border-left text-center" style="width: 20%">
                {{-- {{ $row->KODE_BAYAR ? $row->KODE_BAYAR : '-' }} --}}
                @if($row->STATUS_BILLING_KOLEKTIF=='1')
                <br>Kolektif
                @else($row->STATUS_BILLING_KOLEKTIF=='0')
                <br>Individu
               @endif
                </td>

            </tr>
         @php

                $totalPokok += $row->PBB_YG_HRS_DIBAYAR_SPPT;
                $totalDenda += $denda;
                $totalJumlah += $row->JML_PBB_YG_DIBAYAR;
        @endphp
        @endforeach

         <tr class="background-gray-50">
           <td colspan="4" class="border-top border-right border-bottom border-left text-center">J U M L A H
           </td>

            <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($totalPokok, 0, ',', '.') : $totalPokok }}
            </td>
             <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($totalDenda, 0, ',', '.') : $totalDenda }}
            </td>
             <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($totalJumlah, 0, ',', '.') : $totalJumlah }}
            </td>
            <td class="border-top border-right border-bottom border-left text-right">

            </td>
            <td class="border-top border-right border-bottom border-left text-right">

            </td>

        </tr>
    </table>
</div>
