@php
error_reporting(0);
ini_set('pcre.backtrack_limit', 10000000);
@endphp
<style>
    h3 {
        text-align: center;
    }

    .background-gray-50 {
        background: rgb(226, 225, 225);
    }

    .background-black {
        background: black;
        color: white;
    }

    .text-right {
        text-align: right;
    }

    .text-center {
        text-align: center;
    }

    .border-left {
        border-left: 1px solid #000;
    }

    .border-right {
        border-right: 1px solid #000;
    }

    .border-top {
        border-top: 1px solid #000;
    }

    .border-bottom {
        border-bottom: 1px solid #000;
    }
</style>
<div>
    @if ($request->format == 'PDF')
        @include('components.kop')
    @endif



    <table width="100%" cellspacing="2" cellpadding="2" style="border-collapse: collapse;font-size:10pt;">
        <tr>
            <th colspan="2" style="padding: 10px;">
                <h3>DATA REKAPITULASI REALIASASI TAHUN {{ $request->tahun }}</h3>
            </th>
        </tr>
        <tr>
            <td width="150px">{{ $pemda->s_namakecamatan }}</td>
            <td>: {{ $data[0]->KD_KECAMATAN . ' - ' . $data[0]->nm_kecamatan }}</td>
        </tr>
        @if ($request->kel)
            <tr>
                <td>{{ $pemda->s_namakelurahan }}</td>
                <td>: {{ $data[0]->KD_KELURAHAN . ' - ' . $data[0]->nm_kelurahan }}</td>
            </tr>
        @endif
    </table>
    <table width="100%" cellspacing="2" cellpadding="5" style="border-collapse: collapse;font-size:10pt;">
        <tr class="background-gray-50">
            <th class="border-top border-right border-bottom border-left">NO</th>
            <th class="border-top border-right border-bottom border-left">kode</th>
            <th class="border-top border-right border-bottom border-left">Kelurahan</th>
            <th class="border-top border-right border-bottom border-left">Blok</th>
            <th class="border-top border-right border-bottom border-left">Jumlah SPPT</th>
            <th class="border-top border-right border-bottom border-left">Jumlah Ketetapan (Rp)</th>
            <th class="border-top border-right border-bottom border-left">Jumlah Data Realisasi</th>
            <th class="border-top border-right border-bottom border-left">Jumlah Realisasi (Rp)</th>
            <th class="border-top border-right border-bottom border-left">Persentase (%)</th>
        </tr>
        @php
            $counter = 1;
        @endphp
        @foreach ($data as $key => $row)
            @php
                $ketetapan = $row->jumlah;
                $realisasi = $row->realisasi - $row->denda;
                $persentase = $realisasi > 0 ? ($realisasi / $ketetapan) * 100 : 0;
            @endphp
            <tr>
                <td class="border-top border-right border-bottom border-left text-center">{{ $counter++ }}</td>
                <td class="border-top border-right border-bottom border-left text-center">{{ $row->KD_KELURAHAN }}</td>
                <td class="border-top border-right border-bottom border-left text-left">{{ $row->nm_kelurahan }}</td>
                <td class="border-top border-right border-bottom border-left text-center">{{ $row->KD_BLOK }}</td>
                <td class="border-top border-right border-bottom border-left text-center">
                    {{ $request->format == 'PDF' ? number_format($row->count, 0, ',', '.') : $row->count }}
                </td>
                <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($ketetapan, 0, ',', '.') : $ketetapan }}</td>
                <td class="border-top border-right border-bottom border-left text-center">
                    {{ $request->format == 'PDF' ? number_format($row->countreal, 0, ',', '.') : $row->countreal }}
                </td>
                <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($realisasi, 0, ',', '.') : $realisasi }}</td>

                <td class="border-top border-right border-bottom border-left text-right">
                    {{ $request->format == 'PDF' ? number_format($persentase, 2, ',', '.') : number_format($persentase, 2) }}
                </td>
            </tr>
        @endforeach
    </table>
</div>
