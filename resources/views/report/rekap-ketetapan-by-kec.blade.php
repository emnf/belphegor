@extends('layouts.master')

@section('judulkiri')
    LAPORAN REKAP KETETAPAN DAN REALISASI PER {{ strtoupper($data_pemda->s_namakecamatan) }}
@endsection

@section('judulkanan')
    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ url('report') }}">Laporan</a></li>
    <li class="breadcrumb-item active">Index</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-success">
                <div class="card-header">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <dl class="row">
                                <dd class="col-sm-2">TAHUN </dd>
                                <dt class="col-sm-3">
                                    <select id="filter-tahun" class="form-control form-control-sm">
                                        @for ($i = 0; $i <= 6; $i++)
                                            @php
                                                $tahun = date('Y', strtotime("-$i years"));
                                            @endphp
                                            <option value="{{ $tahun }}">{{ $tahun }}</option>
                                        @endfor
                                    </select>
                                </dt>
                                <dt class="col-sm-7">
                                    <button type="button" class="btn btn-outline-primary btn-sm"
                                        onclick="getDataKecamatan()"><i class="fa fa-search"></i> CARI</button>
                                    <button type="button" class="btn btn-danger btn-sm" onclick="exportData('PDF')"><i
                                            class="fas fa-file-pdf"></i>
                                        Export Data (PDF)</button>
                                    <button type="button" class="btn btn-success btn-sm" onclick="exportData('XLS')"><i
                                            class="fas fa-file-excel"></i>
                                        Export Data (XLS)</button>
                                </dt>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-vertical-center table-sm">
                            <thead>
                                <tr class="bg-blue">
                                    <th class="text-center" rowspan="2">NO</th>
                                    {{-- <th class="text-center" colspan="2">{{ strtoupper($data_pemda->s_namakecamatan) }} --}}
                                    </th>
                                    <th class="text-center" colspan="4">KETETAPAN</th>
                                    <th class="text-center" colspan="4">PEMBAYARAN</th>
                                    <th class="text-center" colspan="4">SISA</th>
                                </tr>
                                <tr class="bg-blue">
                                    <th class="text-center">KODE</th>
                                    <th class="text-center">{{ strtoupper($data_pemda->s_namakecamatan) }}</th>
                                    <th class="text-center">SPPT</th>
                                    <th class="text-center">JML (Rp)</th>
                                    <th class="text-center">SPPT</th>
                                    <th class="text-center">(%)</th>
                                    <th class="text-center">JML (Rp)</th>
                                    <th class="text-center">(%)</th>
                                    <th class="text-center">SPPT</th>
                                    <th class="text-center">(%)</th>
                                    <th class="text-center">JML (Rp)</th>
                                    <th class="text-center">(%)</th>
                                </tr>
                            </thead>
                            <tbody id="dataGrid">
                                <tr>
                                    <td colspan="15">Belum ada data yang
                                        ditampilkan.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        setTimeout(() => {
            getDataKecamatan();
        }, 1000);

        function getDataKecamatan() {
            const tahun = $('#filter-tahun').val();
            $.ajax({
                url: '{{ url('report/get-rekap-ketetapan-by-kec') }}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "tahun": tahun
                },
                beforeSend: function() {
                    $('#dataGrid').html(
                        '<tr><td colspan="15"><i class="fa fa-spinner fa-spin text-lg"></i> Menampilkan data kecamatan ...</td></tr>'
                    );
                },
            }).then(function(data) {
                $('#dataGrid').html(data.data);
            });
        }

        function exportData(a) {

            const tahun = $('#filter-tahun').val();

            window.open('cetakrekapketetapankec?format=' + a + '&tahun=' + tahun);
        }
    </script>
@endpush
