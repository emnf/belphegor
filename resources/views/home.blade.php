@extends('layouts.master')

@section('judulkiri')

@endsection

@section('judulkanan')
    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@section('content')
    <?php
        $background = Illuminate\Support\Facades\DB::table('s_background')
            ->where('s_id_status', 1)
            ->first();
    ?>
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="flex-grow-1 bg-primary rounded-lg elevation-3"
                style="background: linear-gradient(to right, rgba(11, 150, 148, 0.7), rgba(50, 30, 226, 0.5)),
                url({{ $background->s_thumbnail }}) no-repeat center center; ">

                <div class="d-flex align-content-center ">
                    <div class="p-3 align-self-center">
                        <img src="{{ $data_pemda->s_logo }}" alt="" style="height: 120px;">
                    </div>
                    <div class="d-flex flex-column align-self-center p-3">
                        <p class="my-2">{{ strtoupper('Pemerintah ' . $data_pemda->s_namakabkot) }}</p>
                        <h4 class="font-weight-bolder">
                            {{ strtoupper($data_pemda->s_namainstansi) }}
                        </h4>
                        <h2 class="font-weight-bolder">
                            {{ '(' . $data_pemda->s_namasingkatinstansi . ')' }}
                        </h2>

                    </div>
                </div>
            </div>

            {{-- @if (Auth::user()->s_id_hakakses == 3)
                <div id="gridDataObjek"></div>
            @else
                <div class="row mt-2">
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="card elevation-2 bg-blue">
                            <div id="accordion">
                                <div class="card-body">
                                    <div class="d-flex justify-content-start mb-4">
                                        <i class="fas fa-check-circle fa-3x"></i>
                                        <h5 class="ml-2">KETETAPAN<br>
                                            <small>TAHUN {{ date('Y') }}</small>
                                        </h5>
                                    </div>

                                    <span>SPPT : <strong id="countSppt">0</strong></span><br>
                                    <span>Rp. <strong id="sumSppt">0</strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="card elevation-2" style="background: rgb(84, 192, 84);color:white;">
                            <div id="accordion">
                                <div class="card-body">
                                    <div class="d-flex justify-content-start mb-4">
                                        <i class="fas fa-money-check fa-3x"></i>
                                        <h5 class="ml-2">PEMBAYARAN<br>
                                            <small>TAHUN {{ date('Y') }}</small>
                                        </h5>
                                    </div>

                                    <span>SPPT : <strong id="countSpptLunas">0</strong>
                                        <strong id="persenLunas"></strong></span><br>
                                    <span>Rp. <strong id="sumRealisasi">0</strong></span>
                                    <strong id="persentase"></strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="card elevation-2 bg-red">
                            <div id="accordion">
                                <div class="card-body">
                                    <div class="d-flex justify-content-start mb-4">
                                        <i class="fas fa-donate fa-3x"></i>
                                        <h5 class="ml-2">PIUTANG<br>
                                            <small>TAHUN {{ date('Y') }}</small>
                                        </h5>
                                    </div>

                                    <span>SPPT : <strong id="countSpptSisa">0</strong>
                                        <strong id="persenSisa"></strong></span><br>
                                    <span>Rp. <strong id="sumSisa">0</strong></span>
                                    <strong id="persentaseSisa"></strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif --}}

        </div>
        {{-- <div class="col-12 col-md-4">

            <div class="card card-widget widget-user elevation-3">

                <div class="widget-user-header text-white"
                    style="background: linear-gradient(to right, rgba(31, 237, 203, 0.7), rgba(12, 33, 218, 0.5)),
                url('../dist/img/photo5.png') no-repeat center center;">
                    <h3 class="widget-user-username text-right">{{ Auth::user()->name }}</h3>
                    <p class="widget-user-desc text-right">{{ Auth::user()->getRoleNames()->first() }}
                        <br> {{ Auth::user()->nik }}
                    </p>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="{{ asset('dist/img/avatar5.png') }}" alt="User Avatar">
                </div>
                <div class="card-body mt-4">
                    <div class="d-flex align-items-center">
                        <div class="col-md-6 border-right">
                            <div class="mt-4">
                                <h4 class="text-primary">
                                    {{ \Carbon\Carbon::now()->locale('id')->isoFormat('dddd') }}</h4>
                            </div>
                            <div class="">
                                <h5 class="text-sm">
                                    {{ \Carbon\Carbon::now()->locale('id')->isoFormat('D MMMM YYYY') }} </h5>
                                <h5 class="text-sm" id="date-div">
                                    {{ \Carbon\Carbon::now()->isoFormat('h:mm:ss') }}
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center mt-4">
                                <a href="{{ url('profile') }}" class="btn btn-primary"><i class="fa fa-user"></i>
                                    Profile</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>

    {{-- @if (in_array(Auth::user()->s_id_hakakses, [1, 2]))
        @include('components.dashboard-kec')

        <div class="row" style="display: none;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                Top 10 Penetapan
                            </div>
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">01</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">02</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">03</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">04</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">05</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">06</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">07</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">08</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">09</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">10</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Pembayaran</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                Top 10 Pembayaran
                            </div>
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">01</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">02</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">03</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">04</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">05</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">06</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">07</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">08</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">09</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">10</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Penetapan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                Top 10 Tunggakan
                            </div>
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">01</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">02</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">03</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">04</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">05</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">06</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">07</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">08</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between ">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">09</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="align-self-center mr-3">
                                            <div class="card bg-primary p-2" style="width: 50px;">
                                                <h5 class=" font-weight-bolder align-self-center">10</h5>
                                            </div>
                                        </div>
                                        <div class="align-self-center">
                                            <a href="#"
                                                class="text-dark-75 font-weight-bolder align-self-center">Tunggakan</a>
                                            <p class="label label-light label-inline font-weight-bold text-dark-50">Rp.
                                                1.000.000.000</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif --}}
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(() => {

            if ('{{ session('active_account') }}') {
                Swal.fire(
                    'Verifikasi Email Berhasil.',
                    '{{ session('active_account') ?? 'ini dari default' }}',
                    'primary'
                )
            }

            if ('{{ session('login_ok') }}') {
                Swal.fire(
                    'Login Berhasil.',
                    '{{ session('login_ok') ?? 'ini dari default' }}',
                    'primary'
                )
            }
        });

        const dateDiv = document.getElementById('date-div');
        var options = {
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric'
        };

        function myDateFunction() {
            const now = new Date();
            const nowStr = now.toLocaleTimeString('id-ID', options);
            dateDiv.innerHTML = nowStr;
        }
        setInterval(myDateFunction, 1000);
    </script>

    @if (Auth::user()->s_id_hakakses == 3)
        <script type="text/javascript">
            setTimeout(() => {
                cekobjekByUserid();
            }, 1000);

            function cekobjekByUserid() {
                $.ajax({
                    url: '{{ url('home/cekobjek-by-userid') }}',
                    type: 'GET',
                    beforeSend: function() {
                        $('#gridDataObjek').html(
                            '<i class="fa fa-spinner fa-spin"></i> <small>Loading ...</small>');
                    },
                }).then(function(data) {
                    $('#gridDataObjek').html(data.data);
                });
            }
        </script>
    @else
        @if (in_array(Auth::user()->s_id_hakakses, [1, 2]))
            <script type="text/javascript">
                function getDataKecamatan() {
                    const tahun = $('#tahunpajak').val();
                    $.ajax({
                        url: '{{ url('home/data-kecamatan') }}',
                        type: 'GET',
                        data: {
                            tahun: tahun
                        },
                        beforeSend: function() {
                            $('.submitspinner').html('<i class="fa fa-spinner fa-spin"></i>');
                            $('#dataGridKecamatan').html('<i class="fa fa-spinner fa-spin"></i> Loading ...');
                        },
                    }).then(function(data) {
                        $('.submitspinner').html('');
                        $('#dataGridKecamatan').html(data.data);
                    });
                }

                function showKelurahan(a) {
                    const tahun = $('#tahunpajak').val();
                    $.ajax({
                        url: '{{ url('home/data-kelurahan') }}',
                        type: 'GET',
                        data: {
                            kec: a,
                            tahun: tahun
                        },
                        beforeSend: function() {
                            $('.spinnerKelurahan').html(
                                '<i class="fa fa-spinner fa-spin"></i> Menampilkan Data Kelurahan...');
                        },
                    }).then(function(data) {
                        $('.spinnerKelurahan').html('');
                        $('#modal-rincian-kelurahan').modal('show');
                        $('#namaKecamatan').html(data.kecamatan + ' TAHUN ' + tahun);
                        $('#dataGridKelurahan').html(data.data);
                    });
                }
            </script>
        @endif

        <script type="text/javascript">
            setTimeout(() => {
                getCountCard();
            }, 1000);

            function getCountCard() {
                $.ajax({
                    url: '{{ url('home/count-card') }}',
                    type: 'GET',
                    beforeSend: function() {
                        $('#countSppt').html('<i class="fa fa-spinner fa-spin"></i> <small>Menghitung ...</small>');
                        $('#countSpptLunas').html(
                            '<i class="fa fa-spinner fa-spin"></i> <small>Menghitung ...</small>');
                        $('#countSpptSisa').html(
                            '<i class="fa fa-spinner fa-spin"></i> <small>Menghitung ...</small>');
                        $('#sumSppt').html('<i class="fa fa-spinner fa-spin"></i> <small>Menghitung ...</small>');
                        $('#sumRealisasi').html(
                            '<i class="fa fa-spinner fa-spin"></i> <small>Menghitung ...</small>');
                        $('#sumSisa').html(
                            '<i class="fa fa-spinner fa-spin"></i> <small>Menghitung ...</small>');
                    },
                }).then(function(data) {
                    $('#countSppt').html(data.data.sppt);
                    $('#countSpptLunas').html(data.data.spptlunas);
                    $('#countSpptSisa').html(data.data.spptsisa);
                    $('#sumSppt').html(data.data.ketetapan);
                    $('#sumRealisasi').html(data.data.realisasi);
                    $('#sumSisa').html(data.data.sisa);
                    $('#persenLunas').html('(' + data.data.persenlunas + ')');
                    $('#persenSisa').html('(' + data.data.persensisa + ')');
                    $('#persentase').html('(' + data.data.persentase + ')');
                    $('#persentaseSisa').html('(' + data.data.persentasesisa + ')');
                });
            }
        </script>
    @endif
@endpush
