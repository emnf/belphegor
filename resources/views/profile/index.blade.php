@extends('layouts.master')

@section('judulkiri')
    PENGATURAN PROFILE
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-success card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{ asset('dist/img/avatar5.png') }}"
                            alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{ strtoupper(Auth::user()->name) }}</h3>

                    <p class="text-muted text-center">{{ Auth::user()->getRoleNames()->first() }}</p>

                    @if (Auth::user()->s_id_hakakses == 3)
                        <p class="text-muted text-center">NIK : {{ Auth::user()->nik }}</p>
                    @endif

                    {{-- <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
              <b>Followers</b> <a class="float-right">1,322</a>
            </li>
            <li class="list-group-item">
              <b>Following</b> <a class="float-right">543</a>
            </li>
            <li class="list-group-item">
              <b>Friends</b> <a class="float-right">13,287</a>
            </li>
          </ul> --}}

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card card-outline card-success">
                <form class="form-horizontal" action="{{ route('profile.update') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">Nama Lengkap</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name"
                                    placeholder="{{ Auth::user()->name }}" value="{{ Auth::user()->name }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" name="email"
                                    placeholder="{{ Auth::user()->email }}" value="{{ Auth::user()->email }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-outline-warning">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push('script')
    <script type="text/javascript"></script>
@endpush
