<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>My DASHBOARD</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

    <!-- ADD STYLE CUSTOMES -->
    <link rel="stylesheet" href="{{ asset('css/dashboard/running-text.css') }}">

    <style>
        #clockDisplay{
            font-size: 18pt;
            font-weight: bold;
            background: blue;
            color: yellow;
        }
        #runningText{
            background: black;
            color: white;
            padding: 5px 5px 5px 5px;
        }
        .font-text{font-size: 10pt;}
        .main-header{padding:5px 10px 5px 10px;font-size: 14pt;}
        .main-footer{padding:0px 8px 0px 5px;}
    </style>
</head>
<body class="hold-transition layout-top-nav">
    <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header text-center bg-primary">
        <div class="container">
            <img src="{{ asset('/storage/'. $pemda->s_logo) }}" width="40">
            <img src="{{ asset('dist/img/my-dashboard.png') }}"><br>
            <strong>{{ strtoupper('Pemerintah '. $pemda->s_namakabkot) }}</strong><br>
            <strong>{{ strtoupper($pemda->s_namainstansi) }}</strong>
        </div>
    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="padding-top: 10px;">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                @include('component-dashboard.content-kedua')
                @include('component-dashboard.content-pertama')
            </div>
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="row">
            <div class="col-sm-1" id="clockDisplay"></div>
            <div class="col-sm-11" id="runningText">
                <marquee scrolldelay="100">
                    <span id="refresh_text">
                    @foreach($running as $key => $v)
                        <img src="{{ asset('/storage/'.$pemda->s_logo ) }}" width="20px">
                        {{ strtoupper($v->t_isitext) }}
                    @endforeach
                    <img src="{{ asset('/storage/'.$pemda->s_logo ) }}" width="20px">
                    </span>
                </marquee>
            </div>
        </div>
    </footer>
</div>

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
{{-- <script src="{{ asset('js/dashboard/dashboard-2.js') }}"></script> --}}
{{-- <script src="{{ asset('js/dashboard/dashboard-3.js') }}"></script> --}}

<script type="text/javascript">

    // setInterval(dataRealisasi, 60000);
    // setInterval(dataTargetRealisasi, 60000);
    // setInterval(dataRealseminggu, 10000);
    setTimeout(dataRealisasi, 1000);
    setTimeout(dataTargetRealisasi, 1000);
    setTimeout(dataRealseminggu, 1000);

    function dataRealisasi() {
        $.ajax({
            url: '{{ url('frontend/datarealisasi') }}',
            type: 'GET'
        }).then(function(data) {
            let nf = new Intl.NumberFormat('en-US');
            $("#totalBphtb").html(nf.format(data.total_bphtb));
            $("#totalSimpatda").html(nf.format(data.total_simpatda));
            $("#totalPbb").html(nf.format(data.total_pbb));
            $("#totalRealisasi").html(nf.format(data.total_realisasi));
        });
    }

    function dataTargetRealisasi() {
        $.ajax({
            url: '{{ url('frontend/datatargetrealisasi') }}',
            type: 'GET'
        }).then(function(data) {
            $("#dataGridTarget").html(data);
        });
    }

    function dataRealseminggu() {
        $.ajax({
            url: '{{ url('frontend/datarealseminggu') }}',
            type: 'GET'
        }).then(function(data) {

            var ticksStyle = {
                fontColor: '#495057',
                fontStyle: 'bold'
            }

            var mode = 'index'
            var intersect = true

            var $visitorsChart = $('#visitors-chart')
            var visitorsChart = new Chart($visitorsChart, {
                data: {
                    labels: [data.typedate7, data.typedate6, data.typedate5, data.typedate4, data.typedate3, data.typedate2, data.typedate1],
                    datasets: [{
                            type: 'bar',
                            data: [data.jmlhhari_7pbb,
                                data.jmlhhari_6pbb,
                                data.jmlhhari_5pbb,
                                data.jmlhhari_4pbb,
                                data.jmlhhari_3pbb,
                                data.jmlhhari_2pbb,
                                data.jmlhhari_1pbb],
                            backgroundColor: '#007bff',
                            borderColor: '#007bff',
                            pointBorderColor: '#007bff',
                            pointBackgroundColor: '#007bff'
                        },
                        {
                            type: 'bar',
                            data: [data.jmlhhari_7bphtb,
                                data.jmlhhari_6bphtb,
                                data.jmlhhari_5bphtb,
                                data.jmlhhari_4bphtb,
                                data.jmlhhari_3bphtb,
                                data.jmlhhari_2bphtb,
                                data.jmlhhari_1bphtb],
                            backgroundColor: '#28a745',
                            borderColor: '#28a745',
                            pointBorderColor: '#28a745',
                            pointBackgroundColor: '#28a745'
                        },
                        {
                            type: 'bar',
                            data: [data.jmlhhari_7pdl,
                                data.jmlhhari_6pdl,
                                data.jmlhhari_5pdl,
                                data.jmlhhari_4pdl,
                                data.jmlhhari_3pdl,
                                data.jmlhhari_2pdl,
                                data.jmlhhari_1pdl],
                            backgroundColor: '#ffc107',
                            borderColor: '#ffc107',
                            pointBorderColor: '#ffc107',
                            pointBackgroundColor: '#ffc107'
                        }
                    ]
                },
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        mode: mode,
                        intersect: intersect
                    },
                    hover: {
                        mode: mode,
                        intersect: intersect
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            // display: false,
                            gridLines: {
                                display: true,
                                lineWidth: '4px',
                                color: 'rgba(0, 0, 0, .2)',
                                zeroLineColor: 'transparent'
                            },
                            ticks: $.extend({
                                beginAtZero: true,
                                suggestedMax: 200
                            }, ticksStyle)
                        }],
                        xAxes: [{
                            display: true,
                            gridLines: {
                                display: false
                            },
                            ticks: ticksStyle
                        }]
                    }
                }
            })
        });
    }

    // setInterval(renderRunningText, 10000);
    function renderRunningText(){
        $('#refresh_text').load(location.href + ' #refresh_text');
    }

    function renderTime() {
        var currentTime = new Date();
        var h = currentTime.getHours();
        var m = currentTime.getMinutes();
        var s = currentTime.getSeconds();
        if (h == 0) {
            h = 24;
        }
        if (h < 10) {
            h = "0" + h;
        }
        if (m < 10) {
            m = "0" + m;
        }
        if (s < 10) {
            s = "0" + s;
        }
        var myClock = document.getElementById('clockDisplay');
        myClock.textContent = h + ":" + m + ":" + s + "";
        setTimeout('renderTime()', 1000);
    }
    renderTime();

    $(function() {

    })
</script>
</body>
</html>
