@extends('layouts.front')

@section('content')
    <div class="brand-wrapper">
        <a href="{{ url('') }}">
        <h3 class="mt-2 brand-text">
            <img src="{{ asset('dist/img/sppt_big.png') }}" alt="LOGO" class="brand-image" height="70">
        </h3>
        </a>
    </div>
    <div class="login-wrapper my-auto">
        <h3 class="login-title">{{ __('Verify Your Email Address') }}</h3>
        <p class="">
            {{ __('Before proceeding, please check your email for a verification link.') }}
            {{ __('If you did not receive the email') }},
            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}" id="formsubmit">
                @csrf
                <button type="submit" class="btn btn-link p-0 m-0 align-baseline" id="submitbutton">{{ __('click here to request another') }}</button>.
            </form>
        </p>
    </div>
@endsection

{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Addresss') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
