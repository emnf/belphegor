@extends('layouts.front')

@section('content')
    <div class="brand-wrapper">
        <a href="{{ url('') }}">
            <h3 class="mt-2 brand-text">
                <img src="{{ asset('dist/img/sppt_big.png') }}" alt="LOGO" class="brand-image" height="70">
            </h3>
        </a>
    </div>
    <div class="login-wrapper my-auto">
        <h3 class="login-title">{{ __('Reset Password') }}</h3>
        <p class="">Masukkan E-Mail anda yang terdaftar.</p>
        <form method="POST" action="{{ route('password.email') }}" id="formsubmit">
            @csrf
            <div class="form-group">
                <label for="username">E-Mail Address</label>
                <input type="email" name="email" id="email"
                    class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                    placeholder="Masukkan Email" autocomplete="off">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-success btn-block" id="submitbutton">{{ __('Send Password Reset Link') }}
            </button>
        </form>
        <div class="mt-4">
            <p class="login-wrapper-footer-text">saya sudah punya akun, <a href="{{ url('login') }}"
                    class="text-success">Masuk disini.</a></p>
        </div>
    </div>
@endsection
{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
