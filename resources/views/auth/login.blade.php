@extends('layouts.front')

@section('content')
    <div class="brand-wrapper">
        <a href="{{ url('') }}">
            <h3 class="mt-2 brand-text">
                <i class="fas fa-chart-line fa-1x"> || MyDASBOARD</i>
            </h3>
        </a>
    </div>
    <div class="login-wrapper my-auto">
        <h3 class="login-title">Log in</h3>
        <p class=""><span class="text-success">Masuk dulu</span> untuk memulai
            aplikasi</p>
        <form method="POST" action="{{ route('login') }}" id="formsubmit">
            @csrf
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" name="username" id="username"
                    class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}"
                    placeholder="masukkan username" autocomplete="off">
                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"
                    class="form-control" placeholder="masukkan password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                {!! NoCaptcha::display() !!}
                @if ($errors->has('g-recaptcha-response'))
                    <span class="help-block" style="color: red">
                        {{ $errors->first('g-recaptcha-response') }}
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-warning btn-block" id="submitbutton">Login </button>
        </form>
    </div>
@endsection
