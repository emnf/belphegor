@extends('layouts.front')

@section('content')
    <div class="brand-wrapper">
        <a href="{{ url('') }}">
            <img src="dist/img/sppt_big.png" alt="LOGO" class="brand-image" height="50">
        </a>
    </div>
    <div class="login-wrapper my-auto">
        <h5 class="login-title">Daftar Akun</h5>
        <p class=""><span class="text-success">Isi data berikut</span> untuk mendaftarkan akun</p>
        <form method="POST" action="{{ route('register') }}" id="formsubmit">
            @csrf
            <div class="form-group">
            <label for="nama" class="mb-0">NIK</label>
                <input id="nik" type="text" class="form-control @error('nik') is-invalid @enderror" name="nik"
                    value="{{ old('nik') }}" autocomplete="name" maxlength="20" onchange="ceknikSubje()">
                @error('nik')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama" class="mb-0">Nama</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                    value="{{ old('name') }}" autocomplete="name">
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="email" class="mb-0">Email</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                    name="email" value="{{ old('email') }}" autocomplete="email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password" class="mb-0">Password</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password" class="mb-0">Ulangi Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                    autocomplete="new-password">
            </div>
            <div class="form-group">
                {!! NoCaptcha::display() !!}
                @if ($errors->has('g-recaptcha-response'))
                    <span class="help-block" style="color: red">
                        {{ $errors->first('g-recaptcha-response') }}
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-success btn-block" id="submitbutton">
                {{ __('Daftar') }}
            </button>
        </form>
        <div class="mt-2">
            <p class="login-wrapper-footer-text">Sudah punya akun? <a href="{{ url('login') }}" class="text-success">Masuk
                    disini.</a></p>
        </div>

    </div>

    <script type="text/javascript">
        function ceknikSubje() {
            var nik = $('#nik').val();

            $.ajax({
                url: '{{ url('frontend/ceknik') }}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": nik
                },
            }).then(function(data) {
                if (data.status == 200) {

                }

                if (data.status == 404) {
                    $('#nik').val('');
                    $('#nik').focus();
                    Swal.fire({
                        icon: 'warning',
                        title: 'Opsss...',
                        text: data.message
                    });
                }
            });
        }
    </script>
@endsection



