@php
$pemda = Illuminate\Support\Facades\DB::table('s_pemda')->first();
@endphp

@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<img src="http://mysimpatda.fakfakkab.go.id/data_file/1676989713_123_preview_rev_1.png"><br>
{{ config('app.name') }}<br>
{{ strtoupper(optional($pemda)->s_namakabkot) }}<br>
{{ strtoupper(optional($pemda)->s_namainstansi) }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. <img src="https://mitraprimautama.id/frontend/img/logo.png" width="50"> (mpu) @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
