<?php

use App\Http\Controllers\Activity\HistoryActivityController;
use App\Http\Controllers\Billing\BillingKolektifController;
use App\Http\Controllers\CekStatus\CekStatusController;
use App\Http\Controllers\dashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InputSppt\InputSpptController;
use App\Http\Controllers\Payment\PaymentController;
use App\Http\Controllers\Permissions\{
    AssignController,
    PermissionController,
    RoleController,
    UserController
};
use App\Http\Controllers\PerubahanData\PerubahanDataController;
use App\Http\Controllers\RegisterOp\RegisterOpController;
use App\Http\Controllers\Report\ReportController;
use App\Http\Controllers\Report\ReportKetetapanController;
use App\Http\Controllers\Report\ReportRealisasiController;
use App\Http\Controllers\Report\ReportTunggakanController;
use App\Http\Controllers\Setting\{
    SettingBlankoController,
    SettingDukuhController,
    SettingLoginBackground,
    SettingUserController,
    SettingPemdaController,
    SettingPassController,
    SettingProfileController,
    TextController
};
use Facade\FlareClient\Http\Response;
use Hamcrest\Core\Set;
use Illuminate\Support\Facades\{
    Artisan,
    Route,
    Auth,
    Request
};
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes(['verify' => true]);

Route::get('/', function () {
    return view('landing.index');
});



Route::post('captcha-validation', [App\Http\Controllers\Auth\LoginController::class, 'capthcaFormValidate']);
Route::get('reload-captcha', [App\Http\Controllers\Auth\LoginController::class, 'reloadCaptcha']);

Route::middleware(['auth', 'verified'])->group(function () {

    Route::prefix('home')->group(function () {
        Route::get('', [HomeController::class, 'index'])->name('home');
    });

    Route::prefix('dashboard')->group(function () {
        Route::get('', [dashboardController::class, 'index'])->name('home');
        Route::get('penerimaan-harian', [dashboardController::class, 'penerimaanHarian'])->name('penerimaan-harian');
    });

    Route::prefix('setting-pemda')->middleware(['permission:SettingPemda'])->group(function () {
        Route::get('', [SettingPemdaController::class, 'index']);
        Route::post('create-or-update', [SettingPemdaController::class, 'createOrUpdate'])->name('setting-pemda.createOrUpdate');
    });

    Route::prefix('setting-text')->middleware(['permission:SettingText'])->group(function () {
        Route::get('', [TextController::class, 'index']);
        Route::post('datagrid', [TextController::class, 'datagrid']);
        Route::post('store', [TextController::class, 'store'])->name('setting-text.store');
        Route::get('detail/{id}', [TextController::class, 'detail']);
        Route::delete('delete/{id}', [TextController::class, 'delete']);
    });

    Route::prefix('setting-blanko')->middleware(['permission:SettingBlanko'])->group(function () {
        Route::get('', [SettingBlankoController::class, 'index']);
        Route::get('detail', [SettingBlankoController::class, 'detail']);
        Route::post('update', [SettingBlankoController::class, 'update'])->name('setting-blanko.update');
    });

    Route::prefix('setting-login-background')->middleware(['permission:SettingLoginBackground'])->group(function () {
        Route::get('', [SettingLoginBackground::class, 'index']);
        Route::post('datagrid', [SettingLoginBackground::class, 'datagrid']);
        Route::get('detail/{login_background}', [SettingLoginBackground::class, 'detail']);
        Route::get('create', [SettingLoginBackground::class, 'create'])->name('setting_login_background.create');
        Route::post('store', [SettingLoginBackground::class, 'store']);
        Route::get('{login_background}/edit', [SettingLoginBackground::class, 'edit']);
        Route::patch('{login_background}/edit', [SettingLoginBackground::class, 'update']);
        Route::get('delete/{login_background}', [SettingLoginBackground::class, 'destroy']);
        Route::get('export_xls', [SettingLoginBackground::class, 'export_xls'])->name('setting_login_background.export_xls');
        Route::get('export_pdf', [SettingLoginBackground::class, 'export_pdf'])->name('setting_login_background.export_pdf');
    });

    Route::prefix('setting-user')->middleware(['permission:SettingUser'])->group(function () {
        Route::get('', [SettingUserController::class, 'index'])->name('setting-user');
        Route::get('tambah', [SettingUserController::class, 'tambah'])->name('setting-user.tambah');
        Route::post('create', [SettingUserController::class, 'store'])->name('setting-user.create');
        Route::post('datagrid', [SettingUserController::class, 'datagrid']);
        Route::get('detail/{id}', [SettingUserController::class, 'detail']);
        Route::get('edit/{uuid}', [SettingUserController::class, 'edit'])->name('setting-user.edit');
        Route::put('edit/{uuid}', [SettingUserController::class, 'update']);
        Route::get('delete/{id}', [SettingUserController::class, 'delete']);
        Route::get('comboKelurahan', [SettingUserController::class, 'comboKelurahan']);
        Route::get('comboDukuh', [SettingUserController::class, 'comboDukuh']);
    });

    Route::prefix('ubah-pass')->middleware(['permission:SettingPass'])->group(function () {
        Route::get('index', [SettingPassController::class, 'index'])->name('ubah-pass.index');
        Route::post('updatepass', [SettingPassController::class, 'updatepass'])->name('ubah-pass.updatepass');
    });

    Route::prefix('profile')->middleware(['permission:SettingProfile'])->group(function () {
        Route::get('', [SettingProfileController::class, 'index'])->name('profile.index');
        Route::post('update', [SettingProfileController::class, 'update'])->name('profile.update');
    });

    Route::prefix('history-activity')->middleware(['permission:HistoryActivity'])->group(function () {
        Route::get('', [HistoryActivityController::class, 'index'])->name('history-activity.index');
        Route::post('datagrid', [HistoryActivityController::class, 'datagrid']);
        Route::get('detail/{user}', [HistoryActivityController::class, 'detail']);
        Route::get('export', [HistoryActivityController::class, 'export'])->name('history-activity.export');
        Route::get('export_pdf', [HistoryActivityController::class, 'export_pdf'])->name('history-activity.export_pdf');
    });

    Route::prefix('role-and-permission')->namespace('Permissions')->group(function () {
        Route::prefix('roles')->middleware(['permission:SettingRole'])->group(function () {
            Route::get('', [RoleController::class, 'index'])->name('roles.index');
            Route::post('create', [RoleController::class, 'store'])->name('roles.create');
            Route::post('datagrid', [RoleController::class, 'datagrid']);
            Route::get('detail/{role}', [RoleController::class, 'detail']);
            Route::get('{role}/edit', [RoleController::class, 'edit'])->name('roles.edit');
            Route::put('{role}/edit', [RoleController::class, 'update']);
            Route::get('delete/{role}', [RoleController::class, 'delete']);
        });

        Route::prefix('permissions')->middleware(['permission:SettingPermission'])->group(function () {
            Route::get('', [PermissionController::class, 'index'])->name('permissions.index');
            Route::post('create', [PermissionController::class, 'store'])->name('permissions.create');
            Route::post('datagrid', [PermissionController::class, 'datagrid']);
            Route::get('detail/{role}', [PermissionController::class, 'detail']);
            Route::get('{permission}/edit', [PermissionController::class, 'edit'])->name('permissions.edit');
            Route::put('{permission}/edit', [PermissionController::class, 'update']);
            Route::get('delete/{role}', [PermissionController::class, 'delete']);
        });

        Route::prefix('assignable')->middleware(['permission:SettingAssignPermissionToRole'])->group(function () {
            Route::get('', [AssignController::class, 'index'])->name('assign.index');
            Route::post('datagrid', [AssignController::class, 'datagrid']);
            Route::get('detail/{role}', [AssignController::class, 'detail']);
            Route::get('create', [AssignController::class, 'create'])->name('assign.create');
            Route::post('create', [AssignController::class, 'store']);
            Route::get('{role}/edit', [AssignController::class, 'edit'])->name('assign.edit');
            Route::post('{role}/edit', [AssignController::class, 'update']);
            Route::get('delete/{role}', [AssignController::class, 'delete']);
        });

        Route::prefix('assign')->middleware(['permission:SettingAssignRoleToUser'])->group(function () {
            Route::get('user', [UserController::class, 'index'])->name('assign.user.index');
            Route::post('user/datagrid', [UserController::class, 'datagrid']);
            Route::get('user/detail/{role}', [UserController::class, 'detail']);
            Route::get('user/create', [UserController::class, 'create'])->name('assign.user.create');
            Route::post('user/create', [UserController::class, 'store']);
            Route::get('user/{user}/edit', [UserController::class, 'edit'])->name('assign.user.edit');
            Route::put('user/{user}/edit', [UserController::class, 'update']);
            Route::get('user/delete/{role}', [UserController::class, 'delete']);
        });
    });
});
